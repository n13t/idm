module gitlab.com/n13t/idm

go 1.13

require (
	github.com/casbin/casbin/v2 v2.1.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-kit/kit v0.10.0
	github.com/go-redis/redis/v7 v7.2.0
	github.com/golang/mock v1.4.3
	github.com/golang/protobuf v1.3.3
	github.com/google/uuid v1.1.1
	github.com/hashicorp/go-hclog v0.10.1
	github.com/hashicorp/vault v1.3.1
	github.com/hashicorp/vault/api v1.0.5-0.20191216174727-9d51b36f3ae4
	github.com/hashicorp/vault/sdk v0.1.14-0.20191218020134-06959d23b502
	github.com/lib/pq v1.3.0
	github.com/nirasan/go-oauth-pkce-code-verifier v0.0.0-20170819232839-0fbfe93532da
	github.com/oklog/oklog v0.3.2
	github.com/opentracing/opentracing-go v1.1.0
	github.com/openzipkin-contrib/zipkin-go-opentracing v0.4.5
	github.com/openzipkin/zipkin-go v0.2.2
	github.com/prometheus/client_golang v1.3.0
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/sony/gobreaker v0.4.1
	github.com/spf13/viper v1.6.3
	github.com/stretchr/testify v1.4.0
	gitlab.com/n13t/protos v0.7.32
	golang.org/x/crypto v0.0.0-20200204104054-c9f3fb736b72
	golang.org/x/oauth2 v0.0.0-20190402181905-9f3314589c9a
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0
	golang.org/x/tools v0.0.0-20200103221440-774c71fcf114
	google.golang.org/grpc v1.28.0
	gopkg.in/yaml.v2 v2.2.8 // indirect
	sourcegraph.com/sourcegraph/appdash v0.0.0-20190731080439-ebfcffb1b5c0
)
