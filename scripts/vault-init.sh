#!/bin/sh

#apt-get update 2>&1 >/dev/null && apt-get install -y curl 2>&1 >/dev/null
if ! [ -x '$(command -v curl)' ]; then
  apk update >/dev/null 2>&1 && apk add curl >/dev/null 2>&1
fi

curl --header "X-Vault-Token: $VAULT_TOKEN" -XPOST -d '{"type": "transit", "description": "encs encryption"}' $VAULT_ADDR/v1/sys/mounts/transit
curl -s --header "X-Vault-Token: $VAULT_TOKEN" -XGET $VAULT_ADDR/v1/transit/keys/idm
