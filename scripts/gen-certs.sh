#!/bin/sh

NAMESPACE=${1:-identity-8148196}
SERVICE=${2:-production-auto-deploy}

mkdir -p certs
cd certs || exit

cat <<EOF | cfssl genkey - | cfssljson -bare server
{
  "hosts": [
    "$SERVICE.$NAMESPACE.svc.cluster.local"
  ],
  "CN": "$SERVICE.$NAMESPACE.svc.cluster.local",
  "key": {
    "algo": "ecdsa",
    "size": 256
  }
}
EOF

cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: $SERVICE.$NAMESPACE
spec:
  request: $(cat server.csr | base64 | tr -d '\n')
  usages:
  - digital signature
  - key encipherment
  - server auth
EOF


kubectl certificate approve $SERVICE.$NAMESPACE
kubectl get csr $SERVICE.$NAMESPACE -o jsonpath='{.status.certificate}' | base64 --decode > server.crt
kubectl delete csr $SERVICE.$NAMESPACE


openssl ec -in server-key.pem -out server.key


cd - || exit
