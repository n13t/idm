tls=`kubectl -n sidm-14886941 get secrets production-auto-deploy-tls -ojson`
echo "$tls" | jq '.data["tls.crt"]' | tr -d \"
echo "$tls" | jq '.data["tls.key"]' | tr -d \"
