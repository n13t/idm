-- https://stackoverflow.com/questions/24841142/how-can-i-generate-big-data-sample-for-postgresql-using-generate-series-and-rand
INSERT INTO users (
    code, article, name, department
)
SELECT
    left(md5(i::text), 10),
    md5(random()::text),
    md5(random()::text),
    left(md5(random()::text), 4)
FROM generate_series(1, 1000000) s(i)