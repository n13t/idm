# Development

## Functional requirements
1. Storage sensitive data & act as source of truth
1. Prevent unauthorized access (Ex: Database admins read data on the database)
1. No identity change history

## Non-functions Requirements (NFRs)
### Environment
1. Able to [work](#work-state) while storing up to 100 millions records (user identity) 
on a standard machine (2 vCPU, 7.5GB Memory)
### Performance
1. All action that block user interaction SHOULD BE done within ***250ms***. 
Although, this service not directly use by user but every function SHOULD NOT exceed this limit.

1. Read function MUST be done under **250ms**
1. Write function MUST be done under **500ms** on default encrypt protocol (internal + external encrypt)

### Architecture
- Clean Architecture
    - [x] Independent of Frameworks: can be re-implement by other frameworks by import core business logic in pkg/
    - [ ] Testable (The business rules can be tested without the UI, Database, Web Server, or any other external element)
    - [ ] ~~Independent of UI~~ (no UI implementation planed yet)
    - [x] Independent of Databases: by implement more repository
    - [x] Independent of any external agency: using go-kit to decoupling business rules & transport protocols
### 
1. follow Single Responsibility Principle (SRP) and Separation of Concerns

### Threat Modeling
![](docs/threat-model.drawio.png)
#### Attack Tree
![](docs/atk-tree.drawio.png)

## Build
For development you need to compile over and over again, using Bazel will save you a lot of time.
### Prebuild step: install Bazelisk (go 1.11+)
```
go get github.com/bazelbuild/bazelisk
export PATH=$PATH:$(go env GOPATH)/bin
```

### Build command
```
bazelisk build //cmd/idm:idm
```

> use Bazel to build Docker image
```
bazel build //cmd/idm:idm_image.tar && \
docker load -i bazel-bin/cmd/idm/idm_image.tar

docker-compose up
```

### Update WORKSPACE & BUILD files
You may want to run following command after *modify go module(s)* or *adding new import*
```
bazel run //:gazelle -- update-repos -from_file=go.mod
bazel run //:gazelle
```

### Testing
#### Test core components
```
go test -v ./pkg/idmservice/
```

#### Benchmark
```
go test -v -bench=. ./pkg/idmservice/index/test/
```

#### Manual test with **idmcli**
> Login
```shell script
go run ./cmd/idmcli login -server_url=https://accounts.example.com/auth -client_id=my-client
```
###### `server_url` is the path that contains `.well-known/openid-configuration`

> List user
```
IDM_ADDR=idm.example.com:443 SKIP_TLS=true go run ./cmd/idmcli get user
```

> Create new user
```
bazel run //cmd/idmcli:idmcli -- -grpc-addr localhost:5051 -method create myusername user@example.com ThisIsMyPassword
```

> Search user by id
```
bazel run //cmd/idmcli:idmcli -- -grpc-addr localhost:5051 -method search email=user@example.com
```

> With TLS
```
-tls -ca_file $PWD/certs/server.crt -server_host_override production-auto-deploy.identity-8148196.svc.cluster.local
```

> Create 10 users
```
N=10 && for i in $(seq 1 $N); do bazel run //cmd/idmcli:idmcli -- create test$i $i@example.com 123; done
```

## Go standards and style guidelines
Keep the project follow the rules to help better maintaining
This project follow [Go standards and style guidelines](https://docs.gitlab.com/ee/development/go_guide/) by GitLab

## PHI
Names
All geographical data smaller than a state
Dates (other than year) directly related to an individual
Telephone numbers
Fax numbers
Email addresses
Social Security numbers
Medical record numbers
Health insurance plan beneficiary numbers
Account numbers
Certificate/license numbers
Vehicle identifiers and serial numbers including license plates
Device identifiers and serial numbers
Web URLs
Internet protocol (IP) addresses
Biometric identifiers (i.e. retinal scan, fingerprints, Etc.)
Full face photos and comparable images
Any unique identifying number, characteristic or code

## Work state
TODO define work state

## Implementation
Estimate service capability
```
total_write_time    = client_server_communication_time                          (20ms) (same nation)
                        + internal_encrypt_time                                 (5ms)
                        + service_communication_time + external_encrypt_time    (20ms)
                        + database_communication_time + database_execute_time   (100ms*)
                    = 145ms**

-- Cache hit
total_read_time     = client_server_communication_time                          (20ms)
                        + index_lookup_time                                     (100ms)
                        + cache_communication_time + cache_query_time           (30ms**)
                        + internal_decrypt_time                                 (5ms)
                    = 155ms**

-- Cache missed
total_read_time     = client_server_communication_time                          (20ms)
                        + index_lookup_time                                     (100ms*)
                        + database_communication_time + database_query_time     (100ms*)
                        + service_communication_time + external_decrypt_time    (100ms)
                        + internal_decrypt_time                                 (5ms)
                    = 325ms**
            
* Estimate on 1m rows table on standard-2 machine
** Estimate values
```
As the above estimate current architect can handle [Performance Requirement 2](#non-functions-requirements-nfrs)

## Benchmarking
### Postgres 9.6
With current Index table
`CREATE TABLE Index(id VARCHAR(50), doc JSONB)`

- WITH table that has 1m records (standard-2)
    - Took **116ms** just by counting rows
    - Took **112ms** just select one rows with id (string)
    - Took **200ms** by select all rows by some value in JSONB (simple structure)

- WITH table that has 100m records (standard-1)
    - Took **27s37ms** just by counting rows
    - Took **31s935ms** just select one rows with id (string)
    - Took **38s921ms** by select all rows by some value in JSONB (simple structure)

- WITH table that has 5m records (standard-2)
    - Took **375ms** just by counting rows
    - Took **365ms** just select one rows with id (string)
    - Took **810ms** by select all rows by some value in JSONB (simple structure)

## Key Rotation
This service has a built-in key rotation mechanic, which is update function that will use newest key every times user updating their data.  
The downside is sometime we need to change the encryption key in order to enforce security 
so the built-in mechanic may need forever to enforce new key to ours data.

So we may need an addition mechanic I called ["Force Key Rotation"]() which will check key's fingerprint on all our data then perform key rotation
like update function do but without user interact. In addition, we need to add ["Key Management"]() to manage key's fingerprint,
and ["Leader election"]() + ["Job scheduling"]() to perform our operation in an efficient way in scalable environment.

## Working with Hashicorp's Vault on Kubernetes
```
kubectl port-forward svc/vault 8200

export VAULT_ADDR=https://127.0.0.1:8200
export VAULT_SKIP_VERIFY=true
vault status
```

> Transit engine (https://www.vaultproject.io/docs/secrets/transit/)
```
vault secrets enable transit
vault write -f transit/keys/idm
```

## Other resources:
- https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1
- https://github.com/bxcodec/go-clean-arch
- https://medium.com/@cgrant/developing-a-simple-crud-api-with-go-gin-and-gorm-df87d98e6ed1
- https://medium.com/@rosaniline/unit-testing-gorm-with-go-sqlmock-in-go-93cbce1f6b5b
  -> https://github.com/Rosaniline/gorm-ut
- https://levelup.gitconnected.com/use-go-channels-as-promises-and-async-await-ee62d93078ec
- https://nathanleclaire.com/blog/2014/02/15/how-to-wait-for-all-goroutines-to-finish-executing-before-continuing/
- https://medium.com/@gauravsingharoy/asynchronous-programming-with-go-546b96cd50c1
- https://medium.com/rate-engineering/go-test-your-code-an-introduction-to-effective-testing-in-go-6e4f66f2c259
- https://www.hipaaguide.net/hipaa-for-dummies/
- https://en.wikipedia.org/wiki/Optimistic_concurrency_control

- [How to query JSONB, beginner sheet cheat](https://medium.com/hackernoon/how-to-query-jsonb-beginner-sheet-cheat-4da3aa5082a3)
- [Jsonb: few more stories about the performance](https://erthalion.info/2017/12/21/advanced-json-benchmarks/)
- [Faster Operations with the JSONB Data Type in PostgreSQL](https://www.compose.com/articles/faster-operations-with-the-jsonb-data-type-in-postgresql/)
- [When To Avoid JSONB In A PostgreSQL Schema](https://heap.io/blog/engineering/when-to-avoid-jsonb-in-a-postgresql-schema)
