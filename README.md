Identity Management Service 
------

A secure place to store user's data

![](https://nghinhut.dev/content/idm/architecture.svg?src=idm)

# Features
- [x] Application-level Encryption (support external encryption. Ex: Vault by HashiCorp)
- [x] Encryption as a Service (EaaS) (no database needed)
- [ ] Integrate with Keycloak via a custom [plugin](plugins/keycloak/README.md)
- [ ] [Key Rotation]()
- [ ] Multi transport protocols support: default: gRPC, coming soon: HTTP, Thrift
- [ ] OAuth2/UMA2 Compatible
- [ ] Kubernetes deployment
- [x] Non-root image

# Quick start
> Use Docker Compose to start all services
```
docker-compose up --build
```

## Project's Status
The project is ***Under development*** status. 

Things may break (Ex: database migration,.. ), this mean you may had to do manual steps to upgrade.

Production use is *discouraged*.

## Requirement
1. Postgres 9.1+ (for DB migration query)
