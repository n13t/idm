// https://golang.org/pkg/cmd/go/internal/generate/
//go:generate bash -c "GO111MODULE=on mockgen -package=mocks -source=$GOFILE -destination=../../mocks/$GOFILE"
// go:generate mockery -all -output ../../mocks/

package main

import (
	"crypto/tls"
	"database/sql"
	"encoding/base64"
	"flag"
	"fmt"
	"gitlab.com/n13t/idm/pkg/idmservice/user"
	userRepository "gitlab.com/n13t/idm/pkg/idmservice/user/repository"
	"gitlab.com/n13t/idm/pkg/uma2"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"

	"github.com/openzipkin/zipkin-go"
	"gitlab.com/n13t/idm/pkg/idm"
	idmPb "gitlab.com/n13t/protos/pkg/n13t/idm/v0"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/health/grpc_health_v1"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"text/tabwriter"

	"github.com/oklog/oklog/pkg/group"
	stdopentracing "github.com/opentracing/opentracing-go"
	zipkinhttp "github.com/openzipkin/zipkin-go/reporter/http"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc"
	"sourcegraph.com/sourcegraph/appdash"
	appdashot "sourcegraph.com/sourcegraph/appdash/opentracing"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
	"github.com/go-kit/kit/metrics/prometheus"
	kitgrpc "github.com/go-kit/kit/transport/grpc"

	idmEndpoint "gitlab.com/n13t/idm/pkg/idmendpoint"
	idmService "gitlab.com/n13t/idm/pkg/idmservice"
	idmTransport "gitlab.com/n13t/idm/pkg/idmtransport"
)

var (
	Version = "redacted"
	Commit = ""
)

func main() {
	// Define our flags. Your service probably won't need to bind listeners for
	// *all* supported transports, or support both Zipkin and LightStep, and so
	// on, but we do it here for demonstration purposes.
	fs := flag.NewFlagSet("idmsvc", flag.ExitOnError)
	var (
		debugAddr    	= fs.String("debug.addr", ":8080", "Debug and metrics listen address")
		grpcAddr     	= fs.String("grpc-addr", ":8082", "gRPC listen address")
		zipkinURL    	= fs.String("zipkin-url", "", "Enable Zipkin tracing via HTTP reporter URL e.g. http://localhost:9411/api/v2/spans")
		zipkinBridge 	= fs.Bool("zipkin-ot-bridge", false, "Use Zipkin OpenTracing bridge instead of native implementation")
		appdashAddr  	= fs.String("appdash-addr", "", "Enable Appdash tracing via an Appdash server host:port")
		tlsEnabled   	= fs.Bool("tls", false, "Connection uses TLS if true, else plain TCP")
		certFile   		= fs.String("cert_file", "", "The TLS cert file")
		keyFile    		= fs.String("key_file", "", "The TLS key file")
		version    		= fs.Bool("v", false, "IdM version")
		//vaultKeyname = fs.String("vault-keyname", "", "Vault key")
	)
	fs.Usage = usageFor(fs, os.Args[0]+" [flags]")
	fs.Parse(os.Args[1:])

	if *version {
		fmt.Printf("%s\n%s\n", Version, Commit)
		return
	}

	// Create a single logger, which we'll use and give to other components.
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.With(logger, "ts", log.DefaultTimestampUTC)
		logger = log.With(logger, "caller", log.DefaultCaller)
	}

	var zipkinTracer *zipkin.Tracer
	{
		tracingURL := os.Getenv("TRACING_URL")
		if tracingURL != "" {
			zipkinURL = &tracingURL 	// overwrite
		}

		if *zipkinURL != "" {
			var (
				err         error
				hostPort    = "localhost:80"
				serviceName = "idm"
				reporter    = zipkinhttp.NewReporter(*zipkinURL)
			)
			if e := os.Getenv("SERVICE_NAME"); e != "" {
				serviceName = e
			}

			defer reporter.Close()
			zEP, _ := zipkin.NewEndpoint(serviceName, hostPort)
			zipkinTracer, err = zipkin.NewTracer(reporter, zipkin.WithLocalEndpoint(zEP))
			if err != nil {
				logger.Log("err", err)
				os.Exit(1)
			}
			if !(*zipkinBridge) {
				logger.Log("tracer", "Zipkin", "type", "Native", "URL", *zipkinURL)
			}
		}
	}

	// Determine which OpenTracing tracer to use. We'll pass the tracer to all the
	// components that use it, as a dependency.
	var tracer stdopentracing.Tracer
	{
		if *zipkinBridge && zipkinTracer != nil {
			logger.Log("tracer", "Zipkin", "type", "OpenTracing", "URL", *zipkinURL)
			//tracer = zipkinot.Wrap(zipkinTracer)
			//zipkinTracer = nil // do not instrument with both native tracer and opentracing bridge
		} else if *appdashAddr != "" {
			logger.Log("tracer", "Appdash", "addr", *appdashAddr)
			tracer = appdashot.NewTracer(appdash.NewRemoteCollector(*appdashAddr))
		} else {
			tracer = stdopentracing.GlobalTracer() // no-op
		}
	}

	// Create the (sparse) metrics we'll use in the service. They, too, are
	// dependencies that we pass to components that use them.
	var ints, chars metrics.Counter
	{
		// Business-level metrics.
		ints = prometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "n13t",
			Subsystem: "idm",
			Name:      "integers_summed",
			Help:      "Total count of integers summed via the Sum method.",
		}, []string{})
		chars = prometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "n13t",
			Subsystem: "idm",
			Name:      "characters_concatenated",
			Help:      "Total count of characters concatenated via the Concat method.",
		}, []string{})
	}
	var duration metrics.Histogram
	{
		// Endpoint-level metrics.
		duration = prometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "n13t",
			Subsystem: "idm",
			Name:      "request_duration_seconds",
			Help:      "Request duration in seconds.",
		}, []string{"method", "success"})
	}
	http.DefaultServeMux.Handle("/metrics", promhttp.Handler())

	var (
		db *sql.DB
		userRepo user.Repository
	)
	db = idm.NewPostgres()
	if db != nil {
		userRepo = userRepository.NewPostgresUserRepository(db)
	}

	var uma2RsClient *uma2.Config
	if os.Getenv("UMA2_ENABLED") == "true" {
		client, err := uma2.NewUma2CompleteConfig(&uma2.Config{
			ServerUrl: os.Getenv("UMA2_SERVER_URL"),
			Oauth2Config: &clientcredentials.Config{
				ClientID:     os.Getenv("CLIENT_ID"),
				ClientSecret: os.Getenv("CLIENT_SECRET"),
				Scopes:       []string{""},
				AuthStyle:    oauth2.AuthStyleAutoDetect,
			},
		})

		if err != nil {
			panic(err)
		}

		if client.WellKnownConfig == nil {
			panic("idm(uma2): load config failed")
		}

		uma2RsClient = client
	}

	cryptoProvider := idm.NewCryptoProvider(zipkinTracer)
	config := &idmService.Config{
		SecretKeys:				strings.Split(os.Getenv("SECRETS"), ","),
		IndexRepository:		nil,
		UserRepository: 		&userRepo,
		UserCachedRepository: 	nil,
		Uma2RsClient: 			uma2RsClient,
		ExternalCryptoProvider: &cryptoProvider,
		UMAEnabled: 			os.Getenv("UMA_ENABLED") == "true",
		TransitEnabled:			os.Getenv("TRANSIT_ENABLED") == "true",
		// tracers
		Tracer: tracer,
		ZipkinTracer: zipkinTracer,
	}

	// Build the layers of the service "onion" from the inside out. First, the
	// business logic service; then, the set of endpoints that wrap the service;
	// and finally, a series of concrete transport adapters. The adapters, like
	// the HTTP handler or the gRPC server, are the bridge between Go kit and
	// the interfaces that the transports expect. Note that we're not binding
	// them to ports or anything yet; we'll do that next.

	var service = idmService.New(config, logger, ints, chars)
	var endpoints = idmEndpoint.New(service, logger, duration, tracer, zipkinTracer)
	{

		//httpHandler    = idmTransport.NewHTTPHandler(endpoints, tracer, zipkinTracer, logger)
	}
	var grpcServer = idmTransport.NewGRPCServer(endpoints, tracer, zipkinTracer, logger)

	// Now we're to the part of the func main where we want to start actually
	// running things, like servers bound to listeners to receive connections.
	//
	// The method is the same for each component: add a new actor to the group
	// struct, which is a combination of 2 anonymous functions: the first
	// function actually runs the component, and the second function should
	// interrupt the first function and cause it to return. It's in these
	// functions that we actually bind the Go kit server/handler structs to the
	// concrete transports and run them.
	//
	// Putting each component into its own block is mostly for aesthetics: it
	// clearly demarcates the scope in which each listener/socket may be used.
	var g group.Group
	{
		// The debug listener mounts the http.DefaultServeMux, and serves up
		// stuff like the Prometheus metrics route, the Go debug and profiling
		// routes, and so on.
		debugListener, err := net.Listen("tcp", *debugAddr)
		if err != nil {
			logger.Log("transport", "debug/HTTP", "during", "Listen", "err", err)
			os.Exit(1)
		}
		g.Add(func() error {
			logger.Log("transport", "debug/HTTP", "addr", *debugAddr)
			return http.Serve(debugListener, http.DefaultServeMux)
		}, func(error) {
			debugListener.Close()
		})
	}
	{
		if os.Getenv("PORT") != "" {
			*grpcAddr = fmt.Sprintf(":%s", os.Getenv("PORT"))
		}

		// The gRPC listener mounts the Go kit gRPC server we created.
		grpcListener, err := net.Listen("tcp", *grpcAddr)
		if err != nil {
			logger.Log("transport", "gRPC", "during", "Listen", "err", err)
			os.Exit(1)
		}

		g.Add(func() error {
			var opts []grpc.ServerOption
			if os.Getenv("TLS_ENABLED") != "" {
				if os.Getenv("TLS_ENABLED") == "true" {
					*tlsEnabled = true
				} else {
					*tlsEnabled = false
				}
			}
			if *tlsEnabled {
				fmt.Printf("TLS MODE\n")
				if os.Getenv("CERT_FILE") != "" {
					*certFile = os.Getenv("CERT_FILE")
				}
				if os.Getenv("KEY_FILE") != "" {
					*keyFile = os.Getenv("KEY_FILE")
				}

				if tlsCrt := os.Getenv("TLS_CRT"); tlsCrt != "" {
					if decoded, err := base64.StdEncoding.DecodeString(tlsCrt); err != nil {
						panic(err)
					} else {
						// readonly
						f, err := os.OpenFile("/tmp/tls.crt", os.O_CREATE|os.O_RDWR, 0600)
						if err != nil {
							panic(err)
						}
						if _, err = f.Write(decoded); err != nil {
							panic(err)
						}
						if err := f.Close(); err != nil {
							panic(err)
						}
						*certFile = "/tmp/tls.crt"
					}
				}

				if tlsKey := os.Getenv("TLS_KEY"); tlsKey != "" {
					if decoded, err := base64.StdEncoding.DecodeString(tlsKey); err != nil {
						panic(err)
					} else {
						f, err := os.OpenFile("/tmp/tls.key", os.O_CREATE|os.O_RDWR, 0600)
						if err != nil {
							panic(err)
						}
						if _, err = f.Write(decoded); err != nil {
							panic(err)
						}
						if err := f.Close(); err != nil {
							panic(err)
						}
						*keyFile = "/tmp/tls.key"
					}
				}

				creds, err := credentials.NewServerTLSFromFile(*certFile, *keyFile)
				if err != nil {
					panic(fmt.Sprintf("Failed to generate credentials %v", err))
				}
				opts = []grpc.ServerOption{grpc.Creds(creds)}
				credentials.NewTLS(&tls.Config{})
			}
			opts = append(opts, grpc.UnaryInterceptor(kitgrpc.Interceptor))

			logger.Log("transport", "gRPC", "addr", *grpcAddr)
			// we add the Go Kit gRPC Interceptor to our gRPC service as it is used by
			// the here demonstrated zipkin tracing middleware.
			baseServer := grpc.NewServer(opts...)
			idmPb.RegisterIdentityManagementServer(baseServer, grpcServer)
			grpc_health_v1.RegisterHealthServer(baseServer, &idm.HealthServer{ Db: db })
			return baseServer.Serve(grpcListener)
		}, func(error) {
			grpcListener.Close()
		})
	}
	{
		// This function just sits and waits for ctrl-C.
		cancelInterrupt := make(chan struct{})
		g.Add(func() error {
			c := make(chan os.Signal, 1)
			signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
			select {
			case sig := <-c:
				return fmt.Errorf("received signal %s", sig)
			case <-cancelInterrupt:
				return nil
			}
		}, func(error) {
			close(cancelInterrupt)
		})
	}
	logger.Log("exit", g.Run())
}

func usageFor(fs *flag.FlagSet, short string) func() {
	return func() {
		fmt.Fprintf(os.Stderr, "USAGE\n")
		fmt.Fprintf(os.Stderr, "  %s\n", short)
		fmt.Fprintf(os.Stderr, "\n")
		fmt.Fprintf(os.Stderr, "FLAGS\n")
		w := tabwriter.NewWriter(os.Stderr, 0, 2, 2, ' ', 0)
		fs.VisitAll(func(f *flag.Flag) {
			fmt.Fprintf(w, "\t-%s %s\t%s\n", f.Name, f.DefValue, f.Usage)
		})
		w.Flush()
		fmt.Fprintf(os.Stderr, "\n")
	}
}
