package _old

import (
	"fmt"
	log "github.com/hashicorp/go-hclog"
	"github.com/hashicorp/vault/api"
	"github.com/hashicorp/vault/audit"
	"github.com/hashicorp/vault/builtin/audit/file"
	"github.com/hashicorp/vault/builtin/logical/transit"
	"github.com/hashicorp/vault/http"
	"github.com/hashicorp/vault/sdk/helper/logging"
	"github.com/hashicorp/vault/sdk/logical"
	"github.com/hashicorp/vault/vault"
	"net"
	"testing"
)



func createTransitKey(client *api.Client, keyName string) error {
	data := map[string]interface{}{
		"type": "chacha20-poly1305",
	}

	_, err := client.Logical().Write(fmt.Sprintf("transit/keys/%s", keyName), data)
	if err != nil {
		//log.Fatal(err)
	}

	//log.Printf("created %s", keyName)

	return nil
}


//func TestVaultStuff(t *testing.T) {
//	tests := []struct{
//		plaintext string
//	}{
//		{plaintext: "abc@exmaple.com"},
//		{plaintext: "a bc d"},
//		{plaintext: "1 2 5 1 2"},
//	}
//
//
//	ln, client := createVault(t)
//	defer ln.Close()
//
//	// Pass the client to the code under test.
//	//myFunction(client)
//
//	keyName := "idm"
//
//	err := createTransitKey(client, keyName)
//	if err != nil {
//		log.Fatal(err)
//	}
//
//	result, err := encryptWithTransitEngine(client, keyName, "asdfasdf asdfasdf")
//
//	if  err != nil {
//		t.Fatal(err)
//	}
//
//	fmt.Printf("asdf %s", result)
//
//	for _, test := range tests {
//		result, err := encryptWithTransitEngine(client, keyName, test.plaintext)
//		if  err != nil {
//			t.Fatal(err)
//		}
//
//		decryptUserSecrets(client, keyName, result)
//
//	}
//
//	require.Equal(t, )
//}

func createVault(t *testing.T) (net.Listener, *api.Client) {
	t.Helper()

	// Create an in-memory, unsealed core (the "backend", if you will).
	core, keyShares, rootToken := vault.TestCoreUnsealedWithConfig(t, &vault.CoreConfig{
		LogicalBackends: map[string]logical.Factory{
			"transit": transit.Factory,
		},
		AuditBackends: map[string]audit.Factory{
			"file": file.Factory,
		},
		Logger: logging.NewVaultLogger(log.Error),
	})
	//core, keyShares, rootToken := vault.TestCoreUnsealed(t)
	_ = keyShares

	// Start an HTTP server for the core.
	ln, addr := http.TestServer(t, core)

	// Create a client that talks to the server, initially authenticating with
	// the root token.
	conf := api.DefaultConfig()
	conf.Address = addr

	client, err := api.NewClient(conf)
	if err != nil {
		t.Fatal(err)
	}
	client.SetToken(rootToken)

	// Setup required secrets, policies, etc.
	_, err = client.Logical().Write("secret/foo", map[string]interface{}{
		"secret": "bar",
	})
	if err != nil {
		t.Fatal(err)
	}


	err = client.Sys().Mount("transit", &api.MountInput{
		Type: "transit",
	})
	if err != nil {
		t.Fatal(err)
	}

	_, err = client.Logical().Write("transit/keys/abc", map[string]interface{}{
		"type": "chacha20-poly1305",
	})
	if err != nil {
		t.Fatal(err)
	}

	return ln, client
}
