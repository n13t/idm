package _old

import (
	"database/sql/driver"
	"github.com/DATA-DOG/go-sqlmock"
	//"github.com/golang/mock/gomock"
	//"gitlab.com/n13t/idm/mocks"
	idm "gitlab.com/n13t/protos/pkg/n13t/idm/v0"
	"log"
	"testing"

	_ "github.com/lib/pq"
)

type Any struct{}
func (a Any) Match(v driver.Value) bool { return true }

func TestCreateUser(t *testing.T)  {
	var tests = []struct{
		user *idm.User
	}{
		{user: &idm.User{Email: "abc@gmail.com"}},
	}


	err, vault := createVault(t)
	if err != nil {
		log.Fatal(err)
	}

	for _, test := range tests {
		CreateUser(nil, vault, test.user)
	}
}

func TestPostgresQuery(t *testing.T) {
	var tests = []struct{
		givenName string
		familyName string
		middleName string
		nickname string
	}{
		{"a", "nickname", "3", ""},
	}

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	for _, tt := range tests {
		t.Run("test", func(t *testing.T) {
			mock.ExpectBegin()
			mock.ExpectPrepare("INSERT INTO users").ExpectExec().
				WithArgs(Any{}, tt.givenName, tt.familyName, tt.middleName, tt.nickname).WillReturnResult(sqlmock.NewResult(1, 1))
			mock.ExpectCommit()

			// now we execute our method
			if err = CreateUser(db, nil, &idm.User{
				GivenName: tt.givenName,
				FamilyName: tt.familyName,
				MiddleName: tt.middleName,
				Nickname: tt.nickname,
			} ); err != nil {
				t.Errorf("error was not expected while updating stats: %s", err)
			}

			// we make sure that all expectations were met
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("there were unfulfilled expectations: %s", err)
			}
		})
	}
}

//func TestFoo(t *testing.T) {
//	ctrl := gomock.NewController(t)
//
//	// Assert that Bar() is invoked.
//	defer ctrl.Finish()
//
//	m := mocks.NewMockUserHandler(ctrl)
//
//	// Asserts that the first and only call to Bar() is passed 99.
//	// Anything else will fail.
//	m.
//		EXPECT().
//		CreateUser(gomock.Eq(99)).
//		Return(101)
//
//	//SUT(m)
//}

//func TestIntMinTableDriven(t *testing.T) {
//	var tests = []struct {
//		a, b int
//		want int
//	}{
//		{0, 1, 0},
//		{1, 0, 0},
//		{2, -2, -2},
//		{0, -1, -1},
//		{-1, 0, -1},
//	}
//	for _, tt := range tests {
//		testname := fmt.Sprintf("%d,%d", tt.a, tt.b)
//		t.Run(testname, func(t *testing.T) {
//			ans := IntMin(tt.a, tt.b)
//			if ans != tt.want {
//				t.Errorf("got %d, want %d", ans, tt.want)
//			}
//		})
//	}
//}