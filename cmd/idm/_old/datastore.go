package _old

import (
	"github.com/gomodule/redigo/redis"
	"github.com/jinzhu/gorm"
	"gitlab.com/n13t/idm/pkg/idmservice/user"
	userRepository "gitlab.com/n13t/idm/pkg/idmservice/user/repository"
	"net/url"
	"time"
)

var (
	db *gorm.DB
	redisPool *redis.Pool
)

func newUserRepository(databaseURL, redisUrl string) (user.Repository, error) {
	var (
		userRepo user.Repository
	)

	uri, err := url.Parse(databaseURL)
	if err != nil {
		panic("value must be a valid URI")
	} else if !uri.IsAbs() {
		panic("value must be absolute")
	}

	if uri, err := url.Parse(redisUrl); err != nil {
		panic("value must be a valid URI")
	} else if !uri.IsAbs() {
		panic("value must be absolute")
	}
	redisPool = &redis.Pool{
		MaxIdle: 3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (conn redis.Conn, err error) {
			return redis.Dial("tcp", redisUrl)
		},
	}

	switch uri.Scheme {
	case "postgres": {
		db, err = gorm.Open("postgres", databaseURL)
		if err != nil {
			panic("failed to connect database")
		}
		defer db.Close()

		userRepo = userRepository.NewPostgresUserRepository(db, redisPool)
	}
	}

	return userRepo, nil
}
