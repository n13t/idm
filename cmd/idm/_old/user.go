//go:generate bash -c "GO111MODULE=on mockgen -package=mocks -source=$GOFILE -destination=../../mocks/$GOFILE"

package _old

import (
	"database/sql"
	"fmt"
	"github.com/hashicorp/vault/api"
	_ "github.com/lib/pq"
	idmPb "gitlab.com/n13t/protos/pkg/n13t/idm/v0"
	"log"
	"reflect"

	"github.com/google/uuid"
)

type UserHandler interface {
	CreateUser(db *sql.DB, vault *api.Client, user *idmPb.User) (err error)
}

const (
	LIST_USERS_QUERY                    = "SELECT * FROM users;"
	CREATE_USER_QUERY_PREPARE_STATEMENT = `INSERT INTO users
											(
												sub, 
												given_name, 
												family_name, 
												middle_name, 
												nickname
											) 
											VALUES (?, ?, ?, ?, ?)`
	UPDATE_USER_QUERY = ""
	DELETE_USER_QUERY = ""
)

//func listUsers(db *sql.DB) (stream *idmPb.IdentityManagement_ListUsersStreamServer) {
//
//
//	db.Exec(LIST_USERS_QUERY)
//	return
//}

func encryptUserSecrets(user *idmPb.User, vault *api.Client) error {
	//var secretAttributes = [...]string{"BirthDate", "Email"}

	values := reflect.ValueOf(user)
	typeOf := values.Type()

	for i := 0; i < values.NumField(); i++ {
		fmt.Printf("%s=%s", typeOf.Field(i).Name, values.Field(i).Interface())
	}

	//key = encrypt(reflect.ValueOf(user).Field())
	//for _, key := range secretAttributes {
	//
	//}

	return nil
}

func decryptUserSecrets(user *idmPb.User) {
	// iteration through all attributes looking for encrypted test (vault:*)

}

func CreateUser(db *sql.DB, vault *api.Client, user *idmPb.User) (err error) {
	err = user.Validate()
	if err != nil {
		log.Fatal(err)
	}

	err = encryptUserSecrets(user, vault)
	if err != nil {
		log.Fatal(err)
	}

	txn, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}

	stmt, err := txn.Prepare(CREATE_USER_QUERY_PREPARE_STATEMENT)
	if err != nil {
		log.Fatal(err)
	}

	// Generate sub (UUIDv4)
	sub, err := uuid.NewRandom()
	if err != nil {
		log.Fatal(err)
	}

	_, err = stmt.Exec(sub.String(), user.GivenName, user.FamilyName, user.MiddleName, user.Nickname)
	if err != nil {
		log.Fatal(err)
	}

	err = stmt.Close()
	if err != nil {
		log.Fatal(err)
	}

	err = txn.Commit()
	if err != nil {
		log.Fatal(err)
	}

	// Create Blind Index

	return
}
