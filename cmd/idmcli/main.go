// https://golang.org/pkg/cmd/go/internal/generate/

package main

import (
	"bufio"
	"context"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/go-kit/kit/auth/jwt"
	"github.com/go-kit/kit/log"
	"gitlab.com/n13t/idm/pkg/idmcli"
	"gitlab.com/n13t/idm/pkg/idmtransport"
	"gitlab.com/n13t/idm/pkg/uma2"
	"golang.org/x/oauth2"
	"google.golang.org/grpc/credentials"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"text/tabwriter"
	"time"

	"google.golang.org/grpc"

	stdopentracing "github.com/opentracing/opentracing-go"

	"gitlab.com/n13t/idm/pkg/idmservice"
)

const (
	pageSize = 20
)

var tlsEnabled = false

func main() {
	command := os.Args[1]

	if command == "login" {
		fs := flag.NewFlagSet("login", flag.ExitOnError)
		var (
			authServerUrl		= fs.String("server_url", "", "")
			clientId			= fs.String("client_id", "", "")
		)
		if err := fs.Parse(os.Args[2:]); err != nil {
			panic(err)
		}

		serverCfg, err := uma2.GetWellKnownConfig(*authServerUrl)
		if err != nil {
			panic(err)
		}
		login(serverCfg.AuthorizationEndpoint, serverCfg.TokenEndpoint, *clientId)
		setConfig("SERVER_URL", *authServerUrl)
		setConfig("CLIENT_ID", *clientId)
		os.Exit(0)
	}

	var (
		svc idmservice.Service
		idmAddr = "localhost:5051"
		ctx = context.Background()
	)
	if addr := os.Getenv("IDM_ADDR"); addr != "" {
		idmAddr = addr
		port := strings.Split(addr, ":")[1]
		if port == "443" {
			tlsEnabled = true
		}
	}
	conn, err := grpc.Dial(idmAddr, createOpts()...)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v", err)
		os.Exit(1)
	}
	defer conn.Close()
	svc = idmtransport.NewGRPCClient(conn, stdopentracing.GlobalTracer(), nil, log.NewNopLogger())

	{
		serverUrl := getConfig("SERVER_URL")
		clientId := getConfig("CLIENT_ID")
		encodedToken := getConfig(AccessTokenCfg).(string)
		var token oauth2.Token
		_ = json.Unmarshal([]byte(encodedToken), &token)
		ctx = context.WithValue(context.Background(), jwt.JWTTokenContextKey, token.AccessToken)

		if serverUrl == nil || clientId == nil {
			panic("UNAUTHENTICATED")
		}

		if os.Getenv("UMA") != "" {
			uma2Client, er2r := uma2.NewUma2CompleteConfig(&uma2.Config{
				ServerUrl:    serverUrl.(string),
				Oauth2Config: &oauth2.Config{ClientID: clientId.(string)},
			})
			if er2r != nil {
				panic(er2r)
			}

			_ = uma2Client
			svc = idmcli.RetryService(svc, uma2Client, &token)
		}
	}



	switch command {
	case "search":
		a := os.Args[2]

		users, err := svc.Search(ctx, "",  a, 10, 0)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			os.Exit(1)
		}

		for _, u := range users {
			b, _ := json.MarshalIndent(u, "", "  ")
			fmt.Fprintf(os.Stdout, "%s\n", string(b))
		}
	case "create":
		fs := flag.NewFlagSet("login", flag.ExitOnError)
		var (
			_		= fs.String("f", "", "")
		)
		if err := fs.Parse(os.Args[2:]); err != nil {
			panic(err)
		}

		switch resource := os.Args[2]; resource {
		default:
			a := fs.Args()[0]
			b := fs.Args()[1]
			c := fs.Args()[2]
			_, err := svc.CreateUser(ctx, a, b, c)
			if err != nil {
				fmt.Fprintf(os.Stderr, "error: %v\n", err)
				os.Exit(1)
			}
		}
	case "get":
		switch resource := os.Args[2]; resource {
		case "user":
			if len(os.Args) > 3 {
				uid := os.Args[3]
				users, err := svc.Search(ctx, "", fmt.Sprintf("id=%s", uid), 1, 0)
				if err != nil {
					panic(err)
				}
				if len(users) > 0 {
					s, _ := json.MarshalIndent(users[0], "", "  ")
					fmt.Println(string(s))
				}
			} else {
				var pageToken string

				// initialize tabwriter
				w := new(tabwriter.Writer)

				// minwidth, tabwidth, padding, padchar, flags
				w.Init(os.Stdout, 8, 8, 1, '\t', 0)



				fmt.Fprintf(w, "%s\t%s\t%s\t%s\t%s\t%s\n", "ID", "USERNAME", "EMAIL", "NAME", "LAST_UPDATE", "CREATED_AT")
				//fmt.Fprintf(w, "\n %s\t%s\t%s\t", "----", "----", "----")

				for true {
					users, nextPageToken, err := svc.ListUsers(ctx, pageSize,  pageToken)
					if err != nil {
						fmt.Fprintf(os.Stderr, "error: %v\n", err)
						os.Exit(1)
					}

					for _, u := range users {
						var (
							lastUpdate string
							createdAt string
						)
						if !u.UpdatedAt.IsZero() {
							lastUpdate = u.UpdatedAt.Local().Format("2006-01-02 15:04:05")
						}
						createdAt =	u.CreatedAt.Local().Format("2006-01-02 15:04:05")

						fmt.Fprintf(w, "%s\t%s\t%s\t%s %s\t%s\t%s\n",
							u.ID, u.Username, u.Email, u.FamilyName, u.GivenName,
							lastUpdate,
							createdAt,
						)
						//b, _ := json.MarshalIndent(u, "", "  ")
						//fmt.Fprintf(os.Stdout, "%s\n", string(b))
					}
					w.Flush()

					if nextPageToken == "" {
						break
					}

					pageToken = nextPageToken
					bufio.NewReader(os.Stdin).ReadBytes('\n')
				}
			}
		}
	case "edit":
		switch resource := os.Args[2]; resource {
		case "user":
			if len(os.Args) > 3 {
				editor := os.Getenv("EDITOR")
				if editor == "" {
					fmt.Println("No editor found!")
					os.Exit(1)
				}

				uid := os.Args[3]
				users, err := svc.Search(ctx, "", fmt.Sprintf("id=%s", uid), 1, 0)
				if err != nil {
					panic(err)
				}
				if len(users) > 0 {
					user := users[0]
					s, _ := json.MarshalIndent(user, "", "  ")
					fmt.Println(string(s))

					filename := fmt.Sprintf("%d_%s.json", time.Now().Unix(), uid)
					file, err := os.Create(filepath.Join(cfgDir, filename))

					if err != nil {
						os.Exit(1)
					}

					file.WriteString(string(s))
					file.Close()


					cmd := exec.Command(editor, filepath.Join(cfgDir, filename))
					cmd.Stdin = os.Stdin
					cmd.Stdout = os.Stdout
					err = cmd.Run()
					if err != nil {
						fmt.Println(err)
						os.Exit(1)
					}

					// Read file
					content, err := ioutil.ReadFile(filepath.Join(cfgDir, filename))
					if err != nil {
						panic(err)
					}

					// Delete file
					_ = os.Remove(filepath.Join(cfgDir, filename))

					// Update user
					var u idmservice.User
					err = json.Unmarshal(content, &u)
					if err != nil {
						panic(err)
					}

					if u.Email == user.Email { u.Email = "" }
					if u.PhoneNumber == user.PhoneNumber { u.PhoneNumber = "" }

					_, err = svc.UpdateUser(
						ctx,
						u.ID,
						u.HashedPassword,
						u.GivenName,
						u.FamilyName,
						u.MiddleName,
						u.Nickname,
						u.Gender,
						u.Birthday,
						u.ZoneInfo,
						u.Locale,
						u.PhoneNumber,
						u.ProfileURL,
						u.PictureURL,
						u.WebsiteURL,
						u.Email,
						u.StreetAddress,
						u.Locale,
						u.Region,
						u.PostalCode,
						u.Country,
						u.EmailVerified,
						u.PhoneNumberVerified)
					if err != nil {
						panic(err)
					} else {
						fmt.Println("Update successfully")
					}
				}
			}
		}
	case "delete":
		switch resource := os.Args[2]; resource {
		case "user":
			if len(os.Args) > 3 {

				uid := os.Args[3]
				err := svc.DeleteUser(ctx, uid)
				if err != nil {
					panic(err)
				}
			}
		}
	}
}

func createOpts() []grpc.DialOption {
	var opts []grpc.DialOption
	if tlsEnabled || os.Getenv("TLS") == "true" {
		if os.Getenv("SKIP_TLS") == "true"{
			opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{InsecureSkipVerify: true})))
		} else {
			if os.Getenv("CA_FILE") != "" && os.Getenv("CA_CRT") != "" {
				caFile := os.Getenv("CA_FILE")
				if caCrt := os.Getenv("CA_CRT"); caCrt != "" {
					if decoded, err := base64.StdEncoding.DecodeString(caCrt); err != nil {
						panic(err)
					} else {
						f, err := os.OpenFile("/tmp/ca.crt", os.O_CREATE|os.O_RDWR, 0600)
						if err != nil {
							panic(err)
						}
						if _, err = f.Write(decoded); err != nil {
							panic(err)
						}
						if err := f.Close(); err != nil {
							panic(err)
						}
						caFile = "/tmp/ca.crt"
					}
				}

				creds, err := credentials.NewClientTLSFromFile(caFile, os.Getenv("SERVER_HOST_OVERRIDE"))
				if err != nil {
					panic(fmt.Sprintf("Failed to create TLS credentials %v", err))
				}
				opts = append(opts, grpc.WithTransportCredentials(creds))
			} else {
				opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{})))
			}
		}
	} else {
		opts = append(opts, grpc.WithInsecure())
	}

	opts = append(opts, grpc.WithBlock())
	opts = append(opts, grpc.WithTimeout(1 * time.Second))

	return opts
}
