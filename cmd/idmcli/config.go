package main

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
)

/**
 * Configuration Storage (access token, etc)
 */

const (
	ConfigFile = "config.yaml"
	AccessTokenCfg = "accessToken"
	configDirectoryFormat = "%s/.n13t/idmcli"
)

var cfgDir string

func init()  {
	home, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}

	cfgDir = fmt.Sprintf(configDirectoryFormat, home)
	err = os.MkdirAll(cfgDir, os.ModePerm)
	if err != nil {
		panic(err)
	}

	viper.SetConfigName(ConfigFile)
	viper.SetConfigType("yaml")
	viper.AddConfigPath(cfgDir)
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
		} else {
			// Config file was found but another error was produced
		}
	}
	_ = viper.SafeWriteConfig()
}

func setConfig(key string, value interface{}) error {
	viper.Set(key, value)
	return viper.WriteConfig()
}

func getConfig(key string) interface{} { return viper.Get(key) }
