# [0.3.0](https://gitlab.com/n13t/idm/compare/v0.2.0...v0.3.0) (2020-08-17)


### Bug Fixes

* **core:** add input validate for CreateUser ([aea8160](https://gitlab.com/n13t/idm/commit/aea8160ba7d97a722f328214fc40d81550fec1ad))
* **core:** implement DeleteUser rpc ([237fde1](https://gitlab.com/n13t/idm/commit/237fde1c52c623571c1a9f42247a0139141b1a99))
* **idm:** allow override service name ([7c09b9a](https://gitlab.com/n13t/idm/commit/7c09b9a5079e653905b6dddb28d0f0acefae420c))
* **idmcli:** support skip tls verify ([3819126](https://gitlab.com/n13t/idm/commit/3819126dc40c4091362a04954e5a30c40ab77c99))
* **mescli:** use tablewriter ([57af086](https://gitlab.com/n13t/idm/commit/57af0862e6c932507db479799f73d34207756bea))
* **plugin:** building sync user ([de8970c](https://gitlab.com/n13t/idm/commit/de8970cebbe1d9efff94d8a8847dbd88ccbf05fa))
* **plugin:** support config settings on Keycloak UI ([23f6ff2](https://gitlab.com/n13t/idm/commit/23f6ff2bd31edc7dd79c51e630585bae2285f24d))


### Features

* **core:** add ListUsers rpc ([9e86b15](https://gitlab.com/n13t/idm/commit/9e86b15b0b66927ee27a3625c7d3394d545d26af))

# [0.2.0](https://gitlab.com/n13t/idm/compare/v0.1.2...v0.2.0) (2020-04-08)


### Bug Fixes

* **core:** support run ES mode without TLS, return error for key not found ([f54f3fa](https://gitlab.com/n13t/idm/commit/f54f3fab5139dc49e061514f29a4648b95dce09a))
* **core:** transit engine implementation ([11e560a](https://gitlab.com/n13t/idm/commit/11e560ab054a10290222ddc999e89b513f926bf3))
* **idm:** configurate for encryption service mode ([9b12c52](https://gitlab.com/n13t/idm/commit/9b12c522753d679477a3c70a738bbc189c882f23))
* **idm:** support load tls from env variables ([f0bb145](https://gitlab.com/n13t/idm/commit/f0bb1458c4099aca54d576f777be7e60344f62df))


### Features

* **crypto:** support running as an encrypt service ([e3f3e4c](https://gitlab.com/n13t/idm/commit/e3f3e4cd73f93b035201abaa67507b54cae77f68))

## [0.1.2](https://gitlab.com/n13t/idm/compare/v0.1.1...v0.1.2) (2020-04-05)


### Bug Fixes

* **connection:** fix shutdown() execution to handle properly ([dc964dc](https://gitlab.com/n13t/idm/commit/dc964dccad2945f067fb992e24d4bed4e6d08b0d))
* **plugin:** cleanup pom.xml ([c66fa35](https://gitlab.com/n13t/idm/commit/c66fa35bf5d8c13b09c3d83b1dc7324e961bd48b))
* **plugin:** connection state ([590e4b2](https://gitlab.com/n13t/idm/commit/590e4b28859f493bfb557eeb1c2fb8dc9902bfdc))
* **plugin:** connection unavailabe ([d762221](https://gitlab.com/n13t/idm/commit/d762221a7af5f74be2ccd48a1afcc168e0e79f02))
* **plugin:** grpc connection error ([6d0b33d](https://gitlab.com/n13t/idm/commit/6d0b33d52d17976b720035f912593586b94e16bc))
* **plugin:** package with keycloak dependencies ([b252adc](https://gitlab.com/n13t/idm/commit/b252adcfaa2eacbb56b8e5d39084a2a4865ef983))
* **plugin:** protocol mapper ([3ce0c75](https://gitlab.com/n13t/idm/commit/3ce0c75c9cf1ce99d88e26b37344321d74d76ad1))
* **plugin:** support oidc protocol mapper ([c80188e](https://gitlab.com/n13t/idm/commit/c80188e94b6aee76a5c74956400d321fe49cbd08))
* **transport:** search response missing username ([752562d](https://gitlab.com/n13t/idm/commit/752562d09bc8b728a20d40fb3c25c40a443301f3))

## [0.1.1](https://gitlab.com/n13t/idm/compare/v0.1.0...v0.1.1) (2020-04-02)


### Bug Fixes

* **plugin:** keycloak plugin deployment config ([df698ff](https://gitlab.com/n13t/idm/commit/df698fff1c22150fd1559b5e53864a16413dc457))
