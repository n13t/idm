##
# https://github.com/GoogleContainerTools/distroless#examples-with-docker
##

# Build image
FROM golang:1.13-buster as build

WORKDIR /go/src/app

RUN GRPC_HEALTH_PROBE_VERSION=v0.3.2 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe

COPY go.* ./
RUN go mod download         ## Go 1.11+
COPY . .
RUN go build -o /go/bin/idm ./cmd/idm

# Release image
#FROM gcr.io/distroless/base-debian10
#
#COPY --from=build /bin/grpc_health_probe /
#COPY --from=build /go/bin/app /
#CMD ["/idm", "--grpc-addr=:8082"]

# Bitnami base image
FROM bitnami/minideb:stretch
COPY --from=build /bin/grpc_health_probe /
COPY --from=build /go/bin/idm /
RUN useradd -r -u 1001 -g root nonroot
RUN chown nonroot /grpc_health_probe
RUN chown nonroot /idm
USER nonroot

CMD ["/idm", "--grpc-addr=:8082"]
