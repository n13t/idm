#!/usr/bin/env bash

#export PRODUCTION_A=B
#export PRODUCTION_DATABASE_URL=postgres://pg_user:pg_password@production-postgres:5432/keycloak
## Parser Variables by Scope (PRODUCTION|STAGING)
#export CI_ENVIRONMENT_SLUG=production
env_slug=$(echo ${CI_ENVIRONMENT_SLUG//-/_} | tr -s '[:lower:]' '[:upper:]')

echo "Read environemt by scope/prefix=$env_slug"
echo "## SET environment variables ##"

env | grep -oP "^${env_slug}_\K.*" | xargs -I % sh -c "echo export %"
#eval $(env | grep -oP "^${env_slug}_\K.*" | xargs -I % sh -c "echo export %")
eval $(env | grep -oP "^${env_slug}_\K.*" | xargs -I % sh -c "echo export %")

echo "## Done! ##"

## Parse DATABASE_URL into multiple variables that uses by the application
# extract the protocol
proto="`echo $DATABASE_URL | grep '://' | sed -e's,^\(.*://\).*,\1,g'`"
# remove the protocol
url=`echo $DATABASE_URL | sed -e s,$proto,,g`

# extract the user and password (if any)
userpass="`echo $url | grep @ | cut -d@ -f1`"
pass=`echo $userpass | grep : | cut -d: -f2`
if [ -n "$pass" ]; then
    user=`echo $userpass | grep : | cut -d: -f1`
else
    user=$userpass
fi

# extract the host -- updated
hostport=`echo $url | sed -e s,$userpass@,,g | cut -d/ -f1`
port=`echo $hostport | grep : | cut -d: -f2`
if [ -n "$port" ]; then
    host=`echo $hostport | grep : | cut -d: -f1`
else
    host=$hostport
fi

# extract the path (if any)
path="`echo $url | grep / | cut -d/ -f2-`"
proto=`echo $proto | cut -d: -f1`


## SET Keycloak variables
export DB_VENDOR=$proto
export DB_ADDR=$host
export DB_PORT=$port
export POSTGRES_PORT=$DB_PORT       # Backward compatible (Keycloak may use this variable)
export DB_DATABASE=$path
export DB_SCHEMA=public
export DB_USER=$user
export DB_PASSWORD=$pass

echo $DB_VENDOR
echo $DB_ADDR
echo $DB_PORT
echo $DB_DATABASE
echo $DB_SCHEMA
echo $DB_USER
echo $DB_PASSWORD

/opt/jboss/tools/docker-entrypoint.sh $@
