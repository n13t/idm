#!/bin/sh
set -e

N13T_PROTOS_VERSION=v0.7.23
NAMELY_PROTOC_VERSION=1.28_1

CACHE_DIR="`pwd`/cache"
WORKDIR=$CACHE_DIR/n13t/protos

get_protos() {
  if ! [ -f $CACHE_DIR/protos-$N13T_PROTOS_VERSION.zip ]; then
    mkdir -p $CACHE_DIR
    curl -q https://gitlab.com/n13t/protos/-/archive/$N13T_PROTOS_VERSION/protos-$N13T_PROTOS_VERSION.zip -o $CACHE_DIR/protos-$N13T_PROTOS_VERSION.zip
  fi

  if ! [ -d $CACHE_DIR/n13t/protos ]; then
    mkdir -p $CACHE_DIR/n13t
    unzip -qo $CACHE_DIR/protos-$N13T_PROTOS_VERSION.zip -d $CACHE_DIR/n13t
    mv -f $CACHE_DIR/n13t/protos-$N13T_PROTOS_VERSION $CACHE_DIR/n13t/protos
  fi
#  ls -la $WORKDIR
}

compile_protos() {
  docker run --rm -v $WORKDIR:/opt/include/n13t -w /opt/include/n13t "namely/protoc-all:$NAMELY_PROTOC_VERSION" -d /opt/include/n13t/idm -l java -o java --with-validator
}

update_generated_stubs() {
  sudo chown -R $USER $CACHE_DIR
  cp -R $CACHE_DIR/n13t/protos/java/ src/main/
}

rm -rf plugins/keycloak/src/main/java/n13t/idm/v0/

get_protos
compile_protos
update_generated_stubs

rm -rf $CACHE_DIR/n13t
