package n13t.idm.v0;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 **
 * Identity Management Service - manage user's private information
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.28.1-SNAPSHOT)",
    comments = "Source: n13t/idm/v0/idm.proto")
public final class IdentityManagementGrpc {

  private IdentityManagementGrpc() {}

  public static final String SERVICE_NAME = "n13t.idm.v0.IdentityManagement";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<n13t.idm.v0.Idm.SumRequest,
      n13t.idm.v0.Idm.SumReply> getSumMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Sum",
      requestType = n13t.idm.v0.Idm.SumRequest.class,
      responseType = n13t.idm.v0.Idm.SumReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<n13t.idm.v0.Idm.SumRequest,
      n13t.idm.v0.Idm.SumReply> getSumMethod() {
    io.grpc.MethodDescriptor<n13t.idm.v0.Idm.SumRequest, n13t.idm.v0.Idm.SumReply> getSumMethod;
    if ((getSumMethod = IdentityManagementGrpc.getSumMethod) == null) {
      synchronized (IdentityManagementGrpc.class) {
        if ((getSumMethod = IdentityManagementGrpc.getSumMethod) == null) {
          IdentityManagementGrpc.getSumMethod = getSumMethod =
              io.grpc.MethodDescriptor.<n13t.idm.v0.Idm.SumRequest, n13t.idm.v0.Idm.SumReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Sum"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.Idm.SumRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.Idm.SumReply.getDefaultInstance()))
              .setSchemaDescriptor(new IdentityManagementMethodDescriptorSupplier("Sum"))
              .build();
        }
      }
    }
    return getSumMethod;
  }

  private static volatile io.grpc.MethodDescriptor<n13t.idm.v0.Idm.ConcatRequest,
      n13t.idm.v0.Idm.ConcatReply> getConcatMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Concat",
      requestType = n13t.idm.v0.Idm.ConcatRequest.class,
      responseType = n13t.idm.v0.Idm.ConcatReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<n13t.idm.v0.Idm.ConcatRequest,
      n13t.idm.v0.Idm.ConcatReply> getConcatMethod() {
    io.grpc.MethodDescriptor<n13t.idm.v0.Idm.ConcatRequest, n13t.idm.v0.Idm.ConcatReply> getConcatMethod;
    if ((getConcatMethod = IdentityManagementGrpc.getConcatMethod) == null) {
      synchronized (IdentityManagementGrpc.class) {
        if ((getConcatMethod = IdentityManagementGrpc.getConcatMethod) == null) {
          IdentityManagementGrpc.getConcatMethod = getConcatMethod =
              io.grpc.MethodDescriptor.<n13t.idm.v0.Idm.ConcatRequest, n13t.idm.v0.Idm.ConcatReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Concat"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.Idm.ConcatRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.Idm.ConcatReply.getDefaultInstance()))
              .setSchemaDescriptor(new IdentityManagementMethodDescriptorSupplier("Concat"))
              .build();
        }
      }
    }
    return getConcatMethod;
  }

  private static volatile io.grpc.MethodDescriptor<n13t.idm.v0.UserOuterClass.CreateUserRequest,
      n13t.idm.v0.UserOuterClass.CreateUserResponse> getCreateUserMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "CreateUser",
      requestType = n13t.idm.v0.UserOuterClass.CreateUserRequest.class,
      responseType = n13t.idm.v0.UserOuterClass.CreateUserResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<n13t.idm.v0.UserOuterClass.CreateUserRequest,
      n13t.idm.v0.UserOuterClass.CreateUserResponse> getCreateUserMethod() {
    io.grpc.MethodDescriptor<n13t.idm.v0.UserOuterClass.CreateUserRequest, n13t.idm.v0.UserOuterClass.CreateUserResponse> getCreateUserMethod;
    if ((getCreateUserMethod = IdentityManagementGrpc.getCreateUserMethod) == null) {
      synchronized (IdentityManagementGrpc.class) {
        if ((getCreateUserMethod = IdentityManagementGrpc.getCreateUserMethod) == null) {
          IdentityManagementGrpc.getCreateUserMethod = getCreateUserMethod =
              io.grpc.MethodDescriptor.<n13t.idm.v0.UserOuterClass.CreateUserRequest, n13t.idm.v0.UserOuterClass.CreateUserResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "CreateUser"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.UserOuterClass.CreateUserRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.UserOuterClass.CreateUserResponse.getDefaultInstance()))
              .setSchemaDescriptor(new IdentityManagementMethodDescriptorSupplier("CreateUser"))
              .build();
        }
      }
    }
    return getCreateUserMethod;
  }

  private static volatile io.grpc.MethodDescriptor<n13t.idm.v0.UserOuterClass.UpdateUserRequest,
      n13t.idm.v0.UserOuterClass.UpdateUserResponse> getUpdateUserMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "UpdateUser",
      requestType = n13t.idm.v0.UserOuterClass.UpdateUserRequest.class,
      responseType = n13t.idm.v0.UserOuterClass.UpdateUserResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<n13t.idm.v0.UserOuterClass.UpdateUserRequest,
      n13t.idm.v0.UserOuterClass.UpdateUserResponse> getUpdateUserMethod() {
    io.grpc.MethodDescriptor<n13t.idm.v0.UserOuterClass.UpdateUserRequest, n13t.idm.v0.UserOuterClass.UpdateUserResponse> getUpdateUserMethod;
    if ((getUpdateUserMethod = IdentityManagementGrpc.getUpdateUserMethod) == null) {
      synchronized (IdentityManagementGrpc.class) {
        if ((getUpdateUserMethod = IdentityManagementGrpc.getUpdateUserMethod) == null) {
          IdentityManagementGrpc.getUpdateUserMethod = getUpdateUserMethod =
              io.grpc.MethodDescriptor.<n13t.idm.v0.UserOuterClass.UpdateUserRequest, n13t.idm.v0.UserOuterClass.UpdateUserResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "UpdateUser"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.UserOuterClass.UpdateUserRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.UserOuterClass.UpdateUserResponse.getDefaultInstance()))
              .setSchemaDescriptor(new IdentityManagementMethodDescriptorSupplier("UpdateUser"))
              .build();
        }
      }
    }
    return getUpdateUserMethod;
  }

  private static volatile io.grpc.MethodDescriptor<n13t.idm.v0.Search.SearchRequest,
      n13t.idm.v0.Search.SearchResponse> getSearchMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Search",
      requestType = n13t.idm.v0.Search.SearchRequest.class,
      responseType = n13t.idm.v0.Search.SearchResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<n13t.idm.v0.Search.SearchRequest,
      n13t.idm.v0.Search.SearchResponse> getSearchMethod() {
    io.grpc.MethodDescriptor<n13t.idm.v0.Search.SearchRequest, n13t.idm.v0.Search.SearchResponse> getSearchMethod;
    if ((getSearchMethod = IdentityManagementGrpc.getSearchMethod) == null) {
      synchronized (IdentityManagementGrpc.class) {
        if ((getSearchMethod = IdentityManagementGrpc.getSearchMethod) == null) {
          IdentityManagementGrpc.getSearchMethod = getSearchMethod =
              io.grpc.MethodDescriptor.<n13t.idm.v0.Search.SearchRequest, n13t.idm.v0.Search.SearchResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Search"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.Search.SearchRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.Search.SearchResponse.getDefaultInstance()))
              .setSchemaDescriptor(new IdentityManagementMethodDescriptorSupplier("Search"))
              .build();
        }
      }
    }
    return getSearchMethod;
  }

  private static volatile io.grpc.MethodDescriptor<n13t.idm.v0.Idm.IsValidRequest,
      n13t.idm.v0.Idm.IsValidResponse> getIsValidMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "IsValid",
      requestType = n13t.idm.v0.Idm.IsValidRequest.class,
      responseType = n13t.idm.v0.Idm.IsValidResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<n13t.idm.v0.Idm.IsValidRequest,
      n13t.idm.v0.Idm.IsValidResponse> getIsValidMethod() {
    io.grpc.MethodDescriptor<n13t.idm.v0.Idm.IsValidRequest, n13t.idm.v0.Idm.IsValidResponse> getIsValidMethod;
    if ((getIsValidMethod = IdentityManagementGrpc.getIsValidMethod) == null) {
      synchronized (IdentityManagementGrpc.class) {
        if ((getIsValidMethod = IdentityManagementGrpc.getIsValidMethod) == null) {
          IdentityManagementGrpc.getIsValidMethod = getIsValidMethod =
              io.grpc.MethodDescriptor.<n13t.idm.v0.Idm.IsValidRequest, n13t.idm.v0.Idm.IsValidResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "IsValid"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.Idm.IsValidRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  n13t.idm.v0.Idm.IsValidResponse.getDefaultInstance()))
              .setSchemaDescriptor(new IdentityManagementMethodDescriptorSupplier("IsValid"))
              .build();
        }
      }
    }
    return getIsValidMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static IdentityManagementStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<IdentityManagementStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<IdentityManagementStub>() {
        @java.lang.Override
        public IdentityManagementStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new IdentityManagementStub(channel, callOptions);
        }
      };
    return IdentityManagementStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static IdentityManagementBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<IdentityManagementBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<IdentityManagementBlockingStub>() {
        @java.lang.Override
        public IdentityManagementBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new IdentityManagementBlockingStub(channel, callOptions);
        }
      };
    return IdentityManagementBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static IdentityManagementFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<IdentityManagementFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<IdentityManagementFutureStub>() {
        @java.lang.Override
        public IdentityManagementFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new IdentityManagementFutureStub(channel, callOptions);
        }
      };
    return IdentityManagementFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   **
   * Identity Management Service - manage user's private information
   * </pre>
   */
  public static abstract class IdentityManagementImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Sums two integers.
     * </pre>
     */
    public void sum(n13t.idm.v0.Idm.SumRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.Idm.SumReply> responseObserver) {
      asyncUnimplementedUnaryCall(getSumMethod(), responseObserver);
    }

    /**
     * <pre>
     * Concatenates two strings
     * </pre>
     */
    public void concat(n13t.idm.v0.Idm.ConcatRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.Idm.ConcatReply> responseObserver) {
      asyncUnimplementedUnaryCall(getConcatMethod(), responseObserver);
    }

    /**
     */
    public void createUser(n13t.idm.v0.UserOuterClass.CreateUserRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.UserOuterClass.CreateUserResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getCreateUserMethod(), responseObserver);
    }

    /**
     */
    public void updateUser(n13t.idm.v0.UserOuterClass.UpdateUserRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.UserOuterClass.UpdateUserResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateUserMethod(), responseObserver);
    }

    /**
     * <pre>
     *    rpc DeleteUser(User) returns (google.protobuf.Empty) {
     *        option (google.api.http) = {
     *			delete: "/v0/users/{sub}"
     *		};
     *    }
     * </pre>
     */
    public void search(n13t.idm.v0.Search.SearchRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.Search.SearchResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getSearchMethod(), responseObserver);
    }

    /**
     * <pre>
     * for keycloak to check password
     * </pre>
     */
    public void isValid(n13t.idm.v0.Idm.IsValidRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.Idm.IsValidResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getIsValidMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSumMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                n13t.idm.v0.Idm.SumRequest,
                n13t.idm.v0.Idm.SumReply>(
                  this, METHODID_SUM)))
          .addMethod(
            getConcatMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                n13t.idm.v0.Idm.ConcatRequest,
                n13t.idm.v0.Idm.ConcatReply>(
                  this, METHODID_CONCAT)))
          .addMethod(
            getCreateUserMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                n13t.idm.v0.UserOuterClass.CreateUserRequest,
                n13t.idm.v0.UserOuterClass.CreateUserResponse>(
                  this, METHODID_CREATE_USER)))
          .addMethod(
            getUpdateUserMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                n13t.idm.v0.UserOuterClass.UpdateUserRequest,
                n13t.idm.v0.UserOuterClass.UpdateUserResponse>(
                  this, METHODID_UPDATE_USER)))
          .addMethod(
            getSearchMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                n13t.idm.v0.Search.SearchRequest,
                n13t.idm.v0.Search.SearchResponse>(
                  this, METHODID_SEARCH)))
          .addMethod(
            getIsValidMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                n13t.idm.v0.Idm.IsValidRequest,
                n13t.idm.v0.Idm.IsValidResponse>(
                  this, METHODID_IS_VALID)))
          .build();
    }
  }

  /**
   * <pre>
   **
   * Identity Management Service - manage user's private information
   * </pre>
   */
  public static final class IdentityManagementStub extends io.grpc.stub.AbstractAsyncStub<IdentityManagementStub> {
    private IdentityManagementStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected IdentityManagementStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new IdentityManagementStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sums two integers.
     * </pre>
     */
    public void sum(n13t.idm.v0.Idm.SumRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.Idm.SumReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSumMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * Concatenates two strings
     * </pre>
     */
    public void concat(n13t.idm.v0.Idm.ConcatRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.Idm.ConcatReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getConcatMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void createUser(n13t.idm.v0.UserOuterClass.CreateUserRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.UserOuterClass.CreateUserResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCreateUserMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateUser(n13t.idm.v0.UserOuterClass.UpdateUserRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.UserOuterClass.UpdateUserResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateUserMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     *    rpc DeleteUser(User) returns (google.protobuf.Empty) {
     *        option (google.api.http) = {
     *			delete: "/v0/users/{sub}"
     *		};
     *    }
     * </pre>
     */
    public void search(n13t.idm.v0.Search.SearchRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.Search.SearchResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSearchMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * for keycloak to check password
     * </pre>
     */
    public void isValid(n13t.idm.v0.Idm.IsValidRequest request,
        io.grpc.stub.StreamObserver<n13t.idm.v0.Idm.IsValidResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getIsValidMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   **
   * Identity Management Service - manage user's private information
   * </pre>
   */
  public static final class IdentityManagementBlockingStub extends io.grpc.stub.AbstractBlockingStub<IdentityManagementBlockingStub> {
    private IdentityManagementBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected IdentityManagementBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new IdentityManagementBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sums two integers.
     * </pre>
     */
    public n13t.idm.v0.Idm.SumReply sum(n13t.idm.v0.Idm.SumRequest request) {
      return blockingUnaryCall(
          getChannel(), getSumMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * Concatenates two strings
     * </pre>
     */
    public n13t.idm.v0.Idm.ConcatReply concat(n13t.idm.v0.Idm.ConcatRequest request) {
      return blockingUnaryCall(
          getChannel(), getConcatMethod(), getCallOptions(), request);
    }

    /**
     */
    public n13t.idm.v0.UserOuterClass.CreateUserResponse createUser(n13t.idm.v0.UserOuterClass.CreateUserRequest request) {
      return blockingUnaryCall(
          getChannel(), getCreateUserMethod(), getCallOptions(), request);
    }

    /**
     */
    public n13t.idm.v0.UserOuterClass.UpdateUserResponse updateUser(n13t.idm.v0.UserOuterClass.UpdateUserRequest request) {
      return blockingUnaryCall(
          getChannel(), getUpdateUserMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     *    rpc DeleteUser(User) returns (google.protobuf.Empty) {
     *        option (google.api.http) = {
     *			delete: "/v0/users/{sub}"
     *		};
     *    }
     * </pre>
     */
    public n13t.idm.v0.Search.SearchResponse search(n13t.idm.v0.Search.SearchRequest request) {
      return blockingUnaryCall(
          getChannel(), getSearchMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * for keycloak to check password
     * </pre>
     */
    public n13t.idm.v0.Idm.IsValidResponse isValid(n13t.idm.v0.Idm.IsValidRequest request) {
      return blockingUnaryCall(
          getChannel(), getIsValidMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   **
   * Identity Management Service - manage user's private information
   * </pre>
   */
  public static final class IdentityManagementFutureStub extends io.grpc.stub.AbstractFutureStub<IdentityManagementFutureStub> {
    private IdentityManagementFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected IdentityManagementFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new IdentityManagementFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sums two integers.
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<n13t.idm.v0.Idm.SumReply> sum(
        n13t.idm.v0.Idm.SumRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSumMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * Concatenates two strings
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<n13t.idm.v0.Idm.ConcatReply> concat(
        n13t.idm.v0.Idm.ConcatRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getConcatMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<n13t.idm.v0.UserOuterClass.CreateUserResponse> createUser(
        n13t.idm.v0.UserOuterClass.CreateUserRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getCreateUserMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<n13t.idm.v0.UserOuterClass.UpdateUserResponse> updateUser(
        n13t.idm.v0.UserOuterClass.UpdateUserRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateUserMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     *    rpc DeleteUser(User) returns (google.protobuf.Empty) {
     *        option (google.api.http) = {
     *			delete: "/v0/users/{sub}"
     *		};
     *    }
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<n13t.idm.v0.Search.SearchResponse> search(
        n13t.idm.v0.Search.SearchRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSearchMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * for keycloak to check password
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<n13t.idm.v0.Idm.IsValidResponse> isValid(
        n13t.idm.v0.Idm.IsValidRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getIsValidMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SUM = 0;
  private static final int METHODID_CONCAT = 1;
  private static final int METHODID_CREATE_USER = 2;
  private static final int METHODID_UPDATE_USER = 3;
  private static final int METHODID_SEARCH = 4;
  private static final int METHODID_IS_VALID = 5;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final IdentityManagementImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(IdentityManagementImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SUM:
          serviceImpl.sum((n13t.idm.v0.Idm.SumRequest) request,
              (io.grpc.stub.StreamObserver<n13t.idm.v0.Idm.SumReply>) responseObserver);
          break;
        case METHODID_CONCAT:
          serviceImpl.concat((n13t.idm.v0.Idm.ConcatRequest) request,
              (io.grpc.stub.StreamObserver<n13t.idm.v0.Idm.ConcatReply>) responseObserver);
          break;
        case METHODID_CREATE_USER:
          serviceImpl.createUser((n13t.idm.v0.UserOuterClass.CreateUserRequest) request,
              (io.grpc.stub.StreamObserver<n13t.idm.v0.UserOuterClass.CreateUserResponse>) responseObserver);
          break;
        case METHODID_UPDATE_USER:
          serviceImpl.updateUser((n13t.idm.v0.UserOuterClass.UpdateUserRequest) request,
              (io.grpc.stub.StreamObserver<n13t.idm.v0.UserOuterClass.UpdateUserResponse>) responseObserver);
          break;
        case METHODID_SEARCH:
          serviceImpl.search((n13t.idm.v0.Search.SearchRequest) request,
              (io.grpc.stub.StreamObserver<n13t.idm.v0.Search.SearchResponse>) responseObserver);
          break;
        case METHODID_IS_VALID:
          serviceImpl.isValid((n13t.idm.v0.Idm.IsValidRequest) request,
              (io.grpc.stub.StreamObserver<n13t.idm.v0.Idm.IsValidResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class IdentityManagementBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    IdentityManagementBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return n13t.idm.v0.Idm.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("IdentityManagement");
    }
  }

  private static final class IdentityManagementFileDescriptorSupplier
      extends IdentityManagementBaseDescriptorSupplier {
    IdentityManagementFileDescriptorSupplier() {}
  }

  private static final class IdentityManagementMethodDescriptorSupplier
      extends IdentityManagementBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    IdentityManagementMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (IdentityManagementGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new IdentityManagementFileDescriptorSupplier())
              .addMethod(getSumMethod())
              .addMethod(getConcatMethod())
              .addMethod(getCreateUserMethod())
              .addMethod(getUpdateUserMethod())
              .addMethod(getSearchMethod())
              .addMethod(getIsValidMethod())
              .build();
        }
      }
    }
    return result;
  }
}
