/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package n13t.idm.keycloak.readonly_user_storage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.Option;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.internal.GrpcUtil;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import io.grpc.netty.shaded.io.netty.channel.ChannelOption;
import n13t.idm.v0.UserOuterClass;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.jboss.logging.Logger;
import org.keycloak.Config;
import org.keycloak.common.constants.KerberosConstants;
import org.keycloak.component.ComponentModel;
import org.keycloak.component.ComponentValidationException;
import org.keycloak.models.*;
import org.keycloak.models.cache.UserCache;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.provider.ProviderConfigurationBuilder;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.UserStorageProviderFactory;
import org.keycloak.storage.UserStorageProviderModel;
import org.keycloak.storage.user.ImportSynchronization;
import org.keycloak.storage.user.SynchronizationResult;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author <a href="mailto:bill@burkecentral.com">Bill Burke</a>
 * @version $Revision: 1 $
 */
public class IDMUserStorageProviderFactory implements
        UserStorageProviderFactory<IDMUserStorageProvider>
{
    private static final Logger logger = Logger.getLogger(IDMUserStorageProviderFactory.class);
    public static final String PROVIDER_NAME = "n13t-idm";

    protected static final List<ProviderConfigProperty> config;

    static {
        config = ProviderConfigurationBuilder.create()
                .property().name(IDMConstraints.IDM_SERVER_ADDR).type(ProviderConfigProperty.STRING_TYPE).label("IDM Server Addr").defaultValue("example.com:443").add()
                .property().name(IDMConstraints.IDM_TLS_ENABLED).type(ProviderConfigProperty.BOOLEAN_TYPE).label("IDM TLS ENABLED").defaultValue(false).add()
                .property().name(IDMConstraints.OIDC_SERVER_URL).type(ProviderConfigProperty.STRING_TYPE).label("OIDC Server URL").add()
                .property().name(IDMConstraints.OIDC_CLIENT_ID).type(ProviderConfigProperty.STRING_TYPE).label("OIDC Client ID").add()
                .property().name(IDMConstraints.OIDC_CLIENT_SECRET).type(ProviderConfigProperty.STRING_TYPE).label("OIDC Client Secret").add()
                .build();
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return config;
    }

    @Override
    public void validateConfiguration(KeycloakSession session, RealmModel realm, ComponentModel config) {
        String addr = config.getConfig().getFirst(IDMConstraints.IDM_SERVER_ADDR);
        String oidcServerURL = config.getConfig().getFirst(IDMConstraints.OIDC_SERVER_URL);
        if (addr.isEmpty()) throw new ComponentValidationException("IDM Server Addr MUST be defined");
        if (!oidcServerURL.isEmpty()) {
            String clientId = config.getConfig().getFirst(IDMConstraints.OIDC_CLIENT_ID);
            String clientSecret = config.getConfig().getFirst(IDMConstraints.OIDC_CLIENT_SECRET);

            if (clientId == null || clientSecret == null) {
                throw new ComponentValidationException("ID/Secret CANNOT be NULL when OIDC Server URL is set");
            } else {
                try {
                    OIDCClient client = new OIDCClient(oidcServerURL, clientId, clientSecret);
                    client.getAccessToken();
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new ComponentValidationException("CANNOT get OpenID well-known config from OIDC Server URL");
                }
            }
        }
    }

    @Override
    public void init(Config.Scope config) {
        logger.info(config);

    }

    @Override
    public IDMUserStorageProvider create(KeycloakSession session, ComponentModel model) {
        try {
            ManagedChannel channel;
            String tlsEnabled = model.getConfig().getFirst(IDMConstraints.IDM_TLS_ENABLED);
            String idmAddr = model.getConfig().getFirst(IDMConstraints.IDM_SERVER_ADDR);
            String oidcServerURL = model.getConfig().getFirst(IDMConstraints.OIDC_SERVER_URL);
            String clientId = model.getConfig().getFirst(IDMConstraints.OIDC_CLIENT_ID);
            String clientSecret = model.getConfig().getFirst(IDMConstraints.OIDC_CLIENT_SECRET);
            OIDCClient oidcClient = new OIDCClient(oidcServerURL, clientId, clientSecret);
            IDMClient idmClient;

            if (tlsEnabled.equals(Boolean.toString(true))) {
                channel = NettyChannelBuilder.forTarget(idmAddr).useTransportSecurity().build();
            } else {
                channel = NettyChannelBuilder
                        .forTarget(idmAddr)
                        .usePlaintext()
//                        .idleTimeout(10, TimeUnit.SECONDS)
//                        .withOption(ChannelOption.AUTO_CLOSE, true)
//                        .withOption(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10)
//                        .withOption(ChannelOption.SO_KEEPALIVE, false)
                        .build();
            }

            idmClient = new IDMClient(channel, oidcClient);

            IDMUserStorageProvider provider = new IDMUserStorageProvider(this, session, model, idmClient);
            return provider;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getId() { return PROVIDER_NAME; }

    @Override
    public String getHelpText() { return "Identity Management Service from <a href=\"https://gitlab.com/n13t\">N13T</a> "; }

    @Override
    public void close() { logger.info("<<<<<< Closing factory"); }
}
