package n13t.idm.keycloak.readonly_user_storage;

import io.grpc.*;
import n13t.idm.v0.*;
import n13t.idm.v0.UserOuterClass.User;
import n13t.idm.v0.IdentityManagementGrpc;
import org.jboss.logging.Logger;
import org.keycloak.models.RealmModel;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

public class IDMClient {
    private static final Logger logger = Logger.getLogger(IDMClient.class.getName());

    private final ManagedChannel channel;
    private final IdentityManagementGrpc.IdentityManagementBlockingStub blockingStub;
    private final IdentityManagementGrpc.IdentityManagementStub asyncStub;

    private final OIDCClient oidcClient;

    IDMClient(ManagedChannel channel, OIDCClient oidcClient) {
        this.channel = channel;
        this.oidcClient = oidcClient;
        blockingStub = IdentityManagementGrpc.newBlockingStub(channel).withCallCredentials(new CallCredentials() {
            @Override
            public void applyRequestMetadata(RequestInfo requestInfo, Executor executor, MetadataApplier metadataApplier) {
                Metadata headers = new Metadata();
                Metadata.Key<String> authKey = Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER);
                try {
                    headers.put(authKey, oidcClient.getAccessToken());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                metadataApplier.apply(headers);
            }

            @Override
            public void thisUsesUnstableApi() {}
        });
        asyncStub = IdentityManagementGrpc.newStub(channel);


    }

    public void shutdown() {
        channel.shutdownNow();
        try {
            channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    List<User> searchForUser(String search, RealmModel realm, int offset, int limit) {
        logger.info(String.format("[IdM] searchForUser(String search = %s, RealmModel realm = %s, int offset = %d, int limit = %d)", search, realm, offset, limit));

        ensureGRPCChannelConnection();
        Search.SearchResponse resp = Search.SearchResponse.newBuilder().build();

        try {
            resp = blockingStub.search(Search.SearchRequest.newBuilder().setQuery(String.format("%s", search)).build());
        } catch (StatusRuntimeException statusRuntimeException) {
            System.out.println(statusRuntimeException.getStatus());
            if (statusRuntimeException.getStatus().getCode() == Status.NOT_FOUND.getCode()) {
                return null;
            }
        }

        return resp.getUsersList();
    }

    User getUserById(String id) {
        logger.info("getUserById(String id)");

        ensureGRPCChannelConnection();
        Search.SearchResponse resp = Search.SearchResponse.newBuilder().build();

        try {
            resp = blockingStub.search(Search.SearchRequest.newBuilder().setQuery(String.format("id=%s", id)).build());
        } catch (StatusRuntimeException statusRuntimeException) {
            System.out.println(statusRuntimeException.getStatus());
            if (statusRuntimeException.getStatus().getCode() == Status.NOT_FOUND.getCode()) {
                return null;
            }
            shutdown();
        }

         return resp.getUsersList().get(0);
    }

    User getUserByUsername(String username) {
        logger.info("getUserByUsername(String username)");

        ensureGRPCChannelConnection();
        Search.SearchResponse resp = Search.SearchResponse.newBuilder().build();

        try {
            resp = blockingStub.search(Search.SearchRequest.newBuilder()
                    .setQuery(String.format("username=%s", username))
                    .setLimit(1)
                    .setOffset(0)
                    .build());
        } catch (StatusRuntimeException statusRuntimeException) {
            logger.log(Logger.Level.ERROR, statusRuntimeException.getStatus());
            if (statusRuntimeException.getStatus().getCode() == Status.NOT_FOUND.getCode()) {
                return null;
            }
            shutdown();
        }

        if (resp.getUsersList().isEmpty()) {
            return null;
        }

        return resp.getUsersList().get(0);
    }

    User getUserByEmail(String email) {
        logger.info("getUserByEmail(String email)");

        ensureGRPCChannelConnection();
        Search.SearchResponse resp = Search.SearchResponse.newBuilder().build();

        try {
            resp = blockingStub.search(Search.SearchRequest.newBuilder().setQuery(String.format("email=%s", email)).build());
        } catch (StatusRuntimeException statusRuntimeException) {
            System.out.println(statusRuntimeException.getStatus());
            if (statusRuntimeException.getStatus().getCode() == Status.NOT_FOUND.getCode()) {
                return null;
            }
            shutdown();
        }

        if (resp.getUsersList().isEmpty()) {
            return null;
        }

        return resp.getUsersList().get(0);
    }

    private void ensureGRPCChannelConnection() {
        ConnectivityState connectivityState = this.channel.getState(true);
        if (!connectivityState.equals(ConnectivityState.READY) && !connectivityState.equals(ConnectivityState.IDLE) ) {
            throw new Error(String.format("idm(channel): unexpected connection state\n\n%s\n", connectivityState.toString()));
        }
    }

    Boolean isValid(String id, String password) {
        logger.log(Logger.Level.INFO, String.format("isValid(String id=%s, String password=%s)", id, password));

        ensureGRPCChannelConnection();

        Idm.IsValidResponse resp = null;
        boolean valid = false;

        try {
            resp = blockingStub.isValid(Idm.IsValidRequest.newBuilder().setId(id).setPassword(password).build());
        } catch (StatusRuntimeException statusRuntimeException) {
            logger.log(Logger.Level.ERROR, String.format("idm(isValid): %s", statusRuntimeException));
            shutdown();
        }

        if (resp != null && resp.getV()) {
            valid = true;
        }

        return valid;
    }
}
