/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package n13t.idm.keycloak.readonly_user_storage;

import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import org.jboss.logging.Logger;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.credential.CredentialModel;
import org.keycloak.models.*;
import org.keycloak.models.cache.CachedUserModel;
import org.keycloak.models.cache.OnUserCache;
import org.keycloak.models.cache.UserCache;
import org.keycloak.models.utils.UserModelDelegate;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.UserStorageProviderModel;
import org.keycloak.storage.adapter.AbstractUserAdapter;
import org.keycloak.storage.user.*;


//import javax.ejb.Local;
//import javax.ejb.Remove;
//import javax.ejb.Stateful;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.TypedQuery;
import java.util.*;

/* gRPC Stub */
import n13t.idm.v0.UserOuterClass.User;

import static org.keycloak.storage.adapter.AbstractUserAdapterFederatedStorage.*;


/**
 * @author <a href="mailto:bill@burkecentral.com">Bill Burke</a>
 * @version $Revision: 1 $
 */
public class IDMUserStorageProvider implements
        UserStorageProvider,
        UserLookupProvider,
        CredentialInputValidator,
        ImportedUserValidation
{
    private static final Logger logger = Logger.getLogger(IDMUserStorageProvider.class.getName());
    protected Map<String, UserModel> loadedUsers = new HashMap<>();

    protected IDMUserStorageProviderFactory factory;
    protected ComponentModel model;
    protected KeycloakSession session;

    protected OIDCClient oidcClient;
    protected IDMClient idmClient;

    public IDMUserStorageProvider(IDMUserStorageProviderFactory factory, KeycloakSession session, ComponentModel model, IDMClient idmClient) {
        this.factory = factory;
        this.idmClient = idmClient;
        this.session = session;
        this.model = model;
    }

    protected void finalize( ) throws Throwable {
        this.idmClient.shutdown();
    }

    @Override
    public void preRemove(RealmModel realm) {

    }

    @Override
    public void preRemove(RealmModel realm, GroupModel group) {

    }

    @Override
    public void preRemove(RealmModel realm, RoleModel role) {

    }

    @Override
    public void close() {
    }

    /**
     * Required to support basic login
     */
    @Override
    public UserModel getUserById(String id, RealmModel realm) {
        logger.log(Logger.Level.INFO, String.format("getUserById(String id = %s, RealmModel realm = %s)", id, realm));

        StorageId storageId = new StorageId(id);
        String username = storageId.getExternalId();
        return getUserByUsername(username, realm);
    }

    @Override
    public UserModel getUserByUsername(String username, RealmModel realm)  {
        logger.log(Logger.Level.INFO, String.format("getUserByUsername(String username = %s, RealmModel realm = %s)", username, realm));
        UserModel adapter = loadedUsers.get(username);
        if (adapter == null) {
            adapter = createAdapter(realm, username);
            loadedUsers.put(username, adapter);
        }
        return adapter;
    }

    protected UserModel createAdapter(RealmModel realm, String username) {
        UserModel local = session.userLocalStorage().getUserByUsername(username, realm);
        if (local == null) {
            local = session.userLocalStorage().addUser(realm, username);
            local.setFederationLink(model.getId());
            User idmUser = idmClient.getUserByUsername(username);
            local.setUsername(idmUser.getUsername());
            local.setSingleAttribute(IDMConstraints.IDM_ID, idmUser.getSub());
            local.setEnabled(true);
        }

        return new UserModelDelegate(local) {
            @Override
            public void setUsername(String username) {
                logger.log(Logger.Level.INFO, String.format("UserModelDelegate::setUsername(String username = %s)", username));
                super.setUsername(username);
            }
        };
    }

    @Override
    public UserModel getUserByEmail(String email, RealmModel realm) {
        logger.log(Logger.Level.INFO, String.format("getUserByEmail(String email = %s, RealmModel realm = %s)", email, realm));
        User user = idmClient.getUserByEmail(email);
        return new UserAdapter(session, realm, model, user);
    }

    @Override
    public boolean isConfiguredFor(RealmModel realm, UserModel user, String credentialType) {
        logger.log(Logger.Level.INFO, String.format("isConfiguredFor(RealmModel realm, UserModel user, String credentialType)"));
        return credentialType.equals(CredentialModel.PASSWORD);
    }

    @Override
    public boolean supportsCredentialType(String credentialType) {
        logger.log(Logger.Level.INFO, String.format("supportsCredentialType(String credentialType = %s)", credentialType));
        return credentialType.equals(CredentialModel.PASSWORD);
    }

    /**
     * Call IsValid rpc to check password
     */
    @Override
    public boolean isValid(RealmModel realm, UserModel user, CredentialInput input) {
        logger.log(Logger.Level.INFO, String.format("isValid(RealmModel realm=%s, UserModel user=%s, CredentialInput input=%s)", realm, user, input));

        if (user == null) {
            return false;
        }
        if (!supportsCredentialType(input.getType())) return false;

        logger.log(Logger.Level.INFO, String.format("IDMConstraints.IDM_ID: %s", user.getFirstAttribute(IDMConstraints.IDM_ID)));
        return idmClient.isValid(user.getFirstAttribute(IDMConstraints.IDM_ID), input.getChallengeResponse());
    }

    @Override
    public UserModel validate(RealmModel realmModel, UserModel userModel) {
        logger.log(Logger.Level.INFO, String.format("validate(RealmModel realmModel = %s, UserModel userModel = %s)", realmModel, userModel));
        logger.log(Logger.Level.INFO, String.format("ID: %s", userModel.getId()));
        logger.log(Logger.Level.INFO, String.format("UID: %s", userModel.getFirstAttribute(IDMConstraints.IDM_ID)));
        if (userModel.getFirstAttribute(IDMConstraints.IDM_ID) != null) {
            return userModel;
        }
        return null;
    }
}
