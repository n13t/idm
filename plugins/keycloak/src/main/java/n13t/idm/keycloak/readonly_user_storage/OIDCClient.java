package n13t.idm.keycloak.readonly_user_storage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.apis.GoogleApi20;
import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;
import okhttp3.*;
import org.jboss.logging.Logger;
import org.keycloak.component.ComponentValidationException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ExecutionException;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class OIDCClient {
    private static final Logger logger = Logger.getLogger(OIDCClient.class);

    private String tokenType;
    private String accessToken;
    private String refreshToken;
    private Integer expiresIn;
    private Integer refreshExpiresIn;
    private long tokenCreatedAt;
    private final OIDCReadOnlyWellknownConfiguration oidcConfig;
    private final OkHttpClient httpClient;
    private final String clientId;
    private final String clientSecret;

    public OIDCClient(String oidcServerUrl, String clientId, String clientSecret) throws IOException {
        httpClient = new OkHttpClient();
        this.clientId = clientId;
        this.clientSecret = clientSecret;

        Request request = null;

        URL baseUrl = new URL(oidcServerUrl);
        URL url = new URL(baseUrl.getProtocol(), baseUrl.getHost(), baseUrl.getPort(), baseUrl.getPath() + "/.well-known/openid-configuration", null);
        logger.log(Logger.Level.INFO, url);
        request = new Request.Builder()
                .url(url)
                .build();


        Response response = httpClient.newCall(request).execute();

        if (response.code() == 200) {
            String responseBody = response.body().string();
            ObjectMapper objectMapper = new ObjectMapper();
            oidcConfig = objectMapper.readValue(responseBody, OIDCReadOnlyWellknownConfiguration.class);
        } else {
            response.close();
            throw new IOException("Request failed");
        }

    }

    public String getAccessToken() throws IOException {
        long nowInSeconds = System.currentTimeMillis() / 1000;
        if (accessToken == null || nowInSeconds > tokenCreatedAt + expiresIn) {
            RequestBody formBody;
            if (refreshToken == null || nowInSeconds > tokenCreatedAt + refreshExpiresIn) {
                formBody = new FormBody.Builder()
                        .add("grant_type", "client_credentials")
                        .add("client_id", clientId)
                        .add("client_secret", clientSecret)
                        .build();
            } else {
                formBody = new FormBody.Builder()
                        .add("grant_type", "refresh_token")
                        .add("refresh_token", refreshToken)
                        .add("client_id", clientId)
                        .add("client_secret", clientSecret)
                        .build();
            }

            Request request = null;
            request = new Request.Builder()
                    .url(new URL(this.oidcConfig.getTokenEndpoint()))
                    .post(formBody)
//                    .addHeader("Authorization", Base64.getEncoder().withoutPadding().encodeToString(String.format("%s:%s", clientId, clientSecret).getBytes()))
                    .build();

            Response response = httpClient.newCall(request).execute();

            if (response.code() == 200) {
                ObjectMapper objectMapper = new ObjectMapper();
                OIDCTokenResponse tokenResponse = objectMapper.readValue(response.body().string(), OIDCTokenResponse.class);

                this.tokenCreatedAt = System.currentTimeMillis() / 1000;

                this.tokenType = tokenResponse.getTokenType();

                this.accessToken = tokenResponse.getAccessToken();
                this.expiresIn = tokenResponse.getExpiresIn();

                this.refreshToken = tokenResponse.getRefreshToken();
                this.refreshExpiresIn = tokenResponse.getRefreshExpiresIn();

                logger.log(Logger.Level.INFO, accessToken);
            } else {
                response.close();
                throw new IOException("Request failed");
            }
        }

        return String.format("%s %s", tokenType, accessToken);
    }

    public void reset() {
        accessToken = null;
    }


}
