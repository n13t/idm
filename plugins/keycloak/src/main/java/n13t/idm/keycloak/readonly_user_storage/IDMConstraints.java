package n13t.idm.keycloak.readonly_user_storage;

public class IDMConstraints {
    public static final String IDM_PROVIDER = "idm";
    public static final String IDM_SERVER_ADDR = "idmServerAddr";
    public static final String EDIT_MODE = "editMode";
    public static final String IDM_ID = "idmId";
    public static final String IDM_TLS_ENABLED = "tls";
    public static final String OIDC_SERVER_URL = "oidcServerUrl";
    public static final String OIDC_CLIENT_ID = "oidcClientId";
    public static final String OIDC_CLIENT_SECRET = "oidcClientSecret";
}
