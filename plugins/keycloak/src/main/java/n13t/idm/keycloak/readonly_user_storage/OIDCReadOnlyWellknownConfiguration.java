package n13t.idm.keycloak.readonly_user_storage;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OIDCReadOnlyWellknownConfiguration {
    @JsonProperty("token_endpoint")
    private String tokenEndpoint;

    public String getTokenEndpoint() { return tokenEndpoint; }
}
