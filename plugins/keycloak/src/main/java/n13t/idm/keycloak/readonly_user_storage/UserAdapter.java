/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package n13t.idm.keycloak.readonly_user_storage;

import n13t.idm.v0.UserOuterClass.User;
import org.jboss.logging.Logger;
import org.keycloak.common.util.MultivaluedHashMap;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.adapter.AbstractUserAdapterFederatedStorage;

import java.util.Collections;
import java.util.List;
import java.util.Map;


// gRPC Stub


public class UserAdapter extends AbstractUserAdapterFederatedStorage {
    private static final Logger logger = Logger.getLogger(UserAdapter.class);
    protected User user;
    protected String keycloakStorageId;

    public UserAdapter(KeycloakSession session, RealmModel realm, ComponentModel model, User user) {
        super(session, realm, new ComponentModel());
        logger.log(Logger.Level.INFO, "UserAdapter ctor");
        this.user = user;
        keycloakStorageId = StorageId.keycloakId(model, user.getSub());
    }

    @Override
    public String getUsername() {
        logger.log(Logger.Level.INFO, "getUsername()");
        return user.getUsername();
    }

    @Override
    public void setUsername(String username) {}

    @Override
    public void setEmail(String email) {
        logger.log(Logger.Level.INFO, "setEmail()");
//        this.user = User.newBuilder()
//                .setId(this.user.getId())
//                .setUsername(this.user.getUsername())
//                .setEmail(email)
//                .setFirstName(this.user.getFirstName())
//                .setLastName(this.user.getLastName())
//                .setPassword(this.user.getPassword())
//                .build();
    }

    @Override
    public String getEmail() {
        logger.log(Logger.Level.INFO, "getEmail()");
        return user.getEmail();
    }

    @Override
    public String getId() {
        logger.log(Logger.Level.INFO, "getId()");
        return this.keycloakStorageId;
//        return this.user.getSub();
    }

    public String getExternalId() {
        logger.log(Logger.Level.INFO, "getExternalId()");
        return this.user.getSub();
    }

    @Override
    public void setSingleAttribute(String name, String value) {
        logger.log(Logger.Level.INFO, "setSingleAttribute " + name + "=" +value);
        if (name.equals("phone")) {
            User user = User.newBuilder().build();
//            user.getAttributesMap().put(name, value);
        } else {
            super.setSingleAttribute(name, value);
        }
    }

    @Override
    public void removeAttribute(String name) {
        logger.log(Logger.Level.INFO, "removeAttribute " + name);
//        if (name.equals("phone")) {
//            user.setPhone(null);
//        } else {
//            super.removeAttribute(name);
//        }


    }

    @Override
    public void setAttribute(String name, List<String> values) {
        logger.log(Logger.Level.INFO, "setAttribute " + name);
        logger.log(Logger.Level.INFO, values);
//        if (name.equals("phone")) {
//            user.setPhone(values.get(0));
//        } else {
//            super.setAttribute(name, values);
//        }
    }

    @Override
    public String getFirstAttribute(String name) {
        logger.log(Logger.Level.INFO, "getFirstAttribute " + name);
//        if (name.equals("phone")) {
//            return user.getPhone();
//        } else {
//            return super.getFirstAttribute(name);
//        }
        if (name.equals(FIRST_NAME_ATTRIBUTE)) {
            return user.getGivenName();
        } else if (name.equals(LAST_NAME_ATTRIBUTE)) {
            return user.getFamilyName();
        } else if (name.equals(EMAIL_ATTRIBUTE)) {
            return user.getEmail();
        } else if (name.equals(EMAIL_VERIFIED_ATTRIBUTE)) {
            return Boolean.toString(true);
        } else if (name.equals(CREATED_TIMESTAMP_ATTRIBUTE)) {
            return null;
        } else if (name.equals(ENABLED_ATTRIBUTE)) {
            return Boolean.toString(true);
        }

        return super.getFirstAttribute(name);
    }

    @Override
    public Map<String, List<String>> getAttributes() {
        logger.log(Logger.Level.INFO, "getAttributes()");
        Map<String, List<String>> attrs = super.getAttributes();
        MultivaluedHashMap<String, String> all = new MultivaluedHashMap<>();
        all.putAll(attrs);
//        all.add("phone", user.getPhone());
//        return all;
        return all;
    }

    @Override
    public List<String> getAttribute(String name) {
        logger.log(Logger.Level.INFO, String.format("getAttribute(String name = %s)", name));
        if (OIDCProtocolHelper.USERNAME.equals(name)) {
//            return Collections.singletonList(user.getUsername());
            return super.getAttribute(name);
        } else if (OIDCProtocolHelper.EMAIL.equals(name)) {
//            return Collections.singletonList(user.getEmail());
            return super.getAttribute(name);
        } else if (OIDCProtocolHelper.GIVEN_NAME.equals(name)) {
//            return Collections.singletonList(user.getGivenName());
            return super.getAttribute(name);
        } else if (OIDCProtocolHelper.FAMILY_NAME.equals(name)) {
//            return Collections.singletonList(user.getFamilyName());
            return super.getAttribute(name);
        } else if (OIDCProtocolHelper.MIDDLE_NAME.equals(name)) {
            return Collections.singletonList(user.getMiddleName());
        } else if (OIDCProtocolHelper.NICKNAME.equals(name)) {
            return Collections.singletonList(user.getNickname());
        } else if (OIDCProtocolHelper.PROFILE_CLAIM.equals(name)) {
            return Collections.singletonList(user.getProfile());
        } else if (OIDCProtocolHelper.PICTURE.equals(name)) {
            return Collections.singletonList(user.getPicture());
        } else if (OIDCProtocolHelper.WEBSITE.equals(name)) {
            return Collections.singletonList(user.getWebsite());
        } else if (OIDCProtocolHelper.GENDER.equals(name)) {
            return Collections.singletonList(user.getGender());
        } else if (OIDCProtocolHelper.BIRTHDATE.equals(name)) {
            return Collections.singletonList(user.getBirthdate());
        } else if (OIDCProtocolHelper.ZONEINFO.equals(name)) {
            return Collections.singletonList(user.getZoneinfo());
        } else if (OIDCProtocolHelper.UPDATED_AT.equals(name)) {
            return Collections.singletonList(user.getUpdatedAt().toString());
        } else if (OIDCProtocolHelper.LOCALE.equals(name)) {
            return Collections.singletonList(user.getLocale());
        } else if (OIDCProtocolHelper.PHONE_NUMBER.equals(name)) {
            return Collections.singletonList(user.getPhoneNumber());
        } else if (OIDCProtocolHelper.PHONE_NUMBER_VERIFIED.equals(name)) {
            return Collections.singletonList(Boolean.toString(user.getPhoneNumberVerified()));
        } else if (OIDCProtocolHelper.EMAIL_VERIFIED.equals(name)) {
            return Collections.singletonList(Boolean.toString(user.getEmailVerified()));
        } else if (OIDCProtocolHelper.FULL_NAME.equals(name)) {
            return super.getAttribute(name);
        }
        return super.getAttribute(name);
    }
}
