package n13t.idm.keycloak.readonly_user_storage;

import org.keycloak.models.ProtocolMapperModel;

import java.util.Map;

public class OIDCProtocolHelper {
    // User's attributes
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String EMAIL_VERIFIED = "emailVerified";
    public static final String GIVEN_NAME = "giveName";
    public static final String FAMILY_NAME = "familyName";
    public static final String MIDDLE_NAME = "middleName";
    public static final String NICKNAME = "nickname";
    public static final String PROFILE_CLAIM = "profile";
    public static final String PICTURE = "picture";
    public static final String WEBSITE = "website";
    public static final String GENDER = "gender";
    public static final String BIRTHDATE = "birthdate";
    public static final String ZONEINFO = "zoneinfo";
    public static final String UPDATED_AT = "updatedAt";
    public static final String FULL_NAME = "fullName";
    public static final String LOCALE = "locale";
    public static final String ADDRESS = "address";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String PHONE_NUMBER_VERIFIED = "phoneNumberVerified";

//    public static final String REALM_ROLES = "realm roles";
//    public static final String CLIENT_ROLES = "client roles";
//    public static final String AUDIENCE_RESOLVE = "audience resolve";
//    public static final String ALLOWED_WEB_ORIGINS = "allowed web origins";
//    public static final String UPN = "upn";
//    public static final String GROUPS = "groups";
//    public static final String ROLES_SCOPE = "roles";
//    public static final String WEB_ORIGINS_SCOPE = "web-origins";
//    public static final String MICROPROFILE_JWT_SCOPE = "microprofile-jwt";
}
