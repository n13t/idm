/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package n13t.idm._old;

import io.grpc.ManagedChannel;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import n13t.idm.keycloak.readonly_user_storage.IDMConstraints;
import org.jboss.logging.Logger;
import org.keycloak.Config;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.provider.ProviderConfigurationBuilder;
import org.keycloak.storage.UserStorageProviderFactory;

import java.util.List;

/**
 * @author <a href="mailto:bill@burkecentral.com">Bill Burke</a>
 * @version $Revision: 1 $
 */
public class IDMUserStorageProviderFactory implements
        UserStorageProviderFactory<IDMUserStorageProvider>
{
    private static final Logger logger = Logger.getLogger(IDMUserStorageProviderFactory.class);
    public static final String PROVIDER_NAME = "n13t-idm";

    protected static final List<ProviderConfigProperty> configProperties;

    static {
        configProperties = getConfigProps(null);
    }

    private static List<ProviderConfigProperty> getConfigProps(ComponentModel parent) {
        return ProviderConfigurationBuilder
                .create()
                .property().name(IDMConstraints.IDM_ADDR).type(ProviderConfigProperty.STRING_TYPE).add()
                .property().name(IDMConstraints.TLS_ENABLED).type(ProviderConfigProperty.BOOLEAN_TYPE).add()
                .build();
    }

    @Override
    public void init(Config.Scope config) {
        logger.info(config);

    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configProperties;
    }

    @Override
    public IDMUserStorageProvider create(KeycloakSession session, ComponentModel model) {
        try {
//            InitialContext ctx = new InitialContext();
//            VdatUserStorageProvider provider = (VdatUserStorageProvider)ctx.lookup("java:global/vdat-user-storage/" + VdatUserStorageProvider.class.getSimpleName());

//            final String IDENTITY_ENDPOINT = System.getenv("IDENTITY_ENDPOINT");
//            final String IDENTITY_HOST = IDENTITY_ENDPOINT.split(":")[0];
//            final int IDENTITY_PORT = Integer.parseInt(IDENTITY_ENDPOINT.split(":")[1]);
//            System.out.println(IDENTITY_ENDPOINT);
//            System.out.println(IDENTITY_HOST);
//            System.out.println(IDENTITY_PORT);
            ManagedChannel channel;
            String tlsEnabled = System.getenv("IDM_TLS");
            String idmAddr = System.getenv("IDM_ADDR");
            if (tlsEnabled.equals("true")) {
                channel = NettyChannelBuilder.forTarget(idmAddr).useTransportSecurity().build();
            } else {
                channel = NettyChannelBuilder
                        .forTarget(idmAddr)
                        .usePlaintext()
//                        .idleTimeout(10, TimeUnit.SECONDS)
//                        .withOption(ChannelOption.AUTO_CLOSE, true)
//                        .withOption(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10)
//                        .withOption(ChannelOption.SO_KEEPALIVE, false)
                        .build();
            }
            IDMUserStorageProvider provider = new IDMUserStorageProvider(this, session, model, channel);
            return provider;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getId() { return PROVIDER_NAME; }

    @Override
    public String getHelpText() { return "Identity Management Service from <a href=\"https://gitlab.com/n13t\">N13T</a> "; }

    @Override
    public void close() { logger.info("<<<<<< Closing factory"); }

//    @Override
//    public SynchronizationResult sync(KeycloakSessionFactory keycloakSessionFactory, String s, UserStorageProviderModel userStorageProviderModel) {
//        logger.log(Logger.Level.INFO,
//                String.format(
//                        "sync(KeycloakSessionFactory keycloakSessionFactory = %s, String s = %s, UserStorageProviderModel userStorageProviderModel = %s)",
//                        keycloakSessionFactory, s, userStorageProviderModel
//                ));
//        return SynchronizationResult.ignored();
//    }
//
//    @Override
//    public SynchronizationResult syncSince(Date date, KeycloakSessionFactory keycloakSessionFactory, String realm, UserStorageProviderModel userStorageProviderModel) {
//        logger.log(Logger.Level.INFO,
//                String.format("syncSince(Date date = %s, KeycloakSessionFactory keycloakSessionFactory = %s, String realm = %s, UserStorageProviderModel userStorageProviderModel = %s)",
//                        date, keycloakSessionFactory, realm, userStorageProviderModel
//                ));
//        return SynchronizationResult.ignored();

//        SynchronizationResult result = syncImpl(keycloakSessionFactory, realm, userStorageProviderModel);

//        logger.infof("Sync changed users finished: %s", result.getStatus());
//        return result;

//    }

//    protected SynchronizationResult syncImpl(KeycloakSessionFactory sessionFactory, final String realm, final ComponentModel fedModel) {
//
//        final SynchronizationResult syncResult = new SynchronizationResult();
//
//        LDAPConfig ldapConfig = new LDAPConfig(fedModel.getConfig());
//        boolean pagination = ldapConfig.isPagination();
//        if (pagination) {
//            int pageSize = ldapConfig.getBatchSizeForSync();
//
//            boolean nextPage = true;
//            while (nextPage) {
//                userQuery.setLimit(pageSize);
//                final List<LDAPObject> users = userQuery.getResultList();
//                nextPage = userQuery.getPaginationContext().hasNextPage();
//                SynchronizationResult currentPageSync = importIDMUsers(sessionFactory, realmId, fedModel, users);
//                syncResult.add(currentPageSync);
//            }
//        } else {
//            // LDAP pagination not available. Do everything in single transaction
//            final List<LDAPObject> users = userQuery.getResultList();
//            SynchronizationResult currentSync = importIDMUsers(sessionFactory, realmId, fedModel, users);
//            syncResult.add(currentSync);
//        }
//
//        return syncResult;
//    }

//    protected SynchronizationResult importIDMUsers(KeycloakSessionFactory sessionFactory, final String realmId, final ComponentModel fedModel, List<UserOuterClass.User> idmUsers) {
//        final SynchronizationResult syncResult = new SynchronizationResult();
//
//        class BooleanHolder {
//            private boolean value = true;
//        }
//        final BooleanHolder exists = new BooleanHolder();
//
//        for (final UserOuterClass.User idmUser : idmUsers) {
//
//            KeycloakSession session = sessionFactory.create();
//
//            IDMUserStorageProvider idmFedProvider = (IDMUserStorageProvider)session.getProvider(UserStorageProvider.class, fedModel);
//            RealmModel currentRealm = session.realms().getRealm(realmId);
//            session.getContext().setRealm(currentRealm);
//
//            exists.value = true;
////                        LDAPUtils.checkUuid(ldapUser, ldapFedProvider.getLdapIdentityStore().getConfig());
//            UserModel currentUser = session.userLocalStorage().getUserByUsername(idmUser.getUsername(), currentRealm);
//
//            if (currentUser == null) {
//
//                // Add new user to Keycloak
//                exists.value = false;
//                idmFedProvider.importUserFromIDM(session, currentRealm, idmUser);
//                syncResult.increaseAdded();
//
//            } else {
//                if ((fedModel.getId().equals(currentUser.getFederationLink())) && (idmUser.getSub().equals(currentUser.getFirstAttribute(IDMConstraints.IDM_ID)))) {
//                    // Update keycloak user
////                                List<ComponentModel> federationMappers = currentRealm.getComponents(fedModel.getId(), LDAPStorageMapper.class.getName());
////                                List<ComponentModel> sortedMappers = ldapFedProvider.getMapperManager().sortMappersDesc(federationMappers);
////                                for (ComponentModel mapperModel : sortedMappers) {
////                                    LDAPStorageMapper ldapMapper = ldapFedProvider.getMapperManager().getMapper(mapperModel);
////                                    ldapMapper.onImportUserFromLDAP(ldapUser, currentUser, currentRealm, false);
////                                }
//                    idmFedProvider.onImportUserFromIDM(idmUser, currentUser, currentRealm, false);
//                    UserCache userCache = session.userCache();
//                    if (userCache != null) {
//                        userCache.evict(currentRealm, currentUser);
//                    }
//                    logger.debugf("Updated user from LDAP: %s", currentUser.getUsername());
//                    syncResult.increaseUpdated();
//                } else {
//                    logger.warnf("User '%s' is not updated during sync as he already exists in Keycloak database but is not linked to federation provider '%s'", idmUser.getUsername(), fedModel.getName());
//                    syncResult.increaseFailed();
//                }
//            }
//
//        }
//
//        return syncResult;
//    }


}
