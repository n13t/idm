/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package n13t.idm._old;

import io.grpc.ManagedChannel;
import n13t.idm.keycloak.readonly_user_storage.IDMClient;
import n13t.idm.keycloak.readonly_user_storage.UserAdapter;
import n13t.idm.v0.UserOuterClass.User;
import org.jboss.logging.Logger;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.credential.CredentialModel;
import org.keycloak.models.*;
import org.keycloak.models.cache.CachedUserModel;
import org.keycloak.models.utils.UserModelDelegate;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.UserStorageProviderModel;
import org.keycloak.storage.user.ImportedUserValidation;
import org.keycloak.storage.user.UserLookupProvider;

import java.util.HashMap;
import java.util.Map;

import static n13t.idm.keycloak.readonly_user_storage.OIDCProtocolHelper.IDM_USER_ID;

//import javax.ejb.Local;
//import javax.ejb.Remove;
//import javax.ejb.Stateful;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.TypedQuery;
/* gRPC Stub */


/**
 * @author <a href="mailto:bill@burkecentral.com">Bill Burke</a>
 * @version $Revision: 1 $
 */
public class IDMUserStorageProvider implements
        UserStorageProvider,
        UserLookupProvider,
        CredentialInputValidator,
        ImportedUserValidation
{
    private static final Logger logger = Logger.getLogger(IDMUserStorageProvider.class.getName());
    public static final String PASSWORD_CACHE_KEY = UserAdapter.class.getName() + ".password";
    protected Map<String, UserModel> loadedUsers = new HashMap<>();

//    @PersistenceContext
//    protected EntityManager em;

    protected IDMUserStorageProviderFactory factory;
    protected UserStorageProviderModel model;
    protected KeycloakSession session;

    protected IDMClient idmClient;

    public IDMUserStorageProvider(IDMUserStorageProviderFactory factory, KeycloakSession session, ComponentModel model, ManagedChannel idmGRPCChannel) {
        this.factory = factory;
        this.idmClient = new IDMClient(idmGRPCChannel);
        this.session = session;
        this.model = new UserStorageProviderModel(model);
    }

    protected void finalize( ) throws Throwable {
        this.idmClient.shutdown();
    }

    @Override
    public void preRemove(RealmModel realm) {

    }

    @Override
    public void preRemove(RealmModel realm, GroupModel group) {

    }

    @Override
    public void preRemove(RealmModel realm, RoleModel role) {

    }

    @Override
    public void close() {
    }

    /**
     * Required to support basic login
     */
    @Override
    public UserModel getUserById(String id, RealmModel realm) {
        logger.log(Logger.Level.INFO, String.format("getUserById(String id = %s, RealmModel realm = %s)", id, realm));

        StorageId storageId = new StorageId(id);
        String username = storageId.getExternalId();
        return getUserByUsername(username, realm);

////        StorageId storageId = new StorageId(id);
////        String username = storageId.getExternalId();
//        String persistenceId = StorageId.externalId(id);
////        User entity = em.find(UserEntity.class, persistenceId);
//        UserModel adapter = loadedUsers.get(persistenceId);
////         String[] s = id.split(":");
//
//        try {
////             User user = idmClient.getUserById(s.length == 3? s[2] : id);
//            User user = idmClient.getUserById(persistenceId);
//            if (user == null) {
//                logger.log(Logger.Level.INFO, "could not find user by id: " + id);
//                return null;
//            }
//            return new UserAdapter(session, realm, model, user);
//        } catch(StatusRuntimeException error) {
//            logger.log(Logger.Level.INFO, error.toString());
//        }
//
//        return null;
    }

    @Override
    public UserModel getUserByUsername(String username, RealmModel realm)  {
        logger.log(Logger.Level.INFO, String.format("getUserByUsername(String username = %s, RealmModel realm = %s)", username, realm));
        UserModel adapter = loadedUsers.get(username);
        if (adapter == null) {
            adapter = createAdapter(realm, username);
            loadedUsers.put(username, adapter);
        }
        return adapter;

//        User user = null;
//        try{
//            user = idmClient.getUserByUsername(username);
//        } finally {
////            try {
////                idmClient.shutdown();
////            } catch (InterruptedException e) {
////                e.printStackTrace();
////            }
//        }
//
//        if (user == null) {
//            logger.log(Logger.Level.INFO, "could not find username: " + username);
//            return null;
//        }
//
//        return new UserAdapter(session, realm, model, user);



//        TypedQuery<User> query = em.createNamedQuery("getUserByUsername", User.class);
//        query.setParameter("username", username);
//        List<User> result = query.getResultList();
//        if (result.isEmpty()) {
//            logger.log(Logger.Level.INFO, "could not find username: " + username);
//            return null;
//        }

//        return new UserAdapter(session, realm, model, result.get(0));
//        return null;
    }

    protected UserModel createAdapter(RealmModel realm, String username) {
        UserModel local = session.userLocalStorage().getUserByUsername(username, realm);
        if (local == null) {
            local = session.userLocalStorage().addUser(realm, username);
            local.setFederationLink(model.getId());
            User idmUser = idmClient.getUserByUsername(username);
            local.setUsername(idmUser.getUsername());
            local.setSingleAttribute(IDM_USER_ID, idmUser.getSub());
            local.setEnabled(true);
        }

        return new UserModelDelegate(local) {
            @Override
            public String getId() {
                String id = super.getId();
//                String id = super.getAttribute("uuid").get(0));
                logger.log(Logger.Level.INFO, String.format("UserModelDelegate::getId return %s", id));
                return id;
//                return super.getId();
            }

            @Override
            public void setUsername(String username) {
                logger.log(Logger.Level.INFO, String.format("UserModelDelegate::setUsername(String username = %s)", username));
                super.setUsername(username);
            }
        };
//        return new AbstractUserAdapter(session, realm, model) {
//            @Override
//            public String getUsername() {
//                return username;
//            }
//        };
    }

    @Override
    public UserModel getUserByEmail(String email, RealmModel realm) {
        logger.log(Logger.Level.INFO, String.format("getUserByEmail(String email = %s, RealmModel realm = %s)", email, realm));
//        TypedQuery<User> query = em.createNamedQuery("getUserByEmail", User.class);
//        query.setParameter("email", email);
//        List<User> result = query.getResultList();
//        if (result.isEmpty()) return null;
//        return new UserAdapter(session, realm, model, result.get(0));
        User user = idmClient.getUserByEmail(email);
        return new UserAdapter(session, realm, model, user);
    }

    public UserAdapter getUserAdapter(UserModel user) {
        logger.log(Logger.Level.INFO, String.format("UserAdapter getUserAdapter(UserModel user)"));
        UserAdapter adapter = null;
        if (user instanceof CachedUserModel) {
            adapter = (UserAdapter)((CachedUserModel)user).getDelegateForUpdate();
        } else {
            adapter = (UserAdapter)user;
        }
        return adapter;
    }

    @Override
    public boolean isConfiguredFor(RealmModel realm, UserModel user, String credentialType) {
        logger.log(Logger.Level.INFO, String.format("isConfiguredFor(RealmModel realm, UserModel user, String credentialType)"));
        return credentialType.equals(CredentialModel.PASSWORD);
    }

    @Override
    public boolean supportsCredentialType(String credentialType) {
        logger.log(Logger.Level.INFO, String.format("supportsCredentialType(String credentialType = %s)", credentialType));
        return credentialType.equals(CredentialModel.PASSWORD);
    }

    /**
     * Call IsValid rpc to check password
     */
    @Override
    public boolean isValid(RealmModel realm, UserModel user, CredentialInput input) {
        logger.log(Logger.Level.INFO, String.format("isValid(RealmModel realm=%s, UserModel user=%s, CredentialInput input=%s)", realm, user, input));

        if (user == null) {
            return false;
        }
        if (!supportsCredentialType(input.getType())) return false;

        logger.log(Logger.Level.INFO, String.format("IDM_USER_ID: %s", user.getFirstAttribute(IDM_USER_ID)));
//        String[] s = user.getId().split(":");
//        if (s.length == 3) {
//            logger.log(Logger.Level.INFO, String.format("isValid(RealmModel realm, UserModel user, CredentialInput input)\nUnexpected user id\n\n%s", user.getId()));
//            return false;
//        }

//        if (!supportsCredentialType(input.getType()) || !(input instanceof UserCredentialModel)) return false;
//        UserCredentialModel cred = (UserCredentialModel)input;
//        return idmClient.isValid(s[2], input.getChallengeResponse());
        return idmClient.isValid(user.getFirstAttribute(IDM_USER_ID), input.getChallengeResponse());
    }

    @Override
    public UserModel validate(RealmModel realmModel, UserModel userModel) {
        logger.log(Logger.Level.INFO, String.format("validate(RealmModel realmModel = %s, UserModel userModel = %s)", realmModel, userModel));
        logger.log(Logger.Level.INFO, String.format("ID: %s", userModel.getId()));
        logger.log(Logger.Level.INFO, String.format("UID: %s", userModel.getFirstAttribute(IDM_USER_ID)));
        if (userModel.getFirstAttribute(IDM_USER_ID) != null) {
            return userModel;
        }
        return null;
    }

//    protected UserModel importUserFromIDM(KeycloakSession session, RealmModel realm, User idmUser) {
//        UserModel imported = null;
//        if (model.isImportEnabled()) {
//            imported = session.userLocalStorage().addUser(realm, idmUser.getUsername());
//        } else {
//            InMemoryUserAdapter adapter = new InMemoryUserAdapter(session, realm, new StorageId(model.getId(), idmUser.getUsername()).getId());
//            adapter.addDefaults();
//            imported = adapter;
//        }
//        imported.setEnabled(true);
//
//        onImportUserFromIDM(idmUser, imported, realm, true);
//
//        if (model.isImportEnabled()) imported.setFederationLink(model.getId());
//        UserModel proxy = proxy(realm, imported, idmUser);
//        return proxy;
//    }

//    protected UserModel proxy(RealmModel realm, UserModel local, User idmUser) {
//        UserModel existing = loadedUsers.get(local.getUsername());
//        if (existing != null) {
//            return existing;
//        }
//
//        // We need to avoid having CachedUserModel as cache is upper-layer then LDAP. Hence having CachedUserModel here may cause StackOverflowError
//        if (local instanceof CachedUserModel) {
//            local = session.userStorageManager().getUserById(local.getId(), realm);
//
//            existing = loadedUsers.get(local.getUsername());
//            if (existing != null) {
//                return existing;
//            }
//        }
//
//        UserModel proxied = local;
//        if (model.isImportEnabled()) {
//            proxied = new ReadonlyIDMUserModelDelegate(local, this);
//        } else {
//            proxied = new ReadOnlyUserModelDelegate(local);
//        }
//
//        local = session.userLocalStorage().addUser(realm, local.getUsername());
//        local.setFederationLink(model.getId());
//
//        return proxied;
//    }

//    public void onImportUserFromIDM(User idmUser, UserModel currentUser, RealmModel currentRealm, boolean isCreate) {
//        currentUser.setFirstName(idmUser.getGivenName());
//        currentUser.setLastName(idmUser.getFamilyName());
//    }

//    public String getPassword(UserModel user) {
//        String password = null;
//        if (user instanceof CachedUserModel) {
//            password = (String)((CachedUserModel)user).getCachedWith().get(PASSWORD_CACHE_KEY);
//        } else if (user instanceof UserAdapter) {
//            password = ((UserAdapter)user).getPassword();
//        }
//        return password;
//    }

//    @Override
//    public int getUsersCount(RealmModel realm) {
//        logger.log(Logger.Level.INFO, String.format("getUsersCount(RealmModel realm)"));
////        Object count = em.createNamedQuery("getUserCount")
////                .getSingleResult();
////        return ((Number)count).intValue();
//        return 0;
//    }
//
//    @Override
//    public List<UserModel> getUsers(RealmModel realm) {
//        logger.log(Logger.Level.INFO, "getUsers");
//
//        return getUsers(realm, -1, -1);
//    }
//
//    @Override
//    public List<UserModel> getUsers(RealmModel realm, int firstResult, int maxResults) {
//
//        logger.log(Logger.Level.INFO, "getUsers with limit");
//
////        TypedQuery<User> query = em.createNamedQuery("getAllUsers", User.class);
////        if (firstResult != -1) {
////            query.setFirstResult(firstResult);
////        }
////        if (maxResults != -1) {
////            query.setMaxResults(maxResults);
////        }
////        List<User> results = query.getResultList();
////        List<UserModel> users = new LinkedList<>();
////        for (User entity : results) users.add(new UserAdapter(session, realm, model, entity));
////        return users;
//        return new LinkedList<>();
//    }
//
//    @Override
//    public List<UserModel> searchForUser(String search, RealmModel realm) {
//        logger.log(Logger.Level.INFO, String.format("searchForUser(String search = %s, RealmModel realm = %s)", search, realm));
//
//        return searchForUser(search, realm, -1, -1);
//    }
//
//    @Override
//    public List<UserModel> searchForUser(String search, RealmModel realm, int firstResult, int maxResults) {
//        logger.log(Logger.Level.INFO, String.format("searchForUser(String search = %s, RealmModel realm = %s, int firstResult = %d, int maxResults = %d)", search, realm, firstResult, maxResults));
//
//        List<User> results = idmClient.searchForUser(search, realm, firstResult, maxResults);
//        List<UserModel> users = new LinkedList<>();
//        for (User entity : results) users.add(new UserAdapter(session, realm, model, entity));
//
//        return users;
//    }
//
//    @Override
//    public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm) {
//        logger.log(Logger.Level.INFO, String.format("searchForUser(Map<String, String> params = %s, RealmModel realm = %s))", params, realm));
//
//        List<User> results = idmClient.searchForUser(params.toString(), realm, 0, Integer.MAX_VALUE - 1);
//        List<UserModel> users = new LinkedList<>();
//        for (User entity : results) users.add(new UserAdapter(session, realm, model, entity));
//
//        return users;
//    }
//
//    @Override
//    public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm, int firstResult, int maxResults) {
//        logger.log(Logger.Level.INFO, String.format("searchForUser(Map<String, String> params = %s, RealmModel realm = %s, int firstResult = %d, int maxResults = %d)", params, realm, firstResult, maxResults));
//
//        List<User> results = idmClient.searchForUser(params.toString(), realm, firstResult, maxResults);
//        List<UserModel> users = new LinkedList<>();
//        for (User entity : results) users.add(new UserAdapter(session, realm, model, entity));
//
//        return users;
//    }
//
//    @Override
//    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group, int firstResult, int maxResults) {
//        return Collections.EMPTY_LIST;
//    }
//
//    @Override
//    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group) {
//        return Collections.EMPTY_LIST;
//    }
//
//    @Override
//    public List<UserModel> searchForUserByUserAttribute(String attrName, String attrValue, RealmModel realm) {
//        logger.log(Logger.Level.INFO, String.format("searchForUserByUserAttribute(String attrName = %s, String attrValue = %s, RealmModel realm = %s)", attrName, attrValue, realm));
//
//        List<User> results = idmClient.searchForUser(String.format("%s=%s", attrName, attrValue), realm, 0, Integer.MAX_VALUE - 1);
//        List<UserModel> users = new LinkedList<>();
//        for (User entity : results) users.add(new UserAdapter(session, realm, model, entity));
//
//        return users;
//    }


//    @Override
//    public UserModel validate(RealmModel realmModel, UserModel userModel) {
//        logger.log(Logger.Level.INFO, String.format("validate(RealmModel realmModel, UserModel userModel)"));
//        return null;
//    }
}
