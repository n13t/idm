package idmcli

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"gitlab.com/n13t/idm/pkg/idmendpoint"
	"gitlab.com/n13t/idm/pkg/idmservice"
	"gitlab.com/n13t/idm/pkg/uma2"
	"golang.org/x/oauth2"
)

func RetryService(svc idmservice.Service, uma2Client *uma2.Config, token *oauth2.Token) idmservice.Service {
	return idmendpoint.Set{
		SearchEndpoint: RetryTicket(svc.(idmendpoint.Set).SearchEndpoint, uma2Client, token,false),
		ListUsersEndpoint: RetryTicket(svc.(idmendpoint.Set).ListUsersEndpoint, uma2Client, token,false),
		CreateUserEndpoint: RetryTicket(svc.(idmendpoint.Set).CreateUserEndpoint, uma2Client, token,false),
		UpdateUserEndpoint: RetryTicket(svc.(idmendpoint.Set).UpdateUserEndpoint, uma2Client, token,false),
	}
}

func RetryTicket(endpoint endpoint.Endpoint, cfg *uma2.Config, token *oauth2.Token, responseTicketOnFailedRetry bool) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		resp, _ := endpoint(ctx, request)

		switch resp.(type) {
		//case idmendpoint.SearchResponse:
		//	businessError := resp.(idmendpoint.SearchResponse).Err
		//
		//	var uma2Error uma2.UMA2TicketError
		//	err := json.Unmarshal([]byte(businessError.Error()), &uma2Error)
		//	if err != nil {
		//		return resp, nil
		//	}
		//
		//	fmt.Println("Upgrade with ticket")
		//	ticket := uma2Error.Ticket
		//	rpt, err := cfg.RequestRPT(&uma2.RPTRequest{
		//		GrantType: uma2.UMA2TicketGrantType,
		//		Ticket:    ticket.Ticket,
		//	}, token)
		//	if err != nil {
		//		panic(err)
		//	}
		//
		//	fmt.Println(rpt)
		//
		//	// Replace AT by RPT, then retry
		//	return endpoint(context.WithValue(ctx, jwt.JWTTokenContextKey, rpt.AccessToken), request)
		//case idmendpoint.ListUsersResponse:
		//	businessError := resp.(idmendpoint.ListUsersResponse).Err
		//
		//	var uma2Error uma2.UMA2TicketError
		//	err := json.Unmarshal([]byte(businessError.Error()), &uma2Error)
		//	if err != nil {
		//		return resp, nil
		//	}
		//
		//	fmt.Println("Upgrade with ticket")
		//	ticket := uma2Error.Ticket
		//	rpt, err := cfg.RequestRPT(&uma2.RPTRequest{
		//		GrantType: uma2.UMA2TicketGrantType,
		//		Ticket:    ticket.Ticket,
		//	}, token)
		//	if err != nil {
		//		panic(err)
		//	}
		//
		//	fmt.Println(rpt)
		//
		//	// Replace AT by RPT, then retry
		//	return endpoint(context.WithValue(ctx, jwt.JWTTokenContextKey, rpt.AccessToken), request)
		default:
			return resp, nil
		}

	}
}
