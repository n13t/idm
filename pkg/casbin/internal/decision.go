package internal


/**
 * Decision Strategy functions
 */


func Unanimous(args ...interface{}) (interface{}, error) {
	for _, e := range args {
		if e.(bool) == false {
			return false, nil
		}
	}

	return true, nil
}

func Affirmative(args ...interface{}) (interface{}, error) {
	for _, e := range args {
		if e.(bool) == true {
			return true, nil
		}
	}

	return false, nil
}

func Consensus(args ...interface{}) (interface{}, error) {
	var positive, negative int
	for _, e := range args {
		if e.(bool) == true {
			positive++
		} else {
			negative++
		}
	}

	return positive>negative, nil
}
