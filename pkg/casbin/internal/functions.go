package internal

import "strings"

func HasPrefix(args ...interface{}) (interface{}, error) {
	return strings.HasPrefix(args[0].(string), args[1].(string)), nil
}

func HasSuffix(args ...interface{}) (interface{}, error) {
	return strings.HasSuffix(args[0].(string), args[1].(string)), nil
}
