package internal

import "testing"

func TestUnanimous(t *testing.T) {
	for _, test := range []struct{
		inp []interface{}
		out bool
	} {
		{ []interface{}{true, true, true}, true},
		{ []interface{}{true, false, true}, false},
		{ []interface{}{false, false, false}, false},
		{ []interface{}{true, false, false}, false},
	} {
		v, err := Unanimous(test.inp...)
		if err != nil {
			panic(err)
		}

		if v != test.out {
			t.Errorf("want %v have %v", test.out, v)
		}
	}
}

func TestAffirmative(t *testing.T) {
	for _, test := range []struct{
		inp []interface{}
		out bool
	} {
		{ []interface{}{true, true, true}, true},
		{ []interface{}{true, false, true}, true},
		{ []interface{}{false, false, true}, true},
		{ []interface{}{false, false, false}, false},
	} {
		v, _ := Affirmative(test.inp...)

		if v != test.out {
			t.Errorf("want %v have %v", test.out, v)
		}
	}
}

func TestConsensus(t *testing.T) {
	for _, test := range []struct{
		inp []interface{}
		out bool
	} {
		{ []interface{}{true, true, false, true}, true},
		{ []interface{}{true, true, false, false}, false},
		{ []interface{}{true, false, false, false}, false},
	} {
		v, _ := Consensus(test.inp...)

		if v != test.out {
			t.Errorf("want %v have %v", test.out, v)
		}
	}
}
