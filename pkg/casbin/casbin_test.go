package casbin

import (
	"bytes"
	"flag"
	"fmt"
	"github.com/casbin/casbin/v2"
	"gitlab.com/n13t/idm/pkg/casbin/internal"
	"gitlab.com/n13t/idm/pkg/uma2"
	"io/ioutil"
	"path/filepath"
	"testing"
	"time"
)

type Context struct {
	Timestamp time.Time
	Rpc string
}

var update = flag.Bool("update", false, "update .golden files")

func TestCasbin(t *testing.T) {
	for _, m := range []string{
		"model1",
		"model2",
	} {
		t.Run(m, func(t *testing.T) {
			enforcer, err := casbin.NewEnforcer(filepath.Join("testdata/", m + ".conf"))
			if err != nil {
				t.Error(err)
			}

			// Custom functions
			enforcer.AddFunction("U", internal.Unanimous)
			enforcer.AddFunction("A", internal.Affirmative)
			enforcer.AddFunction("C", internal.Consensus)
			enforcer.AddFunction("HasPrefix", internal.HasPrefix)
			enforcer.AddFunction("HasSuffix", internal.HasSuffix)
			//enforcer.AddFunction("Subset", strings.HasPrefix)

			for _, test := range []struct{
				ctx Context
				sub uma2.IntrospectionObject
				obj uma2.Resource
				allow bool
			} {
				{
					ctx: Context{Rpc: "ListUsers", Timestamp: time.Now()},
					sub: uma2.IntrospectionObject{
						ClientId: "urn:org:type:confidence:name:my_client",
						Permissions: []struct {
							ResourceId     string    `json:"resource_id"`
							ResourceScopes []string  `json:"resource_scopes"`
							Exp            time.Time `json:"exp"`
							Iat            time.Time `json:"iat"`
							Nbf            time.Time `json:"nbf"`
						}{
							{ResourceId: "123", ResourceScopes: []string{"manage"}},
						},
					},
					obj: uma2.Resource{
						Id: "123",
						ResourceScopes: []string{"read", "manage"},
					},
					allow: true,
				},
			} {
				actual, _ := enforcer.Enforce(test.ctx, test.sub, test.obj)
				golden := filepath.Join("testdata/", m+".golden")
				if *update {
					ioutil.WriteFile(golden, []byte(fmt.Sprintf("%v", actual)), 0644)
				}
				expected, _ := ioutil.ReadFile(golden)


				if !bytes.Equal(expected, []byte(fmt.Sprintf("%v", actual))) {
					t.Error("FAILED")
				} else {
					t.Logf("OK")
				}

			}
		})
	}
}

