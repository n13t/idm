package idmtransport

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/ratelimit"
	"github.com/golang/protobuf/ptypes"
	"github.com/sony/gobreaker"
	idmService "gitlab.com/n13t/idm/pkg/idmservice"
	"gitlab.com/n13t/idm/pkg/uma2"
	"golang.org/x/time/rate"
	"google.golang.org/grpc"
	"time"

	stdopentracing "github.com/opentracing/opentracing-go"
	stdzipkin "github.com/openzipkin/zipkin-go"

	"github.com/go-kit/kit/auth/jwt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/tracing/opentracing"
	"github.com/go-kit/kit/tracing/zipkin"
	"github.com/go-kit/kit/transport"
	grpcTransport "github.com/go-kit/kit/transport/grpc"

	//"github.com/golang/protobuf/ptypes/empty"
	idmEndpoint "gitlab.com/n13t/idm/pkg/idmendpoint"
	idmPb "gitlab.com/n13t/protos/pkg/n13t/idm/v0"
	//grpcCodes "google.golang.org/grpc/codes"
	//grpcStatus "google.golang.org/grpc/status"
)

type grpcServer struct {
	sum        	grpcTransport.Handler
	concat     	grpcTransport.Handler
	search		grpcTransport.Handler
	listUsers	grpcTransport.Handler
	createUser 	grpcTransport.Handler
	updateUser 	grpcTransport.Handler
	deleteUser 	grpcTransport.Handler
	check      	grpcTransport.Handler
	isValid		grpcTransport.Handler
	encrypt		grpcTransport.Handler
	decrypt		grpcTransport.Handler
}



// NewGRPCServer makes a set of endpoints available as a gRPC AddServer.
func NewGRPCServer(endpoints idmEndpoint.Set, otTracer stdopentracing.Tracer, zipkinTracer *stdzipkin.Tracer, logger log.Logger) idmPb.IdentityManagementServer {
	options := []grpcTransport.ServerOption{
		grpcTransport.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
	}

	if zipkinTracer != nil {
		// Zipkin GRPC Server Trace can either be instantiated per gRPC method with a
		// provided operation name or a global tracing service can be instantiated
		// without an operation name and fed to each Go kit gRPC server as a
		// ServerOption.
		// In the latter case, the operation name will be the endpoint's grpc method
		// path if used in combination with the Go kit gRPC Interceptor.
		//
		// In this example, we demonstrate a global Zipkin tracing service with
		// Go kit gRPC Interceptor.
		options = append(options, zipkin.GRPCServerTrace(zipkinTracer))
	}

	return &grpcServer{
		search: grpcTransport.NewServer(
			endpoints.SearchEndpoint,
			decodeGRPCSearchRequest,
			encodeGRPCSearchResponse,
			append(
				options,
				grpcTransport.ServerBefore(
					opentracing.GRPCToContext(otTracer, "Search", logger),
					jwt.GRPCToContext(),uma2.ContextInitialize(),
					),
				grpcTransport.ServerAfter(uma2.ContextToGRPC()),
			)...,
		),
		listUsers: grpcTransport.NewServer(
			endpoints.ListUsersEndpoint,
			decodeGRPCListUsersRequest,
			encodeGRPCListUsersResponse,
			append(
				options,
				grpcTransport.ServerBefore(
					opentracing.GRPCToContext(otTracer, "ListUsers", logger),
					jwt.GRPCToContext(),uma2.ContextInitialize(),
					),
				grpcTransport.ServerAfter(uma2.ContextToGRPC()),
			)...,
		),
		createUser: grpcTransport.NewServer(
			endpoints.CreateUserEndpoint,
			decodeGRPCCreateUserRequest,
			encodeGRPCCreateUserResponse,
			append(
				options,
				grpcTransport.ServerBefore(opentracing.GRPCToContext(otTracer, "CreateUser", logger),jwt.GRPCToContext(),uma2.ContextInitialize()),
				grpcTransport.ServerAfter(uma2.ContextToGRPC()),
			)...,
		),
		updateUser: grpcTransport.NewServer(
			endpoints.UpdateUserEndpoint,
			decodeGRPCUpdateUserRequest,
			encodeGRPCUpdateUserResponse,
			append(
				options,
				grpcTransport.ServerBefore(opentracing.GRPCToContext(otTracer, "UpdateUser", logger), jwt.GRPCToContext(),uma2.ContextInitialize()),
				grpcTransport.ServerAfter(uma2.ContextToGRPC()),
			)...,
		),
		deleteUser: grpcTransport.NewServer(
			endpoints.DeleteUserEndpoint,
			decodeGRPCDeleteUserRequest,
			encodeGRPCDeleteUserResponse,
			append(
				options,
				grpcTransport.ServerBefore(opentracing.GRPCToContext(otTracer, "DeleteUser", logger), jwt.GRPCToContext(),uma2.ContextInitialize()),
				grpcTransport.ServerAfter(uma2.ContextToGRPC()),
			)...,
		),
		isValid: grpcTransport.NewServer(
			endpoints.IsValidEndpoint,
			decodeGRPCIsValidRequest,
			encodeGRPCIsValidResponse,
			append(options, grpcTransport.ServerBefore(opentracing.GRPCToContext(otTracer, "IsValid", logger)))...,
		),
		encrypt: grpcTransport.NewServer(
			endpoints.EncryptEndpoint,
			decodeGRPCEncryptRequest,
			encodeGRPCEncryptResponse,
			append(options, grpcTransport.ServerBefore(opentracing.GRPCToContext(otTracer, "Encrypt", logger)))...,
		),
		decrypt: grpcTransport.NewServer(
			endpoints.DecryptEndpoint,
			decodeGRPCDecryptRequest,
			encodeGRPCDecryptResponse,
			append(options, grpcTransport.ServerBefore(opentracing.GRPCToContext(otTracer, "Decrypt", logger)))...,
		),
	}
}

//func (s *grpcServer) Sum(ctx context.Context, req *idmPb.SumRequest) (*idmPb.SumReply, error) {
//	_, rep, err := s.sum.ServeGRPC(ctx, req)
//	if err != nil {
//		return nil, err
//	}
//	return rep.(*idmPb.SumReply), nil
//}
//
//func (s *grpcServer) Concat(ctx context.Context, req *idmPb.ConcatRequest) (*idmPb.ConcatReply, error) {
//	_, rep, err := s.concat.ServeGRPC(ctx, req)
//	if err != nil {
//		return nil, err
//	}
//	return rep.(*idmPb.ConcatReply), nil
//}

func (s *grpcServer) Search(ctx context.Context, req *idmPb.SearchRequest) (*idmPb.SearchResponse, error) {
	_, rep, err := s.search.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*idmPb.SearchResponse), nil
}

func (s *grpcServer) ListUsers(ctx context.Context, req *idmPb.ListUsersRequest) (*idmPb.ListUsersResponse, error) {
	_, rep, err := s.listUsers.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*idmPb.ListUsersResponse), nil
}

func (s *grpcServer) CreateUser(ctx context.Context, req *idmPb.CreateUserRequest) (*idmPb.CreateUserResponse, error) {
	_, rep, err := s.createUser.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*idmPb.CreateUserResponse), nil
}

func (s *grpcServer) UpdateUser(ctx context.Context, req *idmPb.UpdateUserRequest) (*idmPb.UpdateUserResponse, error) {
	_, rep, err := s.updateUser.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*idmPb.UpdateUserResponse), nil
}

func (s *grpcServer) DeleteUser(ctx context.Context, req *idmPb.DeleteUserRequest) (*idmPb.DeleteUserResponse, error) {
	_, rep, err := s.deleteUser.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*idmPb.DeleteUserResponse), nil
}

func (s *grpcServer) IsValid(ctx context.Context, req *idmPb.IsValidRequest) (*idmPb.IsValidResponse, error) {
	_, rep, err := s.isValid.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*idmPb.IsValidResponse), nil
}

func (s *grpcServer) Encrypt(ctx context.Context, req *idmPb.EncryptRequest) (*idmPb.EncryptResponse, error) {
	_, rep, err := s.encrypt.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*idmPb.EncryptResponse), nil
}

func (s *grpcServer) Decrypt(ctx context.Context, req *idmPb.DecryptRequest) (*idmPb.DecryptResponse, error) {
	_, rep, err := s.decrypt.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*idmPb.DecryptResponse), nil
}

// NewGRPCClient returns an AddService backed by a gRPC server at the other end
// of the conn. The caller is responsible for constructing the conn, and
// eventually closing the underlying transport. We bake-in certain middlewares,
// implementing the client library pattern.
func NewGRPCClient(conn *grpc.ClientConn, otTracer stdopentracing.Tracer, zipkinTracer *stdzipkin.Tracer, logger log.Logger, ) idmService.Service {
	// We construct a single ratelimiter middleware, to limit the total outgoing
	// QPS from this client to all methods on the remote instance. We also
	// construct per-endpoint circuitbreaker middlewares to demonstrate how
	// that's done, although they could easily be combined into a single breaker
	// for the entire remote instance, too.
	limiter := ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Every(time.Second), 1e4))

	// global client middlewares
	var options []grpcTransport.ClientOption

	if zipkinTracer != nil {
		// Zipkin GRPC Client Trace can either be instantiated per gRPC method with a
		// provided operation name or a global tracing client can be instantiated
		// without an operation name and fed to each Go kit client as ClientOption.
		// In the latter case, the operation name will be the endpoint's grpc method
		// path.
		//
		// In this example, we demonstrace a global tracing client.
		options = append(options, zipkin.GRPCClientTrace(zipkinTracer))

	}
	//// Each individual endpoint is an grpc/transport.Client (which implements
	//// endpoint.Endpoint) that gets wrapped with various middlewares. If you
	//// made your own client library, you'd do this work there, so your server
	//// could rely on a consistent set of client behavior.
	//var sumEndpoint endpoint.Endpoint
	//{
	//	sumEndpoint = grpcTransport.NewClient(
	//		conn,
	//		"n13t.idm.v0.IdentityManagement.Add",
	//		"Sum",
	//		encodeGRPCSumRequest,
	//		decodeGRPCSumResponse,
	//		idmPb.SumReply{},
	//		append(options, grpcTransport.ClientBefore(opentracing.ContextToGRPC(otTracer, logger)))...,
	//	).Endpoint()
	//	sumEndpoint = opentracing.TraceClient(otTracer, "Sum")(sumEndpoint)
	//	sumEndpoint = limiter(sumEndpoint)
	//	sumEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
	//		Name:    "Sum",
	//		Timeout: 30 * time.Second,
	//	}))(sumEndpoint)
	//}
	//
	//// The Concat endpoint is the same thing, with slightly different
	//// middlewares to demonstrate how to specialize per-endpoint.
	//var concatEndpoint endpoint.Endpoint
	//{
	//	concatEndpoint = grpcTransport.NewClient(
	//		conn,
	//		"n13t.idm.v0.IdentityManagement.Add",
	//		"Concat",
	//		encodeGRPCConcatRequest,
	//		decodeGRPCConcatResponse,
	//		idmPb.ConcatReply{},
	//		append(options, grpcTransport.ClientBefore(opentracing.ContextToGRPC(otTracer, logger)))...,
	//	).Endpoint()
	//	concatEndpoint = opentracing.TraceClient(otTracer, "Concat")(concatEndpoint)
	//	concatEndpoint = limiter(concatEndpoint)
	//	concatEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
	//		Name:    "Concat",
	//		Timeout: 10 * time.Second,
	//	}))(concatEndpoint)
	//}

	var searchEndpoint endpoint.Endpoint
	{
		searchEndpoint = grpcTransport.NewClient(
			conn,
			"n13t.idm.v0.IdentityManagement",
			"Search",
			encodeGRPCSearchRequest,
			decodeGRPCSearchResponse,
			idmPb.SearchResponse{},
			append(
				options,
				grpcTransport.ClientBefore(
					opentracing.ContextToGRPC(otTracer, logger),
					jwt.ContextToGRPC(),
					),
				grpcTransport.ClientAfter(uma2.GRPCToContext()),
			)...,
		).Endpoint()
		searchEndpoint = opentracing.TraceClient(otTracer, "Search")(searchEndpoint)
		searchEndpoint = limiter(searchEndpoint)
		searchEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "Search",
			Timeout: 10 * time.Second,
		}))(searchEndpoint)
	}

	var listUsersEndpoint endpoint.Endpoint
	{
		listUsersEndpoint = grpcTransport.NewClient(
			conn,
			"n13t.idm.v0.IdentityManagement",
			"ListUsers",
			encodeGRPCListUsersRequest,
			decodeGRPCListUsersResponse,
			idmPb.ListUsersResponse{},
			append(options,
				grpcTransport.ClientBefore(
					opentracing.ContextToGRPC(otTracer, logger),
					jwt.ContextToGRPC(),
					),
				grpcTransport.ClientAfter(uma2.GRPCToContext()),
			)...,
		).Endpoint()
		listUsersEndpoint = opentracing.TraceClient(otTracer, "ListUsers")(listUsersEndpoint)
		listUsersEndpoint = limiter(listUsersEndpoint)
		listUsersEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "ListUsers",
			Timeout: 10 * time.Second,
		}))(listUsersEndpoint)
	}

	var createUserEndpoint endpoint.Endpoint
	{
		createUserEndpoint = grpcTransport.NewClient(
			conn,
			"n13t.idm.v0.IdentityManagement",
			"CreateUser",
			encodeGRPCCreateUserRequest,
			decodeGRPCCreateUserResponse,
			idmPb.CreateUserResponse{},
			append(options,
				grpcTransport.ClientBefore(opentracing.ContextToGRPC(otTracer, logger), jwt.ContextToGRPC()),
				grpcTransport.ClientAfter(uma2.GRPCToContext()),
			)...,
		).Endpoint()
		createUserEndpoint = opentracing.TraceClient(otTracer, "CreateUser")(createUserEndpoint)
		createUserEndpoint = limiter(createUserEndpoint)
		createUserEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "CreateUser",
			Timeout: 10 * time.Second,
		}))(createUserEndpoint)
		//createUserEndpoint = jwt.NewSigner(
		//	"kid-header",
		//	[]byte("SigningString"),
		//	stdjwt.SigningMethodHS256,
		//	jwt.Claims{},
		//)(createUserEndpoint)
	}

	var updateUserEndpoint endpoint.Endpoint
	{
		updateUserEndpoint = grpcTransport.NewClient(
			conn,
			"n13t.idm.v0.IdentityManagement",
			"UpdateUser",
			encodeGRPCUpdateUserRequest,
			decodeGRPCUpdateUserResponse,
			idmPb.UpdateUserResponse{},
			append(options,
				grpcTransport.ClientBefore(opentracing.ContextToGRPC(otTracer, logger), jwt.ContextToGRPC()),
				grpcTransport.ClientAfter(uma2.GRPCToContext()),
			)...,
		).Endpoint()
		updateUserEndpoint = opentracing.TraceClient(otTracer, "UpdateUser")(updateUserEndpoint)
		updateUserEndpoint = limiter(updateUserEndpoint)
		updateUserEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "UpdateUser",
			Timeout: 10 * time.Second,
		}))(updateUserEndpoint)
	}

	var deleteUserEndpoint endpoint.Endpoint
	{
		deleteUserEndpoint = grpcTransport.NewClient(
			conn,
			"n13t.idm.v0.IdentityManagement",
			"DeleteUser",
			encodeGRPCDeleteUserRequest,
			decodeGRPCDeleteUserResponse,
			idmPb.DeleteUserResponse{},
			append(options,
				grpcTransport.ClientBefore(opentracing.ContextToGRPC(otTracer, logger), jwt.ContextToGRPC()),
				grpcTransport.ClientAfter(uma2.GRPCToContext()),
			)...,
		).Endpoint()
		deleteUserEndpoint = opentracing.TraceClient(otTracer, "DeleteUser")(deleteUserEndpoint)
		deleteUserEndpoint = limiter(deleteUserEndpoint)
		deleteUserEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "DeleteUser",
			Timeout: 10 * time.Second,
		}))(deleteUserEndpoint)
	}

	var isValidEndpoint endpoint.Endpoint
	{
		isValidEndpoint = grpcTransport.NewClient(
			conn,
			"n13t.idm.v0.IdentityManagement",
			"IsValid",
			encodeGRPCIsValidRequest,
			decodeGRPCIsValidResponse,
			idmPb.IsValidResponse{},
			append(options, grpcTransport.ClientBefore(opentracing.ContextToGRPC(otTracer, logger)))...,
		).Endpoint()
		isValidEndpoint = opentracing.TraceClient(otTracer, "IsValid")(isValidEndpoint)
		isValidEndpoint = limiter(isValidEndpoint)
		isValidEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "IsValid",
			Timeout: 10 * time.Second,
		}))(isValidEndpoint)
	}

	var encryptEndpoint endpoint.Endpoint
	{
		encryptEndpoint = grpcTransport.NewClient(
			conn,
			"n13t.idm.v0.IdentityManagement",
			"Encrypt",
			encodeGRPCEncryptRequest,
			decodeGRPCEncryptResponse,
			idmPb.EncryptResponse{},
			append(options, grpcTransport.ClientBefore(opentracing.ContextToGRPC(otTracer, logger)))...,
		).Endpoint()
		encryptEndpoint = opentracing.TraceClient(otTracer, "Encrypt")(encryptEndpoint)
		encryptEndpoint = limiter(encryptEndpoint)
		encryptEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "Encrypt",
			Timeout: 10 * time.Second,
		}))(encryptEndpoint)
	}

	var decryptEndpoint endpoint.Endpoint
	{
		decryptEndpoint = grpcTransport.NewClient(
			conn,
			"n13t.idm.v0.IdentityManagement",
			"Decrypt",
			encodeGRPCDecryptRequest,
			decodeGRPCDecryptResponse,
			idmPb.DecryptResponse{},
			append(options, grpcTransport.ClientBefore(opentracing.ContextToGRPC(otTracer, logger)))...,
		).Endpoint()
		decryptEndpoint = opentracing.TraceClient(otTracer, "Decrypt")(decryptEndpoint)
		decryptEndpoint = limiter(decryptEndpoint)
		decryptEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
			Name:    "Decrypt",
			Timeout: 10 * time.Second,
		}))(decryptEndpoint)
	}

	// Returning the endpoint.Set as a service.Service relies on the
	// endpoint.Set implementing the Service methods. That's just a simple bit
	// of glue code.
	return idmEndpoint.Set{
		//SumEndpoint:        sumEndpoint,
		//ConcatEndpoint:     concatEndpoint,
		SearchEndpoint:		searchEndpoint,
		ListUsersEndpoint:	listUsersEndpoint,
		CreateUserEndpoint: createUserEndpoint,
		UpdateUserEndpoint: updateUserEndpoint,
		DeleteUserEndpoint: deleteUserEndpoint,
		IsValidEndpoint: 	isValidEndpoint,
		EncryptEndpoint: 	encryptEndpoint,
		DecryptEndpoint:	decryptEndpoint,
	}
}

/**
 * DECODE REQUEST FUNCTIONS
 */
// decodeGRPCSumRequest is a transport/grpc.DecodeRequestFunc that converts a
// gRPC sum request to a user-domain sum request. Primarily useful in a server.
//func decodeGRPCSumRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
//	req := grpcReq.(*idmPb.SumRequest)
//	return idmEndpoint.SumRequest{A: int(req.A), B: int(req.B)}, nil
//}
//
//// decodeGRPCConcatRequest is a transport/grpc.DecodeRequestFunc that converts a
//// gRPC concat request to a user-domain concat request. Primarily useful in a
//// server.
//func decodeGRPCConcatRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
//	req := grpcReq.(*idmPb.ConcatRequest)
//	return idmEndpoint.ConcatRequest{A: req.A, B: req.B}, nil
//}

func decodeGRPCSearchRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*idmPb.SearchRequest)
	return idmEndpoint.SearchRequest{Index: req.Index, Query: req.Query, Limit: req.Limit, Offset: req.Offset}, nil
}

func decodeGRPCListUsersRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*idmPb.ListUsersRequest)
	return idmEndpoint.ListUsersRequest{PageSize: req.PageSize, PageToken: req.PageToken}, nil
}

func decodeGRPCCreateUserRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*idmPb.CreateUserRequest)

	//if err := req.Validate(); err != nil {
	//	return nil, grpcStatus.Errorf(grpcCodes.InvalidArgument, "%v", err)
	//}

	return idmEndpoint.CreateUserRequest{Username: req.GetUser().Username, Email: req.GetUser().GetEmail(), Password: req.GetPassword()}, nil
}

func decodeGRPCUpdateUserRequest(_ context.Context, grpcReq interface{}) (interface{}, error) {
	req := grpcReq.(*idmPb.UpdateUserRequest)
	return idmEndpoint.UpdateUserRequest{
		UID:         req.User.Sub,
		Password: 	 req.User.Password,
		GivenName:   req.User.GivenName,
		FamilyName:  req.User.FamilyName,
		MiddleName:  req.User.MiddleName,
		Nickname:    req.User.Nickname,
		Gender:      req.User.Gender,
		Birthday:    req.User.Birthdate,
		ZoneInfo:    req.User.Zoneinfo,
		Locale:      req.User.Locale,
		Email: 		 req.User.Email,
		PhoneNumber: req.User.PhoneNumber,
		ProfileURL:  req.User.Profile,
		PictureURL:          req.User.Picture,
		WebsiteURL:          req.User.Website,
		StreetAddress:       req.User.Address.StreetAddress,
		Locality:            req.User.Address.Locality,
		Region:              req.User.Address.Region,
		PostalCode:          req.User.Address.PostalCode,
		Country:             req.User.Address.Country,
		EmailVerified:       req.User.EmailVerified,
		PhoneNumberVerified: req.User.PhoneNumberVerified,
	}, nil
}

func decodeGRPCDeleteUserRequest(_ context.Context, grpcRequest interface{}) (interface{}, error) {
	req := grpcRequest.(*idmPb.DeleteUserRequest)
	return idmEndpoint.DeleteUserRequest{UID: req.Id}, nil
}

func decodeGRPCIsValidRequest(_ context.Context, grpcRequest interface{}) (interface{}, error) {
	req := grpcRequest.(*idmPb.IsValidRequest)
	return idmEndpoint.IsValidRequest{Id: req.Id, Password: req.Password}, nil
}

func decodeGRPCEncryptRequest(_ context.Context, grpcRequest interface{}) (interface{}, error) {
	req := grpcRequest.(*idmPb.EncryptRequest)
	return idmEndpoint.EncryptRequest{Plaintext: req.Plaintext}, nil
}

func decodeGRPCDecryptRequest(_ context.Context, grpcRequest interface{}) (interface{}, error) {
	req := grpcRequest.(*idmPb.DecryptRequest)
	return idmEndpoint.DecryptRequest{Ciphertext: req.Ciphertext}, nil
}
/**
 * DECODE RESPONSE FUNCTIONS
 */
// decodeGRPCSumResponse is a transport/grpc.DecodeResponseFunc that converts a
// gRPC sum reply to a user-domain sum response. Primarily useful in a client.
//func decodeGRPCSumResponse(_ context.Context, grpcReply interface{}) (interface{}, error) {
//	reply := grpcReply.(*idmPb.SumReply)
//	return idmEndpoint.SumResponse{V: int(reply.V), Err: str2err(reply.Err)}, nil
//}
//
//// decodeGRPCConcatResponse is a transport/grpc.DecodeResponseFunc that converts
//// a gRPC concat reply to a user-domain concat response. Primarily useful in a
//// client.
//func decodeGRPCConcatResponse(_ context.Context, grpcReply interface{}) (interface{}, error) {
//	reply := grpcReply.(*idmPb.ConcatReply)
//	return idmEndpoint.ConcatResponse{V: reply.V, Err: str2err(reply.Err)}, nil
//}

func decodeGRPCSearchResponse(ctx context.Context, grpcRes interface{}) (interface{}, error) {
	res := grpcRes.(*idmPb.SearchResponse)
	var users []idmService.User

	for _, u := range res.Users {
		createdAt, _ := ptypes.Timestamp(u.CreatedAt)
		UpdatedAt, _ := ptypes.Timestamp(u.UpdatedAt)

		users = append(users,
			idmService.User{
				ID: u.Sub,
				Username: u.Username,
				Email: u.Email,
				EmailVerified: u.EmailVerified,
				PhoneNumber: u.PhoneNumber,
				PhoneNumberVerified: u.PhoneNumberVerified,
				Birthday: u.Birthdate,
				Gender: u.Gender,
				Locale: u.Locale,
				StreetAddress: u.Address.StreetAddress,
				Country: u.Address.Country,
				PostalCode: u.Address.PostalCode,
				Locality: u.Address.Locality,
				Region: u.Address.Region,

				WebsiteURL: u.Website,
				ProfileURL: u.Profile,
				PictureURL: u.Picture,

				ZoneInfo: u.Zoneinfo,
				Nickname: u.Nickname,
				MiddleName: u.MiddleName,
				FamilyName: u.FamilyName,
				GivenName: u.GivenName,

				CreatedAt: createdAt,
				UpdatedAt: UpdatedAt,
			},
		)
	}

	fmt.Println("decode ", res.Err)
	if res.Err == err2str(idmService.ErrPermissionDenied) {
		if ticket := ctx.Value(uma2.UMA2TicketContextKey); ticket != nil {
			fmt.Println("received ticket")

			errWrapper := uma2.UMA2TicketError{
				Error: err2str(idmService.ErrPermissionDenied),
				Ticket: *ticket.(*uma2.Ticket),
			}
			ticketString, _ := json.Marshal(errWrapper)

			return idmEndpoint.SearchResponse{Users: users, Err: str2err(string(ticketString))}, nil
			//return , nil
		} else {
			panic("Server SHOULD response permission denied with ticket")
		}
	} else {
		return idmEndpoint.SearchResponse{Users: users, Err: str2err(res.Err)}, nil
	}
}

func decodeGRPCListUsersResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
	res := grpcResponse.(*idmPb.ListUsersResponse)

	var users []idmService.User

	for _, u := range res.Users {
		createdAt, _ := ptypes.Timestamp(u.CreatedAt)
		updatedAt, _ := ptypes.Timestamp(u.UpdatedAt)
		users = append(users,
			idmService.User{
				ID: u.Sub,
				Username: u.Username,
				Email: u.Email,
				EmailVerified: u.EmailVerified,
				Birthday: u.Birthdate,
				Gender: u.Gender,
				Locale: u.Locale,
				StreetAddress: u.Address.StreetAddress,
				Country: u.Address.Country,
				PostalCode: u.Address.PostalCode,
				Locality: u.Address.Locality,
				Region: u.Address.Region,

				PhoneNumber: u.PhoneNumber,
				PhoneNumberVerified: u.PhoneNumberVerified,

				WebsiteURL: u.Website,
				ProfileURL: u.Profile,
				PictureURL: u.Picture,

				ZoneInfo: u.Zoneinfo,
				Nickname: u.Nickname,
				MiddleName: u.MiddleName,
				FamilyName: u.FamilyName,
				GivenName: u.GivenName,

				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
			},
		)
	}

	return idmEndpoint.ListUsersResponse{Users: users, NextPageToken: res.NextPageToken, Err: str2err(res.Err)}, nil
}

func decodeGRPCCreateUserResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
	r := grpcResponse.(*idmPb.CreateUserResponse)

	return idmEndpoint.CreateUserResponse{Err: str2err(r.Err)}, nil
}

func decodeGRPCUpdateUserResponse(_ context.Context, grpcReply interface{}) (interface{}, error) {
	r := grpcReply.(*idmPb.UpdateUserResponse)
	return idmEndpoint.UpdateUserResponse{Err: str2err(r.Err)}, nil
}

func decodeGRPCDeleteUserResponse(_ context.Context, grpcReply interface{}) (interface{}, error) {
	r := grpcReply.(*idmPb.DeleteUserResponse)
	return idmEndpoint.DeleteUserResponse{Err: str2err(r.Err)}, nil
}

func decodeGRPCIsValidResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
	resp := grpcResponse.(*idmEndpoint.IsValidResponse)
	return idmEndpoint.IsValidResponse{V: resp.V, Err: resp.Err}, nil
}

func decodeGRPCEncryptResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
	r := grpcResponse.(*idmPb.EncryptResponse)

	return idmEndpoint.EncryptResponse{Ciphertext: r.Ciphertext, Err: str2err(r.Err)}, nil
}

func decodeGRPCDecryptResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
	r := grpcResponse.(*idmPb.DecryptResponse)

	return idmEndpoint.DecryptResponse{Plaintext: r.Plaintext, Err: str2err(r.Err)}, nil
}

/**
 * ENCODE RESPONSE FUNCTIONS
 */
// encodeGRPCSumResponse is a transport/grpc.EncodeResponseFunc that converts a
// user-domain sum response to a gRPC sum reply. Primarily useful in a server.
//func encodeGRPCSumResponse(_ context.Context, response interface{}) (interface{}, error) {
//	resp := response.(idmEndpoint.SumResponse)
//	return &idmPb.SumReply{V: int64(resp.V), Err: err2str(resp.Err)}, nil
//}
//
//// encodeGRPCConcatResponse is a transport/grpc.EncodeResponseFunc that converts
//// a user-domain concat response to a gRPC concat reply. Primarily useful in a
//// server.
//func encodeGRPCConcatResponse(_ context.Context, response interface{}) (interface{}, error) {
//	resp := response.(idmEndpoint.ConcatResponse)
//	return &idmPb.ConcatReply{V: resp.V, Err: err2str(resp.Err)}, nil
//}

func encodeGRPCSearchResponse(ctx context.Context, response interface{}) (interface{}, error) {
	//switch response.(type) {
	//case uma2.Ticket:
	//	ctx.Value()
	//}
	resp := response.(idmEndpoint.SearchResponse)


	var pbUsers []*idmPb.User
	for _, u := range resp.Users {
		createdAt, _ := ptypes.TimestampProto(u.CreatedAt)
		updatedAt, _ := ptypes.TimestampProto(u.UpdatedAt)

		pbUsers = append(pbUsers,
			&idmPb.User{
				Username: 			u.Username,
				Enabled:             true,
				Sub:                 u.ID,
				Name:                "",
				GivenName:           u.GivenName,
				FamilyName:          u.FamilyName,
				MiddleName:          u.MiddleName,
				Nickname:            u.Nickname,
				PreferredUsername:   "",
				Profile:             u.ProfileURL,
				Picture:             u.PictureURL,
				Website:             u.WebsiteURL,
				Email:               u.Email,
				EmailVerified:       u.EmailVerified,
				Gender:              u.Gender,
				Birthdate:           u.Birthday,
				Zoneinfo:            u.ZoneInfo,
				Locale:              u.Locale,
				PhoneNumber:         u.PhoneNumber,
				PhoneNumberVerified: u.PhoneNumberVerified,
				Address: &idmPb.User_Address{
					Formatted:     "",
					StreetAddress: u.StreetAddress,
					Locality:      u.Locality,
					Region:        u.Region,
					PostalCode:    u.PostalCode,
					Country:       u.Country,
				},
				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
				//CreatedAt: &timestamppb.Timestamp{Seconds: int64(u.CreatedAt.Second()), Nanos: int32(u.CreatedAt.Nanosecond())},
				//UpdatedAt: &timestamppb.Timestamp{Seconds: int64(u.UpdatedAt.Second()), Nanos: int32(u.UpdatedAt.Nanosecond())},
			},
		)
	}

	return &idmPb.SearchResponse{Users: pbUsers, Err: err2str(resp.Err)}, nil
}

func encodeGRPCListUsersResponse(_ context.Context, response interface{}) (interface{}, error) {
	r := response.(idmEndpoint.ListUsersResponse)

	var pbUsers []*idmPb.User
	for _, u := range r.Users {
		createdAt, _ := ptypes.TimestampProto(u.CreatedAt)
		updatedAt, _ := ptypes.TimestampProto(u.UpdatedAt)

		pbUsers = append(pbUsers,
			&idmPb.User{
				Username: 			u.Username,
				Enabled:             true,
				Sub:                 u.ID,
				Name:                "",
				GivenName:           u.GivenName,
				FamilyName:          u.FamilyName,
				MiddleName:          u.MiddleName,
				Nickname:            u.Nickname,
				PreferredUsername:   "",
				Profile:             u.ProfileURL,
				Picture:             u.PictureURL,
				Website:             u.WebsiteURL,
				Email:               u.Email,
				EmailVerified:       u.EmailVerified,
				Gender:              u.Gender,
				Birthdate:           u.Birthday,
				Zoneinfo:            u.ZoneInfo,
				Locale:              u.Locale,
				PhoneNumber:         u.PhoneNumber,
				PhoneNumberVerified: u.PhoneNumberVerified,
				Address: &idmPb.User_Address{
					Formatted:     "",
					StreetAddress: u.StreetAddress,
					Locality:      u.Locality,
					Region:        u.Region,
					PostalCode:    u.PostalCode,
					Country:       u.Country,
				},
				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
			},
		)
	}

	return &idmPb.ListUsersResponse{Users: pbUsers, NextPageToken: r.NextPageToken, Err: err2str(r.Err)}, nil
}

func encodeGRPCCreateUserResponse(_ context.Context, response interface{}) (interface{}, error) {
	r := response.(idmEndpoint.CreateUserResponse)
	return &idmPb.CreateUserResponse{Err: err2str(r.Err)}, nil
}

func encodeGRPCUpdateUserResponse(_ context.Context, response interface{}) (interface{}, error) {
	resp := response.(idmEndpoint.UpdateUserResponse)
	return &idmPb.UpdateUserResponse{Err: err2str(resp.Err)}, nil
}

func encodeGRPCIsValidResponse(_ context.Context, response interface{}) (interface{}, error) {
	r := response.(idmEndpoint.IsValidResponse)
	return &idmPb.IsValidResponse{V: r.V, Err: err2str(r.Err)}, nil
}

func encodeGRPCDeleteUserResponse(_ context.Context, response interface{}) (interface{}, error) {
	r := response.(idmEndpoint.DeleteUserResponse)
	return &idmPb.DeleteUserResponse{Err: err2str(r.Err)}, nil
}

func encodeGRPCEncryptResponse(_ context.Context, response interface{}) (interface{}, error) {
	r := response.(idmEndpoint.EncryptResponse)
	return &idmPb.EncryptResponse{Ciphertext: r.Ciphertext, Err: err2str(r.Err)}, nil
}

func encodeGRPCDecryptResponse(_ context.Context, response interface{}) (interface{}, error) {
	r := response.(idmEndpoint.DecryptResponse)
	return &idmPb.DecryptResponse{Plaintext: r.Plaintext, Err: err2str(r.Err)}, nil
}

/**
 * ENCODE REQUEST FUNCTIONS
 */
// encodeGRPCSumRequest is a transport/grpc.EncodeRequestFunc that converts a
// user-domain sum request to a gRPC sum request. Primarily useful in a client.
//func encodeGRPCSumRequest(_ context.Context, request interface{}) (interface{}, error) {
//	req := request.(idmEndpoint.SumRequest)
//	return &idmPb.SumRequest{A: int64(req.A), B: int64(req.B)}, nil
//}
//
//// encodeGRPCConcatRequest is a transport/grpc.EncodeRequestFunc that converts a
//// user-domain concat request to a gRPC concat request. Primarily useful in a
//// client.
//func encodeGRPCConcatRequest(_ context.Context, request interface{}) (interface{}, error) {
//	req := request.(idmEndpoint.ConcatRequest)
//	return &idmPb.ConcatRequest{A: req.A, B: req.B}, nil
//}

func encodeGRPCSearchRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(idmEndpoint.SearchRequest)
	return &idmPb.SearchRequest{Index: req.Index, Query: req.Query, Limit: req.Limit, Offset: req.Offset}, nil
}

func encodeGRPCListUsersRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(idmEndpoint.ListUsersRequest)
	return &idmPb.ListUsersRequest{PageSize: req.PageSize, PageToken: req.PageToken}, nil
}

func encodeGRPCCreateUserRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(idmEndpoint.CreateUserRequest)
	return &idmPb.CreateUserRequest{User: &idmPb.User{Username: req.Username, Email: req.Email, Password: req.Password}, Password: req.Password}, nil
}

func encodeGRPCUpdateUserRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(idmEndpoint.UpdateUserRequest)
	return &idmPb.UpdateUserRequest{
		User: &idmPb.User{
			Enabled:             true,
			Sub:                 req.UID,
			Password: 			 req.Password,
			Name:                "",
			GivenName:           req.GivenName,
			FamilyName:          req.FamilyName,
			MiddleName:          req.MiddleName,
			Nickname:            req.Nickname,
			PreferredUsername:   "",
			Profile:             req.ProfileURL,
			Picture:             req.PictureURL,
			Website:             req.WebsiteURL,
			Email:               req.Email,
			EmailVerified:       req.EmailVerified,
			Gender:              req.Gender,
			Birthdate:           req.Birthday,
			Zoneinfo:            req.ZoneInfo,
			Locale:              req.Locale,
			PhoneNumber:         req.PhoneNumber,
			PhoneNumberVerified: req.PhoneNumberVerified,
			Address: &idmPb.User_Address{
				Formatted:     "",
				StreetAddress: req.StreetAddress,
				Locality:      req.Locality,
				Region:        req.Region,
				PostalCode:    req.PostalCode,
				Country:       req.Country,
			},
		},
	}, nil
}

func encodeGRPCDeleteUserRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(idmEndpoint.DeleteUserRequest)
	return &idmPb.DeleteUserRequest{Id: req.UID}, nil
}

func encodeGRPCIsValidRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(idmEndpoint.IsValidRequest)
	return &idmPb.IsValidRequest{Id: req.Id, Password: req.Password}, nil
}

func encodeGRPCEncryptRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(idmEndpoint.EncryptRequest)
	return &idmPb.EncryptRequest{Plaintext: req.Plaintext}, nil
}

func encodeGRPCDecryptRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(idmEndpoint.DecryptRequest)
	return &idmPb.DecryptRequest{Ciphertext: req.Ciphertext}, nil
}

// These annoying helper functions are required to translate Go error types to
// and from strings, which is the type we use in our IDLs to represent errors.
// There is special casing to treat empty strings as nil errors.

func str2err(s string) error {
	if s == "" {
		return nil
	}
	return errors.New(s)
}

func err2str(err error) string {
	if err == nil {
		return ""
	}
	return err.Error()
}
