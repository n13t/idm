package idmtransport

import (
	"gitlab.com/n13t/idm/pkg/idmendpoint"
	"gitlab.com/n13t/idm/pkg/idmservice"
	"testing"
	"time"
)

func TestGRPCListUsersResponse(t *testing.T) {
	response := idmendpoint.ListUsersResponse{
		Users: []idmservice.User{
			{
				ID: "123", Username: "myUsername",
				Email: "myEmail@example.com", EmailVerified: true,
				PhoneNumber: "012345678", PhoneNumberVerified: true,
				CreatedAt: time.Now(), UpdatedAt: time.Now(),
			},
		},
	}

	grpcListUsersResponse, _ := encodeGRPCListUsersResponse(nil, response)
	v, _ := decodeGRPCListUsersResponse(nil, grpcListUsersResponse)
	decoded := v.(idmendpoint.ListUsersResponse)

	if response.Users[0].ID != decoded.Users[0].ID ||
		response.Users[0].Username != decoded.Users[0].Username ||
		response.Users[0].PhoneNumber != decoded.Users[0].PhoneNumber ||
		response.Users[0].PhoneNumberVerified != decoded.Users[0].PhoneNumberVerified ||

		response.Users[0].Email != decoded.Users[0].Email ||
		response.Users[0].EmailVerified != decoded.Users[0].EmailVerified {
		t.Errorf("FAILED")
	}

	if 	response.Users[0].CreatedAt.Local() != decoded.Users[0].CreatedAt.Local(){
		t.Errorf("want %s have %s\n", response.Users[0].CreatedAt.Local(), decoded.Users[0].CreatedAt.Local())
	}

	if 	response.Users[0].UpdatedAt.Local() != decoded.Users[0].UpdatedAt.Local() {
		t.Errorf("want %s have %s\n", response.Users[0].UpdatedAt.Local(), decoded.Users[0].UpdatedAt.Local())
	}
}
