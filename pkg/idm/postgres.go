package idm

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"os"
)

func NewPostgres() *sql.DB {
	databaseURL := os.Getenv("DATABASE_URL")
	//redisURL := os.Getenv("REDIS_URL")

	if databaseURL != "" {
		db, err := sql.Open("postgres", databaseURL)
		if err != nil {
			fmt.Print(err)
			panic("failed to connect database")
		}

		db.SetMaxOpenConns(5)

		return db
	}

	return nil
}
