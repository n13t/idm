package idm

import (
	"context"
	"database/sql"
	"google.golang.org/grpc/health/grpc_health_v1"
	"time"
)

type HealthServer struct {
	grpc_health_v1.UnimplementedHealthServer
	Db *sql.DB
}

func (s *HealthServer) Check(ctx context.Context, request *grpc_health_v1.HealthCheckRequest) (*grpc_health_v1.HealthCheckResponse, error) {
	stt := grpc_health_v1.HealthCheckResponse_SERVING

	if request.Service != "" && request.Service != "idm" {
		stt = grpc_health_v1.HealthCheckResponse_SERVICE_UNKNOWN
	}

	if s.Db != nil {
		ctx, cancel := context.WithTimeout(ctx, 500*time.Millisecond)
		defer cancel()
		stt = grpc_health_v1.HealthCheckResponse_SERVING
		if err := s.Db.PingContext(ctx); err != nil {
			stt = grpc_health_v1.HealthCheckResponse_NOT_SERVING
		}
	}
	return &grpc_health_v1.HealthCheckResponse{Status: stt}, nil
}

func (s *HealthServer) Watch(_ *grpc_health_v1.HealthCheckRequest, stream grpc_health_v1.Health_WatchServer) error {

	for true {
		if err := stream.Send(&grpc_health_v1.HealthCheckResponse{Status: grpc_health_v1.HealthCheckResponse_SERVING}); err != nil {
			return err
		}
		time.Sleep(3 * time.Second)
	}
	return nil
}

