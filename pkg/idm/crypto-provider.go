package idm

import (
	"crypto/tls"
	"fmt"
	"github.com/go-kit/kit/log"
	vaultApi "github.com/hashicorp/vault/api"
	stdopentracing "github.com/opentracing/opentracing-go"
	"github.com/openzipkin/zipkin-go"
	"gitlab.com/n13t/idm/pkg/idmservice"
	"gitlab.com/n13t/idm/pkg/idmservice/cryptor"
	cryptorImpl "gitlab.com/n13t/idm/pkg/idmservice/cryptor/impl"
	"gitlab.com/n13t/idm/pkg/idmtransport"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"net/http"
	"os"
	"time"
)


func NewCryptoProvider(zipkinTracer *zipkin.Tracer) cryptor.Provider {
	if addr := os.Getenv("IDM_ES_ADDR"); addr != "" {
		idmClient := newIdmCryptoProvider(addr, os.Getenv("IDM_ES_TLS") == "true", zipkinTracer)
		return *idmClient
	}

	if url := os.Getenv("VAULT_URL"); url != "" {
		vaultProvider := newHashiCorpVaultProvider(url)
		return *vaultProvider
	}

	return nil
}

func newIdmCryptoProvider(addr string, tlsEnabled bool, zipkinTracer *zipkin.Tracer) *idmservice.Service {
	var opts []grpc.DialOption

	if tlsEnabled {
		opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{InsecureSkipVerify: true})))
	} else {
		opts = append(opts, grpc.WithInsecure())
	}
	//opts = append(opts, grpc.WithBlock())

	//fmt.Println(opts)
	conn, err := grpc.Dial(addr, opts...)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v", err)
		os.Exit(1)
	}

	var otTracer stdopentracing.Tracer
	{
		otTracer = stdopentracing.GlobalTracer() // no-op
	}


	svc := idmtransport.NewGRPCClient(conn, otTracer, zipkinTracer, log.NewNopLogger())
	return &svc
}

func newHashiCorpVaultProvider(url string) *cryptor.Provider {
	vaultClient, err := vaultApi.NewClient(&vaultApi.Config{
		Address: url,
		HttpClient: &http.Client{Timeout: 10 * time.Second},
	})
	if err != nil {
		panic(err)
	}
	vaultClient.SetToken(os.Getenv("VAULT_TOKEN"))

	p := cryptorImpl.NewHashiCorpVaultCryptoProvider(vaultClient)

	return &p
}
