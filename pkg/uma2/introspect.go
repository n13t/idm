package uma2

import "time"

// https://tools.ietf.org/html/rfc7009#section-2.1
type IntrospectRequest struct {
	Token         string `json:"token"`           // REQUIRED
	TokenTypehint string `json:"token_type_hint"` // OPTIONAL
}

// https://docs.kantarainitiative.org/uma/wg/rec-oauth-uma-federated-authz-2.0.html#rfc.section.5.1.1
type IntrospectionObject struct {
	// OAuth2 - RFC7662 (https://tools.ietf.org/html/rfc7662#section-2.2)
	Active    bool      `json:"active"`     // REQUIRED
	Scope     string    `json:"scope"`      // OPTIONAL. Not available in UMA2 context
	ClientId  string    `json:"client_id"`  // OPTIONAL
	Username  string    `json:"username"`   // OPTIONAL
	TokenType string    `json:"token_type"` // OPTIONAL
	Exp       time.Time `json:"exp"`        // OPTIONAL
	Iat       time.Time `json:"iat"`        // OPTIONAL
	Nbf       time.Time `json:"nbf"`        // OPTIONAL
	Sub       string    `json:"sub"`        // OPTIONAL
	Aud       string    `json:"aud"`        // OPTIONAL
	Iss       string    `json:"iss"`        // OPTIONAL
	Jti       string    `json:"jti"`        // OPTIONAL

	// UMA2
	Permissions []struct{
		ResourceId     string    `json:"resource_id"`     // REQUIRED
		ResourceScopes []string  `json:"resource_scopes"` // REQUIRED
		Exp            time.Time `json:"exp"`             // OPTIONAL
		Iat            time.Time `json:"iat"`             // OPTIONAL
		Nbf            time.Time `json:"nbf"`             // OPTIONAL
	} `json:"permissions"`
}
