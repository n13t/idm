package uma2

type Resource struct {
	Id			   string 	`json:"-"`
	Name           string   `json:"name,omitempty"`
	Type           string   `json:"type,omitempty"`
	ResourceScopes []string `json:"resource_scopes,omitempty"`
	Description    string 	`json:"description,omitempty"`
	IconUri        string 	`json:"icon_uri,omitempty"`
}

type ReadOnlyResource struct {
	Name           string   `json:"name,omitempty"`
	Type           string   `json:"type,omitempty"`
	ResourceScopes []ReadOnlyResourceScope `json:"resource_scopes,omitempty"`
	Description    string 	`json:"description,omitempty"`
	IconUri        string 	`json:"icon_uri,omitempty"`

	// readonly
	Id string `json:"_id,omitempty"`
	Owner ReadOnlyOwner `json:"owner,omitempty"`
	OwnerManagedAccess bool `json:"ownerManagedAccess"`
	ReadOnlyResourceScopes []ReadOnlyResourceScope `json:"resource_scopes,omitempty"`
}

type ReadOnlyOwner struct {
	Id string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type ReadOnlyResourceScope struct {
	Id string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type CreateResourceResponse struct {
	Id                  string `json:"_id,omitempty"`
	UserAccessPolicyUri string `json:"user_access_policy_uri,omitempty"`
}