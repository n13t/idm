package uma2

import (
	"context"
	"fmt"
	stdhttp "net/http"
	"strings"

	"google.golang.org/grpc/metadata"

	"github.com/go-kit/kit/transport/grpc"
	"github.com/go-kit/kit/transport/http"
)

const (
	umaFormat string = `UMA realm="%s", as_uri="%s", ticket="%s"`
)

// HTTPToContext moves a JWT from request header to context. Particularly
// useful for servers.
func HTTPToContext() http.ServerResponseFunc {
	return func(ctx context.Context, r stdhttp.ResponseWriter) context.Context {
		//token, ok := extractTicketFromAuthHeader(r.Header.Get("WWW-Authenticate"))
		//if !ok {
		//	return ctx
		//}
		//
		//return context.WithValue(ctx, UMA2TicketContextKey, token)
		return ctx
	}
}

// ContextToHTTP moves a JWT from context to request header. Particularly
// useful for clients.
func ContextToHTTP() http.ClientResponseFunc {
	return func(ctx context.Context, r *stdhttp.Response) context.Context {
		ticket, ok := ctx.Value(UMA2TicketContextKey).(Ticket)
		if ok {
			r.Header.Add("WWW-Authenticate", generateAuthHeaderFromToken(ticket.Realm, ticket.AsUri, ticket.Ticket))
		}
		return ctx
	}
}

/**
 * for Server
 */

// Init a reference to be set inside service
func ContextInitialize() grpc.ServerRequestFunc {
	return func(ctx context.Context, md metadata.MD) context.Context {
		return context.WithValue(ctx, UMA2TicketContextKey, &Ticket{})
	}
}

// Unload ticket from ctx to response for client
func ContextToGRPC() grpc.ServerResponseFunc {
	return func(ctx context.Context, md *metadata.MD, trailer *metadata.MD) context.Context {
		v := ctx.Value(UMA2TicketContextKey)
		if v != nil {
			ticket := v.(*Ticket)
			key, val := grpc.EncodeKeyValue("WWW-Authenticate", generateAuthHeaderFromToken(ticket.Realm, ticket.AsUri, ticket.Ticket))
			//(*md)[key] = append((*md)[key], val)
			*md = metadata.Join(*md, metadata.Pairs(key, val))
		}

		return ctx
	}
}

// for client: response metadata -> decode ctx
func GRPCToContext() grpc.ClientResponseFunc {
	return func(ctx context.Context, md metadata.MD, trailer metadata.MD) context.Context {
		// capital "Key" is illegal in HTTP/2.
		headers := md["www-authenticate"]

		if len(headers) > 0 {
			ticket, ok := extractTicketFromAuthHeader(headers[0])
			if ok {
				ctx = context.WithValue(ctx, UMA2TicketContextKey, ticket)
			}
		}

		return ctx
	}
}

func extractTicketFromAuthHeader(val string) (ticket *Ticket, ok bool) {
	splitByComma := strings.Split(val, ",")

	if len(splitByComma) < 3 {
		return nil, false
	}

	ticket = &Ticket{}
	for _, attr := range splitByComma {
		splitByEqual := strings.Split(strings.Trim(attr, " "),"=")
		key := splitByEqual[0]
		val := strings.Trim(splitByEqual[1], "\"")

		switch strings.ToLower(key) {
		case "uma realm": ticket.Realm = val
		case "as_uri": ticket.AsUri = val
		case "ticket": ticket.Ticket = val
		}
	}

	return ticket, true
}

func generateAuthHeaderFromToken(realm, asUri, ticket string) string {
	return fmt.Sprintf(umaFormat, realm, asUri, ticket)
}
