package uma2

import "encoding/json"

// Keycloak only support multi permission request (not accept single permission)
type PermissionRequest struct {
	permissions []Permission
}

type PermissionResponse struct {
	Ticket string `json:"ticket"`
}

type Permission struct {
	ResourceId 		string		`json:"resource_id,omitempty"`
	ResourceScopes 	[]string 	`json:"resource_scopes,omitempty"`
	Claim			interface{} `json:"claim,omitempty"`
}

func (p *PermissionRequest) Add(perm Permission) {
	p.permissions = append(p.permissions, perm)
}

func (p *PermissionRequest) Marshal() ([]byte, error) {
	return json.Marshal(p.permissions)
}
