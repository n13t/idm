package uma2

import (
	"fmt"
	"testing"
)

func TestExtractTicketFromAuthHeader(t *testing.T) {
	for _, ticket := range []Ticket{
		{Realm: "example", AsUri: "https://as.example.com", Ticket: "01234"},
	} {
		v := fmt.Sprintf(umaFormat, ticket.Realm, ticket.AsUri, ticket.Ticket)

		tic, _ := extractTicketFromAuthHeader(v)

		if ticket.Realm != tic.Realm {
			t.Errorf("want %s have %s", ticket.Realm, tic.Realm)
		}
		if ticket.AsUri != tic.AsUri {
			t.Errorf("want %s have %s", ticket.AsUri, tic.AsUri)
		}
		if ticket.Ticket != tic.Ticket {
			t.Errorf("want %s have %s", ticket.Ticket, tic.Ticket)
		}
	}
}
