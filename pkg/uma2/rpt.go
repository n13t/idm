package uma2

import "golang.org/x/oauth2"

// https://docs.kantarainitiative.org/uma/wg/rec-oauth-uma-grant-2.0.html#rfc.section.3.3.1
type RPTRequest struct {
	// REQUIRED fields
	GrantType string `json:"grant_type,omitempty"` 	// required
	Ticket    string `json:"ticket,omitempty"`		// required

	// OPTIONAL fields
	ClaimToken       string `json:"claim_token,omitempty"`
	ClaimTokenFormat string `json:"claim_token_format,omitempty"`
	Pct              string `json:"pct,omitempty"`
	Rpt              string `json:"rpt,omitempty"`
	Scope            string `json:"scope,omitempty"`
}

// https://docs.kantarainitiative.org/uma/wg/rec-oauth-uma-grant-2.0.html#rfc.section.3.3.5
type RequestPartyToken struct {
	oauth2.Token

	// OPTIONAL fields
	Pct      string `json:"pct,omitempty"`
	Upgraded bool   `json:"upgraded"`
}
