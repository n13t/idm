package uma2

type Config struct {
	Oauth2Config interface{}
	ServerUrl string
	WellKnownConfig *WellKnownConfig
}
