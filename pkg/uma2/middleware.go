package uma2

//noinspection GoNameStartsWithPackageName
const (
	//UMA2TicketContextKey
	UMA2TicketContextKey string = "UMA2Ticket"
)

type Ticket struct {
	Realm  string `json:"realm,omitempty"`
	AsUri  string `json:"as_uri,omitempty"`
	Ticket string `json:"ticket,omitempty"`
}
