package uma2

import (
	"testing"
)

var testHasUMAServer bool

func init() {
	//if _, err := exec.LookPath("git"); err == nil {
		//testHasGit = true
	//}

	// if has UMA Server with CLIENT_ID, CLIENT_SECRET -> set to true
	testHasUMAServer = false
}

func TestUMA(t *testing.T) {
	if !testHasUMAServer {
		t.Log("UMA Server not found, skipping")
		t.Skip()
	}
}
