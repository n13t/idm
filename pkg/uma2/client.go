package uma2

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
	"io/ioutil"
	"net/http"
	"strings"
)

//func (u uma2Client) CreateResource() error {
//fmt.Println(u.uma2WellKnownConfig.ResourceRegistrationEndpoint)
//resp, err := u.oauth2.Client(context.Background()).Post(u.uma2WellKnownConfig.ResourceRegistrationEndpoint)
//if err != nil {
//	fmt.Println(err)
//	return nil, err
//}
//
//fmt.Printf("%s\n", resp.Status)
//fmt.Printf("%s\n", resp.Header)
//
//body, _ := ioutil.ReadAll(resp.Body)
//defer resp.Body.Close()
//
//
//fmt.Printf("%v\n", string(body))
//
//return nil, err
//}

const (
	CreateTicketGrantType = "urn:ietf:params:oauth:grant-type:uma-ticket"
)

func (c Config) ListResources(query string) ([]string, error) {
	var client *http.Client
	switch c.Oauth2Config.(type) {
	case *clientcredentials.Config:
		client = c.Oauth2Config.(*clientcredentials.Config).Client(context.Background())
	default:
		return nil, errors.New("config(uma2): config MUST use client credentials flow")
	}

	resp, err := client.Get(c.WellKnownConfig.ResourceRegistrationEndpoint + query)
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	// TODO cleanup
	var resources []string
	err = json.Unmarshal(body, &resources)
	if err != nil {
		fmt.Printf("snap: JSON error: %s", err)
		return nil, err
	}

	return resources, nil
}

func (c Config) CreateResource(resource *Resource) (*CreateResourceResponse, error) {
	var client *http.Client
	switch c.Oauth2Config.(type) {
	case *clientcredentials.Config:
		client = c.Oauth2Config.(*clientcredentials.Config).Client(context.Background())
	default:
		return nil, errors.New("config(uma2): config MUST use client credentials flow")
	}

	// marshal User to json
	reqBody, err := json.Marshal(resource)
	if err != nil {
		panic(err)
	}

	req, _ := http.NewRequest(http.MethodPost, c.WellKnownConfig.ResourceRegistrationEndpoint, bytes.NewBuffer(reqBody))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// process the response
	defer res.Body.Close()
	var response CreateResourceResponse
	body, _ := ioutil.ReadAll(res.Body)

	// unmarshal the json into a string map
	err = json.Unmarshal(body, &response)
	if err != nil {
		fmt.Printf("snap: JSON error: %s", err)
		return nil, err
	}

	fmt.Printf("%v\n", response)

	return nil, err
}

func (c Config) UpdateResource(resource Resource) (interface{}, error) {
	var client *http.Client
	switch c.Oauth2Config.(type) {
	case *clientcredentials.Config:
		client = c.Oauth2Config.(*clientcredentials.Config).Client(context.Background())
	default:
		return nil, errors.New("config(uma2): config MUST use client credentials flow")
	}

	// marshal User to json
	reqBody, err := json.Marshal(resource)
	if err != nil {
		panic(err)
	}

	req, _ := http.NewRequest(http.MethodPut, c.WellKnownConfig.ResourceRegistrationEndpoint, bytes.NewBuffer(reqBody))
	//req.Header.Set("Content-Type", "application/json; charset=utf-8")
	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// process the response
	defer res.Body.Close()
	var responseData map[string]interface{}
	body, _ := ioutil.ReadAll(res.Body)

	// unmarshal the json into a string map
	err = json.Unmarshal(body, &responseData)
	if err != nil {
		fmt.Printf("snap: JSON error: %s", err)
		return "", err
	}

	fmt.Printf("%v\n", responseData)

	return nil, err
}

func (c Config) CreatePermissionTicket(request *PermissionRequest) (*PermissionResponse, error) {
	var client *http.Client
	switch c.Oauth2Config.(type) {
	case *clientcredentials.Config:
		client = c.Oauth2Config.(*clientcredentials.Config).Client(context.Background())
	default:
		return nil, errors.New("config(uma2): config MUST use client credentials flow")
	}

	reqBody, err := request.Marshal()
	if err != nil {
		panic(err)
	}

	//fmt.Println(string(reqBody))

	req, _ := http.NewRequest(http.MethodPost, c.WellKnownConfig.PermissionEndpoint, bytes.NewBuffer(reqBody))
	req.Header.Set("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()


	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != 201 {
		return nil, errors.New("uma2(permission): Unexpected response status\nexpected 201 have " + res.Status)
	}


	var response PermissionResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c Config) RequestRPT(request *RPTRequest, token *oauth2.Token) (*RequestPartyToken, error) {
	var client *http.Client
	switch c.Oauth2Config.(type) {
	case *oauth2.Config:
		//client = c.Oauth2Config.(*oauth2.Config).Client(context.Background(), token)
		client = oauth2.NewClient(context.Background(), oauth2.StaticTokenSource(token))
	default:
		return nil, errors.New("config(uma2): Unsupported client config")
	}

	data := fmt.Sprintf("grant_type=%s&ticket=%s", UMA2TicketGrantType, request.Ticket)
	payload := strings.NewReader(data)
	req, _ := http.NewRequest(http.MethodPost, c.WellKnownConfig.TokenEndpoint, payload)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// process the response
	defer res.Body.Close()
	var responseData RequestPartyToken
	body, _ := ioutil.ReadAll(res.Body)

	//
	fmt.Println("response rpt")
	fmt.Println(string(body))

	// unmarshal the json into a string map
	err = json.Unmarshal(body, &responseData)
	if err != nil {
		fmt.Printf("snap: JSON error: %s", err)
		return nil, err
	}

	return &responseData, err
}

func GetWellKnownConfig(serverUrl string) (*WellKnownConfig, error) {
	resp, err := http.Get(fmt.Sprintf("%s/.well-known/uma2-configuration", serverUrl))
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var c WellKnownConfig
	err = json.Unmarshal(body, &c)
	if err != nil {
		panic(err)
	}
	return &c, nil
}

func NewUma2CompleteConfig(c *Config) (*Config, error) {
	config, _ := GetWellKnownConfig(c.ServerUrl)

	// you can modify the client (for example ignoring bad certs or otherwise)
	// by modifying the context
	//ctx := context.Background()
	//client := config.Client(ctx)

	// return complete Config
	c.WellKnownConfig = config

	switch c.Oauth2Config.(type) {
	case *clientcredentials.Config:
		c.Oauth2Config.(*clientcredentials.Config).TokenURL = config.TokenEndpoint
	case *oauth2.Config:
		c.Oauth2Config.(*oauth2.Config).Endpoint.AuthURL = config.TokenEndpoint
		c.Oauth2Config.(*oauth2.Config).Endpoint.TokenURL = config.AuthorizationEndpoint
	default:
		panic("config(uma2): Unsupported client config")
	}

	return c, nil
}
