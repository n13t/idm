package uma2

//noinspection GoNameStartsWithPackageName
type UMA2TicketError struct {
	Error string
	Ticket Ticket
}
