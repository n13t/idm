// Implement pkg/idmservice/cryptor/provider.go

package idmservice

import (
	"context"
	"gitlab.com/n13t/idm/pkg/idmservice/cryptor"
)

type n13tIDMCryptoProvider struct {
	idmClient Service
}

func NewIDMCryptoProvider(idmClient *Service) cryptor.Provider {
	return &n13tIDMCryptoProvider{*idmClient}
}

func (p *n13tIDMCryptoProvider) Encrypt(ctx context.Context, Plaintext string) (ciphertext string, err error) {
	return p.idmClient.Encrypt(ctx, Plaintext)
}

func (p *n13tIDMCryptoProvider) Decrypt(ctx context.Context, Ciphertext string) (plaintext string, err error) {
	return p.idmClient.Decrypt(ctx, Ciphertext)
}
