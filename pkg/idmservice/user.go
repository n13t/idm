package idmservice

import "time"

type User struct {
	metadata	UserMetadata
	ID           	string	`json:"id,omitempty"`

	Username            string `json:"username,omitempty"`
	Email               string `json:"email,omitempty"`
	EmailVerified       bool   `json:"email_verified"`
	PhoneNumber         string `json:"phone_number,omitempty"`
	PhoneNumberVerified bool   `json:"phone_number_verified"`
	HashedPassword      string `json:"pwd,omitempty"`

	GivenName 	string 	`json:"given_name,omitempty"`
	FamilyName 	string 	`json:"family_name,omitempty"`
	MiddleName 	string 	`json:"middle_name,omitempty"`
	Nickname 	string 	`json:"nickname,omitempty"`
	Gender 		string 	`json:"gender,omitempty"`
	Birthday 	string 	`json:"birthday,omitempty"`

	ProfileURL	string 	`json:"profile_url,omitempty"`
	PictureURL	string 	`json:"picture_url,omitempty"`
	WebsiteURL	string 	`json:"website_url,omitempty"`

	ZoneInfo	string 	`json:"zone_info,omitempty"`
	Locale		string 	`json:"locale,omitempty"`

	// User's Address
	StreetAddress	string	`json:"street_address,omitempty"`
	Locality		string 	`json:"locality,omitempty"`
	Region			string 	`json:"region,omitempty"`
	PostalCode		string 	`json:"postal_code,omitempty"`
	Country			string 	`json:"country,omitempty"`

	CreatedAt	time.Time `json:"created_at,omitempty"`
	UpdatedAt	time.Time `json:"updated_at,omitempty"`
}

// Store as plain in DB
type UserMetadata struct {
	UMA2ResourceServerURL			string 	`json:"uma2_rs_svr_url"`
	UMA2ResourceOwner				string 	`json:"uma2_rs_owner"`
	UMA2ResourceId					string 	`json:"uma2_rs_id"`

	UUIDFingerprint					string 	`json:"hashed_uuid"`
	EmailFingerprint				string 	`json:"hashed_email"`
	UsernameFingerprint				string 	`json:"hashed_username"`

	// SHA256($SERVICE_KEY), this value tense to be use for forcing key rotation. This value lost will NOT cause data loss
	EncryptedServiceKeyFingerprint 	string	`json:"enc_svc_key_fgpt"`
	//Labels map[string]string		`json:"labels"`

	ProtocolEncryptDescriptions		[]EncryptedStepDescription `json:"protocol_encrypt_description"`
}

type EncryptedStepDescription struct {
	Method 			string 	`json:"m"`
	KeyFingerprint 	string 	`json:"fgpt"`
}
