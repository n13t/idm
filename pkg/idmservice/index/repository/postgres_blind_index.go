package repository

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.com/n13t/idm/pkg/idmservice/index"
	"strings"
)
type postgresBlindIndexRepository struct {
	DB *sql.DB
}

func (p postgresBlindIndexRepository) Create(ctx context.Context, index string) error {
	p.DB.Exec(fmt.Sprintf(`CREATE TABLE "%s"(id VARCHAR(50), doc JSONB)`, index))
	//panic("implement me")
	// CREATE TABLE "$index(id VARCHAR(50) PRIMARY KEY, doc JSONB NOT NULL)"
	return nil
}

func (p postgresBlindIndexRepository) AddDocument(ctx context.Context, index, documentId string, document interface{}) error {

	b, err := json.Marshal(document);
	if err != nil {
		panic(err)
	}

	_, _ = p.DB.Exec(fmt.Sprintf(`CREATE TABLE IF NOT EXISTS "%s" (id VARCHAR(64) PRIMARY KEY, doc JSONB NOT NULL);`, index))
	_, err = p.DB.Exec(fmt.Sprintf(`INSERT INTO "%s"(id, doc) VALUES($1, $2)`, index), documentId, string(b))
	if err != nil {
		return err
	}

	//panic("implement me")
	// "
	return nil
}

func (p postgresBlindIndexRepository) Search(ctx context.Context, index, search interface{}, limit, offset int32) (interface{}, error) {
	var rows *sql.Rows
	var err error

	switch v := search.(type) {
	case string: {
		s := strings.Split(v, "=")
		q := fmt.Sprintf(`SELECT id, doc FROM "%s" WHERE doc @> '{"%s": "%s"}' LIMIT %d OFFSET %d`, index, s[0], s[1], limit, offset)
		rows, err = p.DB.QueryContext(ctx, q)
		if err != nil {
			return nil, err
		}
	}
	}

	return rows, nil
}

func (p postgresBlindIndexRepository) Delete(ctx context.Context, index string) error {
	panic("implement me")
	// DROP TABLE $index;
}

func NewPostgresBlindIndexRepository(db *sql.DB) index.Repository {
	//if err := migrate(db); err != nil {
	//	panic("index(database): migration failed.\n\nSee database for more details.")
	//}

	return &postgresBlindIndexRepository{DB:db}
}

func migrate(db *sql.DB) error {
	//var id int
	var currentMigration sql.NullString 		// https://golang.org/pkg/database/sql/#NullString
	skip := true
	//var migratedAt time.Time

	// CREATE TABLE IF NOT EXISTS require Postgres 9.1+
	_, _ = db.Exec(`CREATE TABLE IF NOT EXISTS "IndexesMigrations" (
													id serial PRIMARY KEY,
													name VARCHAR (50) UNIQUE NOT NULL,
													migrated_at TIMESTAMP NOT NULL
					);`)

	migrations, err := db.Query(`SELECT name FROM "IndexesMigrations" ORDER BY id DESC;`)
	if err != nil {
		panic(err)
	}
	if migrations.Next() {
		err := migrations.Scan(&currentMigration)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%v \n",  currentMigration)
	}

	// Execute migrations in a single transaction
	{
		txn, err := db.Begin()
		if err != nil {
			panic(err)
		}

		for _, migrate := range []struct {
			name  string
			query string
		}{
			{
				name: "20200313124737_initial_migration",
				query: `
				CREATE TABLE "Indexes" (
					id serial PRIMARY KEY,
					index VARCHAR(50) NOT NULL,
					reference VARCHAR(50) NOT NULL,
					label VARCHAR(50) NOT NULL,
					value VARCHAR(50) NOT NULL
				);
			`,
			},
		} {
			if !currentMigration.Valid || !skip {
				_, err = txn.Exec(migrate.query)
				if err != nil {
					panic(err)
				}

				// Update migration log
				_, _ = txn.Exec(`INSERT INTO "IndexesMigrations"(name, migrated_at) VALUES($1, NOW());`, migrate.name)
			} else if currentMigration.String == migrate.name {
				skip = false
			}
		}

		err = txn.Commit()
		if err != nil {
			panic(err)
		}
	}

	return nil
}
