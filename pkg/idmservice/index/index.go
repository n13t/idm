package index

import "context"

type Repository interface {
	// Add Document to Index
	AddDocument(ctx context.Context, index, documentId string, document interface{}) error
	// Search by text in Index and return references
	Search(ctx context.Context, index, search interface{}, limit, offset int32) (documents interface{}, err error)
	// Delete by Index or Reference or Index+Reference
	Delete(ctx context.Context, index string) error
}