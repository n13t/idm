package test

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"gitlab.com/n13t/idm/pkg/idmservice/index/repository"
	"math/rand"
	"os"
	"testing"
)

const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

type Doc struct {
	Id 	string	`json:"id"`
	X	string	`json:"x"`
	Y 	string	`json:"y"`
	Z 	string	`json:"z"`
}
var docs []Doc

func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func GenerateDocs() {
	docID := RandStringBytes(32)
	docAttrX := RandStringBytes(8)
	docAttrY := RandStringBytes(16)
	docAttrZ := RandStringBytes(32)

	docs = append(docs, Doc{docID, docAttrX, docAttrY, docAttrZ})
}

func BenchmarkCreateDocuments(b *testing.B) {
	const IndexID = "IndexOfDemoData"
	databaseURL := os.Getenv("DATABASE_URL")
	if databaseURL == "" {
		databaseURL = "postgres://postgres:postgres@localhost:5432/postgres?sslmode=disable"
	}

	fmt.Printf("%v\n", databaseURL)
	db, err := sql.Open("postgres", databaseURL)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	index := repository.NewPostgresBlindIndexRepository(db)


	n := make([]byte, 10000)
	for _ = range n {
		GenerateDocs()
	}
	b.ResetTimer()

	for i := range n {
		err = index.AddDocument(nil, IndexID, fmt.Sprintf("%v", docs[i].Id), docs[i])
		if err != nil {
			fmt.Print(err)
		}
	}
}