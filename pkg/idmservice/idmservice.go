//go:generate mockgen -source=$GOFILE -destination=mocks/$GOFILE

/**
 * the business layer rules of IdM Service
 * also the Use Cases layer in The Clean Architecture (https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
 */

package idmservice

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-kit/kit/auth/jwt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
	"github.com/google/uuid"
	"github.com/opentracing/opentracing-go"
	"github.com/openzipkin/zipkin-go"
	"github.com/openzipkin/zipkin-go/model"
	"gitlab.com/n13t/idm/pkg/idmservice/cryptor"
	"gitlab.com/n13t/idm/pkg/idmservice/index"
	"gitlab.com/n13t/idm/pkg/idmservice/internal/token"
	"gitlab.com/n13t/idm/pkg/idmservice/user"
	"gitlab.com/n13t/idm/pkg/uma2"
	"golang.org/x/crypto/bcrypt"
	"sort"
	"strings"
	"time"
)

// Service describes a service that adds things together.
type Service interface {
	CreateUser(ctx context.Context, Username, Email, Password string) (string, error)
	UpdateUser(ctx context.Context, UID, Password,
		GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber,
		ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country string,
		EmailVerified, PhoneNumberVerified bool) (string, error)
	Search(ctx context.Context, Index, Query string, Limit, Offset int32) ([]User, error)
	ListUsers(ctx context.Context, PageSize int32, PageToken string) ([]User, string, error)
	DeleteUser(ctx context.Context, UID string) error
	IsValid(ctx context.Context, UID string, Password string) (bool, error)
	Encrypt(ctx context.Context, plaintext string) (ciphertext string, err error)
	Decrypt(ctx context.Context, ciphertext string) (plaintext string, err error)
}

func New(config *Config, logger log.Logger, ints, chars metrics.Counter) Service {
	var svc Service

	{
		svc = NewService(config)
		svc = AuthMiddleware(config)(svc)
		svc = ValidateMiddleware()(svc)
		svc = LoggingMiddleware(logger)(svc)
		svc = InstrumentingMiddleware(ints, chars)(svc)
	}

	return svc
}

func NewService(config *Config) Service {
	svc := &service{
		secretKeys:      nil,
		encryptProtocol: config.Protocol,
		cryptoContext: &CryptoContext{
			externalCryptoProvider: config.ExternalCryptoProvider,
		},
		transitEnabled: config.TransitEnabled,
		uma2Client: config.Uma2RsClient,
		// tracers
		tracer: config.Tracer,
		zipkinTracer: config.ZipkinTracer,
	}

	if svc.encryptProtocol == "" {
		fmt.Printf("config(crypto): No encryption protcol specified. Use default protocol\n")
		svc.encryptProtocol = DefaultEncryptProtocol
	}

	if strings.Contains(svc.encryptProtocol, Internal) || config.TransitEnabled {
		if len(config.SecretKeys) == 0 {
			panic("config(secret): Secret key(s) Not Found!")
		} else {
			secrets := make([]secret, len(config.SecretKeys))
			for _, sc := range config.SecretKeys {
				if len(sc) != 32 {
					panic("config(secret): secret string length must be 32")
				}

				secrets = append(secrets, secret{sc, sha256StringToHexString(sc)})
			}
			svc.secretKeys = secrets
		}
	}

	//if config.IndexRepository == nil {
	//	panic("config(index): Index Repository CANNOT be nil!")
	//}

	if config.IndexRepository != nil {
		svc.index = *config.IndexRepository
	}

	if config.UserRepository != nil {
		svc.user = *config.UserRepository
	}

	if svc.uma2Client != nil {
		if err := migrateDefaultResources(svc.uma2Client); err != nil {
			panic("config(uma2): Cannot migrate default resources to Authorization Server")
		}
	}

	return svc
}

type service struct {
	secretKeys      []secret
	encryptProtocol string
	index           index.Repository
	user            user.Repository
	userCached      user.Repository
	cryptoContext 	*CryptoContext
	cryptoProvider 	cryptor.Provider
	umaEnabled 		bool
	transitEnabled 	bool
	uma2UserRsId	string
	uma2Client		*uma2.Config
	// tracers
	tracer 			opentracing.Tracer
	zipkinTracer	*zipkin.Tracer
}

func (s service) DeleteUser(ctx context.Context, UID string) error {
	err := s.user.Delete(ctx, UID)
	if err != nil {
		return errors.New("core(DeleteUser): Failed")
	}

	return nil
}

func (s service) IsValid(ctx context.Context, UID string, Password string) (bool, error) {
	u, err := s.getUserByID(ctx, UID)
	if err != nil {
		return false, err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(u.HashedPassword), []byte(Password)); err != nil {
		return false, fmt.Errorf("ErrMismatchedHashAndPassword")
	}

	return true, nil
}

func (s service) Encrypt(ctx context.Context, Plaintext string) (string, error) {
	if !s.transitEnabled {
		return "", errors.New("core(config): transit disabled")
	}

	if s.zipkinTracer != nil {
		var sc model.SpanContext
		if parentSpan := zipkin.SpanFromContext(ctx); parentSpan != nil {
			sc = parentSpan.Context()
		}
		sp := s.zipkinTracer.StartSpan("Encrypt", zipkin.Parent(sc))
		defer sp.Finish()

		ctx = zipkin.NewContext(ctx, sp)
	}

	if Plaintext != "" {
		r, _, err := s.encryptByProtocol(ctx, strings.Join([]string{Internal}, ","), s.secretKeys, Plaintext)
		return r, err
	}

	return "", nil
}

func (s service) Decrypt(ctx context.Context, Ciphertext string) (string, error) {
	if !s.transitEnabled {
		return "", errors.New("core(config): transit disabled")
	}

	if s.zipkinTracer != nil {
		var sc model.SpanContext
		if parentSpan := zipkin.SpanFromContext(ctx); parentSpan != nil {
			sc = parentSpan.Context()
		}
		sp := s.zipkinTracer.StartSpan("Decrypt", zipkin.Parent(sc))
		defer sp.Finish()

		ctx = zipkin.NewContext(ctx, sp)
	}

	if Ciphertext != "" {
		r, err := s.decryptByProtocol(ctx,  s.secretKeys, []EncryptedStepDescription{{Method:Internal}}, Ciphertext)
		return r, err
	}

	return "", nil
}

func (s service) Search(ctx context.Context, Index, Query string, Limit, Offset int32) ([]User, error) {
	var (
		_ = ctx.Value(jwt.JWTTokenContextKey)
		accessGranted = false
	)

	if Limit == 0 || Limit > maxPageSize { Limit = defaultPageSize }

	if s.umaEnabled {
		jwtClaims := ctx.Value(jwt.JWTClaimsContextKey).(*CustomClaims)
		for _, p := range jwtClaims.Authorization.Permissions {
			if p.GetResourceId() == DefaultUserResource.Id {
				tokenScopes := p.GetResourceScopes()
				l := len(tokenScopes)
				m := 0
				for _, rsScope := range []string{uma2ReadOnlyScope, uma2ManageScope} {
					if sort.SearchStrings(tokenScopes, rsScope) < l {
						m++
					}
				}
				if m > 0 {
					// required scopes matched
					accessGranted = true
				}
			}
		}
		if accessGranted != true {
			permissions := uma2.PermissionRequest{}
			permissions.Add(uma2.Permission{
				ResourceId:     DefaultUserResource.Id,
				ResourceScopes: DefaultUserResource.ResourceScopes,
			})

			if r, err := s.uma2Client.CreatePermissionTicket(&permissions); err != nil {
				panic(err)
			} else {
				ticketRef := ctx.Value(uma2.UMA2TicketContextKey).(*uma2.Ticket)

				*ticketRef = uma2.Ticket{
					Realm:  "default",
					AsUri:  s.uma2Client.ServerUrl,
					Ticket: r.Ticket,
				}

				return nil, ErrPermissionDenied
			}
		}
	}


	if Index == "" {
		Index = userIndexTable
	}

	users := make([]User, 0)

	sp := strings.Split(Query, "=")
	switch field, value := sp[0], strings.Join(sp[1:], ""); field {
	case "id": {
		u, err := s.getUserByID(ctx, value)
		if err != nil {
			return nil, err
		}

		return []User{*u}, nil
	}
	case "username": {
		results, err := s.user.Search(ctx, "hashed_username", sha256StringToHexString(value), int(Limit), int(Offset))
		if err != nil {
			return nil, err
		}

		for _, r := range results {
			var u *User
			u, err := s.getUserByID(ctx, r["uid"])
			if err != nil {
				return nil, err
			}

			users = append(users, *u)
		}

		return users, nil
	}
	case "email": {
		results, err := s.user.Search(ctx, "hashed_email", sha256StringToHexString(value), int(Limit), int(Offset))
		if err != nil {
			return nil, err
		}

		for _, r := range results {
			var u *User
			u, err := s.getUserByID(ctx, r["uid"])
			if err != nil {
				return nil, err
			}

			users = append(users, *u)
		}
		return users, nil
	}

	}

	//r, err := s.index.Search(ctx, Index, Query, Limit, Offset)
	//if err != nil {
	//	panic(err)
	//}
	//rows := r.(*sql.Rows)
	////mds := make([]UserMetadata, 0)
	//ids := make([]string, 0)
	//for rows.Next() {
	//	var id string
	//	var doc string
	//	if err := rows.Scan(&id, &doc); err != nil {
	//		panic(err)
	//	}
	//	fmt.Println(id)
	//	ids = append(ids, id)
	//	//mds = append(mds, u)
	//}
	//
	//for _, id := range ids {
	//	u, _ := s.getUserByID(ctx, id)
	//	fmt.Println(u)
	//	users = append(users, *u)
	//}

	return users, nil
}

func (s service) ListUsers(ctx context.Context, PageSize int32, PageToken string) ([]User, string, error) {
	users := make([]User, 0)
	var offset int32
	var nextPageToken string

	if PageSize == 0 || PageSize > maxPageSize { PageSize = defaultPageSize }
	if PageToken != "" {
		decodedPageToken, err := token.DecodePageToken(PageToken)
		if err != nil {
			return nil, "", err
		}

		offset = decodedPageToken["pageSize"] * decodedPageToken["currentPage"]
	}
	results, err := s.user.List(ctx, PageSize, offset)
	if err != nil {
		return nil, "", err
	}

	for _, r := range results {
		var u *User
		u, err := s.getUserByID(ctx, r["uid"])
		if err != nil {
			return nil, "", err
		}

		users = append(users, *u)
	}

	if int32(len(results)) == PageSize {
		nextPageToken, err = token.CreateNextPageToken(PageToken, PageSize)
		if err != nil {
			return users, "", err
		}
	}

	return users, nextPageToken, nil
}

func (s *service) CreateUser(ctx context.Context, Username, Email, Password string) (string, error) {
	//if ctx.Value(jwt.JWTTokenContextKey) == nil {
	//	panic("Context shouldn't contain the encoded JWT")
	//}

	// token := ctx.Value(jwt.JWTTokenContextKey).(string)
	//if token != signedKey {
	//	t.Errorf("Context doesn't contain the expected encoded token value; expected: %s, got: %s", signedKey, token)
	//}

	// UMA2 Authorization
	//if sort.SearchStrings(SortedScopes, "CREATE_USER") == len(SortedScopes) {
	//	// create ticket
	//
	//	// return permission denied
	//	return "", nil
	//}

	/**
	 * Permission granted, process to create user
	 */
	var (
		userUUID uuid.UUID
		hashedPwdBuf []byte
		err error
	)

	if s.user == nil {
		panic("core(config): user repository is null")
	}

	if Username == "" || Email == ""  {
		return "", errors.New("invalid username/email")
	}
	isEmailExists, err := s.isEmailExists(ctx, Email)
	if err != nil {
		panic(err)
	}
	if isEmailExists {
		return "", errors.New("invalid username/email")
	}

	isUsernameExists, err := s.isEmailExists(ctx, Username)
	if err != nil {
		panic(err)
	}
	if isUsernameExists {
		return "", errors.New("invalid username/email")
	}


	encryptProtocol := s.encryptProtocol

	// Generate UUID
	if userUUID, err = uuid.NewRandom(); err != nil {
		return "", ErrUnableToGenerateUUID
	}
	// Hash password
	if hashedPwdBuf, err = bcrypt.GenerateFromPassword([]byte(Password), bcrypt.DefaultCost); err != nil {
		panic(err)
	}

	u := &User{
		ID: userUUID.String(),
		Username: Username,
		Email: Email,
		HashedPassword: string(hashedPwdBuf),
		CreatedAt: time.Now(),
	}

	md := UserMetadata{
		UUIDFingerprint: sha256StringToHexString(u.ID),
		UsernameFingerprint: sha256StringToHexString(u.Username),
		EmailFingerprint: sha256StringToHexString(u.Email),
	}

	// Encrypt data & save to repository
	{
		attrsBuf, _ := json.Marshal(u)

		var encryptedAttrs string
		encryptedAttrs, md.ProtocolEncryptDescriptions, err = s.encryptByProtocol(ctx, encryptProtocol, s.secretKeys, string(attrsBuf))
		if err != nil {
			panic(err)
		}
		if encryptedAttrs == ""{
			panic("core(create): unexpected value\n\nuser's data is empty")
		}

		metadataBuf, _ := json.Marshal(md)
		err = s.user.Create(ctx, u.ID, string(metadataBuf), encryptedAttrs)
		if err != nil {
			panic(err)
		}

		// Create Index
		//err = s.index.AddDocument(
		//	ctx,
		//	userIndexTable,
		//	md.UUIDFingerprint,
		//	md,
		//)
		//if err != nil {
		//	panic(err)
		//}

		go func() {
			// TODO emailService.sendEmail()
		}()
	}

	return "", nil
}

// long running task, but will wait
// assuming all not null attributes are those that need to update, avoid adding additional changes tracking mechanics
// -> performance tip: on client-side only send attributes that need to update to avoid unnecessary encrypting
// note: the updated_at field MUST be send to perform OCC (https://en.wikipedia.org/wiki/Optimistic_concurrency_control)
func (s *service) UpdateUser(ctx context.Context, UID, Password,
	GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber,
	ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country string,
	EmailVerified, PhoneNumberVerified bool) (string, error) {
	var err error

	if s.umaEnabled {
		var claims = ctx.Value(jwt.JWTClaimsContextKey).(*CustomClaims)

		hasManageScope := false

		for _, p := range claims.Authorization.Permissions {
			if p.GetResourceId() == DefaultUserResource.Id {
				tokenScopes := p.GetResourceScopes()

				if l := len(tokenScopes); sort.SearchStrings(tokenScopes, uma2ManageScope) < l {
					hasManageScope = true
				}
			}
		}

		if UID != claims.Subject && hasManageScope != true {
			permissions := uma2.PermissionRequest{}
			permissions.Add(uma2.Permission{
				ResourceId:     DefaultUserResource.Id,
				ResourceScopes: []string{uma2ManageScope},
			})

			if r, err := s.uma2Client.CreatePermissionTicket(&permissions); err != nil {
				panic(err)
			} else {
				ticketRef := ctx.Value(uma2.UMA2TicketContextKey).(*uma2.Ticket)

				*ticketRef = uma2.Ticket{
					Realm:  "default",
					AsUri:  s.uma2Client.ServerUrl,
					Ticket: r.Ticket,
				}

				return "", ErrPermissionDenied
			}
		}
	}

	u, err := s.getUserByID(ctx, UID)
	if err != nil {
		return "", err
	}

	// not found
	if u == nil {
		return "", ErrNotFound
	}

	if GivenName != "" { u.GivenName = GivenName }
	if FamilyName != "" { u.FamilyName = FamilyName }
	if MiddleName != "" { u.MiddleName = MiddleName }
	if Nickname != "" { u.Nickname = Nickname }

	if Gender 	!= "" { u.Gender = Gender }
	if Birthday != "" { u.Birthday = Birthday }

	if ZoneInfo != "" { u.ZoneInfo = ZoneInfo }
	if Locale 	!= "" { u.Locale = Locale }

	if StreetAddress 	!= "" { u.StreetAddress = StreetAddress }
	if Locality 		!= "" { u.Locality = Locality }
	if Region 			!= "" { u.Region = Region }
	if PostalCode 		!= "" { u.PostalCode = PostalCode }
	if Country 			!= "" { u.Country = Country }

	if ProfileURL != "" { u.ProfileURL = ProfileURL }
	if PictureURL != "" { u.PictureURL = PictureURL }
	if WebsiteURL != "" { u.WebsiteURL = WebsiteURL }

	if Password != "" {
		if hashedPwdBuf, err := bcrypt.GenerateFromPassword([]byte(Password), bcrypt.DefaultCost); err != nil {
			panic(err)
		} else {
			u.HashedPassword = string(hashedPwdBuf)
		}
	}

	if Email != "" {
		u.Email = Email
		u.EmailVerified = false
	}

	if PhoneNumber != "" {
		u.PhoneNumber = PhoneNumber
		u.PhoneNumberVerified = false
	}

	if PhoneNumberVerified == true {
		u.PhoneNumberVerified = true
	}

	if EmailVerified == true {
		u.EmailVerified = true
	}

	if Password != "" {
		if hashedPwdBuf, err := bcrypt.GenerateFromPassword([]byte(Password), bcrypt.DefaultCost); err != nil {
			panic(err)
		} else {
			u.HashedPassword = string(hashedPwdBuf)
		}
	}

	if Email != "" {
		u.Email = Email
		u.EmailVerified = false
	}

	if PhoneNumber != "" {
		u.PhoneNumber = PhoneNumber
		u.PhoneNumberVerified = false
	}

	if PhoneNumberVerified == true {
		u.PhoneNumberVerified = true
	}

	if EmailVerified == true {
		u.EmailVerified = true
	}

	u.UpdatedAt = time.Now()

	// Parse UserAttributes -> string
	var encryptedData string
	dataBuf, err := json.Marshal(u)
	if err != nil {
		panic(err)
	}

	encryptedData, u.metadata.ProtocolEncryptDescriptions, err = s.encryptByProtocol(ctx, s.encryptProtocol, s.secretKeys, string(dataBuf))
	mdJSONString, err := json.Marshal(u.metadata)
	if err != nil {
		panic(err)
	}

	err = s.user.Update(ctx, UID, string(mdJSONString), encryptedData)
	if err != nil {
		return "", err
	}

	return "", nil
}

func (s service) getUserByID(ctx context.Context, UID string) (*User, error) {
	var err error

	md := UserMetadata{}

	// Get user from UserRepository
	mdStr, encryptedStr, err := s.user.GetByID(ctx, UID)
	if err != nil {
		panic(err)
	}
	if mdStr == "" && encryptedStr == "" {
		return nil, ErrNotFound
	}

	if err := json.Unmarshal([]byte(mdStr), &md); err != nil {
		panic(err)
	}

	// Decrypt
	decryptedStr, err := s.decryptByProtocol(ctx, s.secretKeys, md.ProtocolEncryptDescriptions, encryptedStr)
	if err != nil {
		return nil, err
	}

	// Parse string -> User
	u := User{metadata: md, ID: UID}
	if err := json.Unmarshal([]byte(decryptedStr), &u); err != nil {
		panic(err)
	}

	return &u, nil
}


func (s service) isEmailExists(ctx context.Context, Email string) (bool, error) {
	results, err := s.user.Search(ctx, "hashed_email", sha256StringToHexString(Email), 1, 0)
	if err != nil {
		return false, err
	}

	if results == nil || len(results) == 0 {
		return false, nil
	}

	return true, nil
}

func (s service) isUsernameExists(ctx context.Context, Username string) (bool, error) {
	results, err := s.user.Search(ctx, "hashed_username", sha256StringToHexString(Username), 1, 0)
	if err != nil {
		return false, err
	}

	if len(results) == 0 {
		return false, nil
	}

	return true, nil
}
