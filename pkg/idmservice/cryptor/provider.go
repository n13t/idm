package cryptor

import "context"

type Provider interface {
	Encrypt(ctx context.Context, plaintext string) (ciphertext string, err error)
	Decrypt(ctx context.Context, ciphertext string) (plaintext string, err error)
}
