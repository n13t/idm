package impl

import (
	"context"
	"encoding/base64"
	"fmt"
	"github.com/hashicorp/vault/api"
	"gitlab.com/n13t/idm/pkg/idmservice/cryptor"
)

type hashiCorpVaultprovider struct {
	vaultClient *api.Client
	keyName string
}

func NewHashiCorpVaultCryptoProvider(vaultClient *api.Client) cryptor.Provider {
	return &hashiCorpVaultprovider{
		vaultClient: vaultClient,
		keyName: "idm",
	}
}

func (p *hashiCorpVaultprovider) Encrypt(ctx context.Context, plaintext string) (ciphertext string, err error) {
	data := map[string]interface{}{
		"plaintext": base64.StdEncoding.EncodeToString([]byte(plaintext)),
	}

	secret, err := p.vaultClient.Logical().Write(fmt.Sprintf("/transit/encrypt/%s", p.keyName), data)
	if err != nil {
		return "", err
	}

	return secret.Data["ciphertext"].(string), nil
}

func (p *hashiCorpVaultprovider) Decrypt(ctx context.Context, ciphertext string) (plaintext string, err error) {
	if string(ciphertext[0:5]) != "vault" {
		panic("crypto(vault): Unexpected ciphertext prefix. Cannot decrypt")
	}

	data := map[string]interface{}{
		"ciphertext": string(ciphertext),
	}

	secret, err := p.vaultClient.Logical().Write(fmt.Sprintf("/transit/decrypt/%s", p.keyName), data)
	if err != nil {
		return "", err
	}

	d, err := base64.StdEncoding.DecodeString(secret.Data["plaintext"].(string))

	return string(d), err
}



