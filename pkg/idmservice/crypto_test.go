package idmservice

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/hashicorp/go-hclog"
	"github.com/hashicorp/vault/api"
	"github.com/hashicorp/vault/audit"
	"github.com/hashicorp/vault/builtin/audit/file"
	"github.com/hashicorp/vault/builtin/logical/transit"
	"github.com/hashicorp/vault/http"
	"github.com/hashicorp/vault/sdk/helper/logging"
	"github.com/hashicorp/vault/sdk/logical"
	"github.com/hashicorp/vault/vault"
	"gitlab.com/n13t/idm/pkg/idmservice/cryptor"
	cryptorImpl "gitlab.com/n13t/idm/pkg/idmservice/cryptor/impl"
	"net"
	"strings"
	"testing"
)


func structToJsonString() string {
	user := &struct{
		Name string `json:"1" `
		Sbc string	`json:"2"`
	}{Name: "Frank123", Sbc: "asdvavasdvhasdvkjaw9048yt93hvgadfun0yhcsdfgfgasdfn=`9711630"}
	j, _ := json.Marshal(user)

	return string(j)
}

func TestChacha20Poly1305(t *testing.T)  {
	key := make([]byte, 32)

	for _, test := range []struct{
		text string
	} {
		{			structToJsonString() },
		{			"dfadsf" },
		{			"abcdefghijklmnopqrstuvwxyz0123456789" },
		{			"abcdefghijklmnopqrstuvwxyz" },
		{			"`~!@#$%^&*()-_=+" },
		{			`{"list":[{"id":1,"province_name":"Hà Nội","province_type":"Thành phố"},{"id":2,"province_name":"Hà Giang","province_type":"Tỉnh"},{"id":4,"province_name":"Cao Bằng","province_type":"Tỉnh"},{"id":6,"province_name":"Bắc Kạn","province_type":"Tỉnh"},{"id":8,"province_name":"Tuyên Quang","province_type":"Tỉnh"},{"id":10,"province_name":"Lào Cai","province_type":"Tỉnh"},{"id":11,"province_name":"Điện Biên","province_type":"Tỉnh"},{"id":12,"province_name":"Lai Châu","province_type":"Tỉnh"},{"id":14,"province_name":"Sơn La","province_type":"Tỉnh"},{"id":15,"province_name":"Yên Bái","province_type":"Tỉnh"}],"total":63}` },
	} {
		plaintext := []byte(test.text)


		encrypted, _ := chacha20Poly1305Encrypt(key, plaintext)
		decrypted, _ := chacha20Poly1305Decrypt(key, encrypted)

		if string(decrypted) != string(plaintext) {
			t.Errorf("want %s, have %s", plaintext, decrypted)
		}
	}
}

func TestEncryptByMethod(t *testing.T)  {
	k := "0123456789"
	k2 := string(make([]byte, 32))
	secrets := []secret{
		{value: k, fingerprint: sha256StringToHexString(k)},
		{value: k2, fingerprint: sha256StringToHexString(k2)},
	}

	for _, method := range []string{Plain, Internal} {
		for _, test := range []struct{
			text string
		} {
			{			structToJsonString() },
			{			"dfadsf" },
			{			"abcdefghijklmnopqrstuvwxyz0123456789" },
			{			"abcdefghijklmnopqrstuvwxyz" },
			{			"`~!@#$%^&*()-_=+" },
			{			`{"list":[{"id":1,"province_name":"Hà Nội","province_type":"Thành phố"},{"id":2,"province_name":"Hà Giang","province_type":"Tỉnh"},{"id":4,"province_name":"Cao Bằng","province_type":"Tỉnh"},{"id":6,"province_name":"Bắc Kạn","province_type":"Tỉnh"},{"id":8,"province_name":"Tuyên Quang","province_type":"Tỉnh"},{"id":10,"province_name":"Lào Cai","province_type":"Tỉnh"},{"id":11,"province_name":"Điện Biên","province_type":"Tỉnh"},{"id":12,"province_name":"Lai Châu","province_type":"Tỉnh"},{"id":14,"province_name":"Sơn La","province_type":"Tỉnh"},{"id":15,"province_name":"Yên Bái","province_type":"Tỉnh"}],"total":63}` },
		} {
			plaintext := []byte(test.text)


			encrypted, _, _ := encryptByMethod(context.Background(), method, secrets, plaintext)
			decrypted, _ := decryptByDefinedMethod(context.Background(), method, secrets, encrypted)

			if string(decrypted) != string(plaintext) {
				t.Errorf("want %s, have %s", plaintext, decrypted)
			}
		}
	}
}

func createVaultTest(t *testing.T) (net.Listener, *api.Client) {
	t.Helper()

	// Create an in-memory, unsealed core (the "backend", if you will).
	core, keyShares, rootToken := vault.TestCoreUnsealedWithConfig(t, &vault.CoreConfig{
		LogicalBackends: map[string]logical.Factory{
			"transit": transit.Factory,
		},
		AuditBackends: map[string]audit.Factory{
			"file": file.Factory,
		},
		Logger: logging.NewVaultLogger(log.Error),
	})
	//core, keyShares, rootToken := vault.TestCoreUnsealed(t)
	_ = keyShares

	// Start an HTTP server for the core.
	ln, addr := http.TestServer(t, core)

	// Create a client that talks to the server, initially authenticating with
	// the root token.
	conf := api.DefaultConfig()
	conf.Address = addr

	client, err := api.NewClient(conf)
	if err != nil {
		t.Fatal(err)
	}
	client.SetToken(rootToken)

	// Setup required secrets, policies, etc.
	_, err = client.Logical().Write("secret/foo", map[string]interface{}{
		"secret": "bar",
	})
	if err != nil {
		t.Fatal(err)
	}


	err = client.Sys().Mount("transit", &api.MountInput{
		Type: "transit",
	})
	if err != nil {
		t.Fatal(err)
	}

	_, err = client.Logical().Write("transit/keys/abc", map[string]interface{}{
		"type": "chacha20-poly1305",
	})
	if err != nil {
		t.Fatal(err)
	}

	return ln, client
}

func TestEncryptByProtocol(t *testing.T) {
	_, vaultClient := createVaultTest(t)
	hashiCorpVaultProvider := cryptorImpl.NewHashiCorpVaultCryptoProvider(vaultClient)

	// IdM as a Encryption Service
	idmClient := NewService(&Config{
		SecretKeys: []string{
			string(make([]byte, 32)),
		},
		TransitEnabled: true,
	})
	idmProvider := NewIDMCryptoProvider(&idmClient)

	//svc := service{
	//	encryptProtocol: strings.Join([]string{Plain, Internal, Vault}, ","),
	//	cryptoContext: &CryptoContext{
	//		vaultClient: vaultClient,
	//		vaultKeyName: "abc",
	//	},
	//}
	//k := "0123456789"
	//k2 := string(make([]byte, 32))
	//secrets := []secret{
	//	{value: k, fingerprint: sha256StringToHexString(k)},
	//	{value: k2, fingerprint: sha256StringToHexString(k2)},
	//}

	for _, p := range []struct{
		name string
		provider *cryptor.Provider
	} {
		{ "IdM (EaaS mode)",&idmProvider},
		{"HashiCorp's Vault", &hashiCorpVaultProvider},
	} {
		svc := NewService(&Config{
			IndexRepository: nil,
			UserRepository: nil,
			SecretKeys: []string{
				string(make([]byte, 32)),
			},
			Protocol: strings.Join([]string{Plain, Internal, Vault, Plain, Internal}, ","),
			ExternalCryptoProvider: p.provider,
		}).(*service)

		t.Run(fmt.Sprintf("Crypto provider: %s", p.name), func (t *testing.T) {
			for _, test := range []struct{
				text string
			} {
				{			structToJsonString() },
				{			"dfadsf" },
				{			"abcdefghijklmnopqrstuvwxyz0123456789" },
				{			"abcdefghijklmnopqrstuvwxyz" },
				{			"`~!@#$%^&*()-_=+" },
				{			`{"list":[{"id":1,"province_name":"Hà Nội","province_type":"Thành phố"},{"id":2,"province_name":"Hà Giang","province_type":"Tỉnh"},{"id":4,"province_name":"Cao Bằng","province_type":"Tỉnh"},{"id":6,"province_name":"Bắc Kạn","province_type":"Tỉnh"},{"id":8,"province_name":"Tuyên Quang","province_type":"Tỉnh"},{"id":10,"province_name":"Lào Cai","province_type":"Tỉnh"},{"id":11,"province_name":"Điện Biên","province_type":"Tỉnh"},{"id":12,"province_name":"Lai Châu","province_type":"Tỉnh"},{"id":14,"province_name":"Sơn La","province_type":"Tỉnh"},{"id":15,"province_name":"Yên Bái","province_type":"Tỉnh"}],"total":63}` },
			} {
				plaintext := test.text

				encrypted, description, _ := svc.encryptByProtocol(context.Background(), svc.encryptProtocol, svc.secretKeys, plaintext)
				decrypted, _ := svc.decryptByProtocol(context.Background(), svc.secretKeys, description, encrypted)

				if decrypted != plaintext {
					t.Errorf("want %s, have %s", plaintext, decrypted)
				}
			}
		})


	}

}
