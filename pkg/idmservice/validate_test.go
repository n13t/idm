package idmservice

import "testing"

func TestUsernameValidate(t *testing.T) {
	for _, test := range []struct{
		inp string
		out bool
	} {
		{"username", true},
		{"username\"", false},
		{"abc", false},
		{"@adf", false},
		{"a@adf", false},
		{"_username", true},
		{"_username_", true},
		{".username", false},
		{"user.name", true},
		{"user..name", false},
		{"64characters_string_4565623123_87644_123123123123123112314345464", true},
		{"65characters_string_4565623123_87644_1231231231231231123143456565", false},
	} {
		err := usernameValidate(test.inp)

		if (err == nil) != test.out {
			t.Errorf("expected \"%v\" validate %v, have %v\n", test.inp, test.out, err)
		}
	}
}

func TestEmailValidate(t *testing.T) {
	for _, test := range []struct{
		inp string
		out bool
	} {
		{"a@example.com", true},
		{"@example.com", false},
		{"a@example.com@", false},
		{"abc", false},
	} {
		err := emailValidate(test.inp)

		if (err == nil) != test.out {
			t.Errorf("expected \"%v\" validate %v, have %v\n", test.inp, test.out, err)
		}
	}
}

func TestPasswordValidate(t *testing.T) {
	for _, test := range []struct{
		inp string
		out bool
	} {
		{"a@example.com", true},
		{"@example.com", true},
		{"a@example.com@", true},
		{"abc", false},
	} {
		err := passwordValidate(test.inp)

		if (err == nil) != test.out {
			t.Errorf("expected \"%v\" validate %v, have %v\n", test.inp, test.out, err)
		}
	}
}
