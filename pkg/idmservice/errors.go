package idmservice

import "errors"

var (
	// ErrTwoZeroes is an arbitrary business rule for the Add method.
	ErrTwoZeroes = errors.New("can't sum two zeroes")

	// ErrIntOverflow protects the Add method. We've decided that this error
	// indicates a misbehaving service and should count against e.g. circuit
	// breakers. So, we return it directly in endpoints, to illustrate the
	// difference. In a real service, this probably wouldn't be the case.
	ErrIntOverflow = errors.New("integer overflow")

	// ErrMaxSizeExceeded protects the Concat method.
	ErrMaxSizeExceeded = errors.New("result exceeds maximum size")

	//
	ErrUnableToGenerateUUID = errors.New("core(user): unable to generate UUID")

	//
	ErrUpdateUser = errors.New("core(user): error on updating user\n\nDefault error on update user.")
	ErrNotFound = errors.New("core(user): user not found")
	ErrPermissionDenied = errors.New("core(user): permission denied")
)
