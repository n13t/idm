package idmservice

import (
	"context"
	cipher "crypto/cipher"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	cryptor2 "gitlab.com/n13t/idm/pkg/idmservice/cryptor"
	"golang.org/x/crypto/chacha20poly1305"
	"strings"
)

// Crypto methods
const (
	Plain = "0"
	Internal = "1"
	Vault = "2"				// Use current connected vault provider for encrypt & decrypt
	VaultEmbedded = "3"		// User current connected vault for encrypt, detect vault provider by embedded url for decrypt
)

const (
	CryptoContextKey = "crypto-context"
)

type CryptoContext struct {
	externalCryptoProvider *cryptor2.Provider
}

func (s *service) encryptByProtocol(ctx context.Context, protocol string, secrets []secret, plaintext string) (string, []EncryptedStepDescription, error) {
	var err error

	ctx = context.WithValue(ctx, CryptoContextKey, s.cryptoContext)

	c := []byte(plaintext)
	encryptedProtocol := make([]EncryptedStepDescription, 0)

	for _, method := range strings.Split(protocol, ",") {
		var keyFingerprint string

		if c, keyFingerprint, err = encryptByMethod(ctx, method, secrets, c); err != nil {
			return "", []EncryptedStepDescription{}, err
		}

		encryptedStepDescription := EncryptedStepDescription{
			Method: method,
			KeyFingerprint: keyFingerprint,
		}
		encryptedProtocol = append(encryptedProtocol, encryptedStepDescription)
	}

	// return hex format cipher text
	{
		//// Service Encryption
		//aead, err := chacha20poly1305.NewX([]byte("key"))
		//if err != nil {
		//	panic(err)
		//}
		//
		//nonce := make([]byte, chacha20poly1305.NonceSizeX)
		//if _, err := cryptorand.Read(nonce); err != nil {
		//	panic(err)
		//}
		//ciphertext := aead.Seal(nil, nonce, []byte(plaintext), nil)
		//
		//
		//// Vault Encryption
		//data := map[string]interface{}{
		//	"plaintext": ciphertext,
		//}
		//
		//keyName := "idm-service-key"
		//
		//secret, err := s.vault.Logical().Write(fmt.Sprintf("/transit/encrypt/%s", keyName), data)
		//if err != nil {
		//	return "", fmt.Errorf("issue encrypting with %s key", keyName)
		//}
		//
		//return secret.Data["ciphertext"].(string), nil
	}

	//return hex.EncodeToString(c), nil
	return string(c), encryptedProtocol, nil
}

func (s *service) decryptByProtocol(ctx context.Context, secrets []secret, protocolEncryptDescriptions []EncryptedStepDescription, cipher string) (string, error) {
	var err error

	ctx = context.WithValue(ctx, CryptoContextKey, s.cryptoContext)

	cipherBuf := []byte(cipher)


	for i := len(protocolEncryptDescriptions)-1; i >= 0;  {
		method := protocolEncryptDescriptions[i].Method

		switch method {
		case Plain: {
			if cipherBuf, err = decryptByDefinedMethod(ctx, method, nil, cipherBuf); err != nil {
				return "", err
			}
		}
		case Internal: {
			//var sc *secret
			//md := ctx.Value("metadata").(UserMetadata)
			//
			//for idx, value := range s.secretKeys {
			//	if value.fingerprint == md.EncryptedServiceKeyFingerprint {
			//		sc = &(s.secretKeys[idx])
			//	}
			//}

			//if sc == nil {
			//	panic(fmt.Sprintf("crypto(internal): CANNOT decrypt data.\n\nSecret key with fingerprint %s does not exists.", md.EncryptedServiceKeyFingerprint))
			//}

			if cipherBuf, err = decryptByDefinedMethod(ctx, method, secrets, cipherBuf); err != nil {
				return "", err
			}
		}
		case Vault: {
			if cipherBuf, err = decryptByDefinedMethod(ctx, method, secrets, cipherBuf); err != nil {
				return "", err
			}
		}
		default:
			panic("crypto(decryptByProtocol): Unsupported decryption method")
		}
		i--
	}

	return string(cipherBuf), nil
}

func encryptByMethod(ctx context.Context, method string, secretKeys []secret,  text []byte) ([]byte, string, error) {
	switch method {
	case Plain: {
		return []byte(fmt.Sprintf("%s.%s", Plain, text)), "", nil
	}
	case Internal: { //
		sc := secretKeys[len(secretKeys) - 1]

		c, err := chacha20Poly1305Encrypt([]byte(sc.value), text)
		if err != nil {
			return nil, "", err
		}

		var RawURLEncoding = base64.URLEncoding.WithPadding(base64.NoPadding)

		header := sc.fingerprint
		_ = RawURLEncoding.EncodeToString(c)

		return []byte(fmt.Sprintf("%s.%s.%s", method, header, string(c))), sc.fingerprint, nil
	}
	case Vault: {
		c, err := externalEncrypt(ctx, text)
		return c, "", err
	}
	default:
		return nil, "", nil
	}
}

func decryptByDefinedMethod(ctx context.Context, method string, secretKeys []secret, ciphertext []byte) ([]byte, error) {
	switch method {
	case Plain: {
		s := strings.Split(string(ciphertext), ".")
		method := s[0]
		payload := strings.Join(s[1:], ".")

		if method != Plain {
			panic("crypto(plain): Unexpected header's method")
		}

		return []byte(payload), nil
	}
	case Internal: {
		sp := strings.Split(string(ciphertext), ".")

		if len(sp) != 3 {
			return nil, errors.New("core(crypto): Unexpected ciphertext\n\nInvalid playload\n")
		}

		method := sp[0]
		encryptedKeyFingerprint := []byte(sp[1])
		payloadStr := strings.Join(sp[2:], ".")

		// Additional check
		if method != Internal {
			panic("crypto(internal): Method not match\n\nExpected method id in metadata not match with actual method in ciphertext")
		}

		secrets := secretKeys

		var secret *secret
		for _, sc := range secrets {
			if sc.fingerprint == string(encryptedKeyFingerprint) {
				secret = &sc
				break
			}
		}

		//var RawURLEncoding = base64.URLEncoding.WithPadding(base64.NoPadding)
		//payload, err := RawURLEncoding.DecodeString(payloadStr)
		//if err != nil {
		//	panic(err)
		//}

		if secret == nil {
			return nil, errors.New("core(crypto): decrypt key not found")
		}

		return chacha20Poly1305Decrypt([]byte((*secret).value), []byte(payloadStr))
	}
	case Vault: {
		return externalDecrypt(ctx, ciphertext)
	}
	default:
		return nil, nil
	}
}

func chacha20Poly1305Encrypt(key, text []byte) ([]byte, error)  {
	var aead cipher.AEAD
	var err error

	if aead, err = chacha20poly1305.NewX(key); err != nil {
		return nil, err
	}

	nonce := make([]byte, chacha20poly1305.NonceSizeX)
	//if _, err := cryptorand.Read(nonce); err != nil {
	//	panic(err)
	//}
	ciphertext := aead.Seal(nil, nonce, text, nil)

	return []byte(hex.EncodeToString(ciphertext)), nil
}

func chacha20Poly1305Decrypt(key, ciphertext []byte) ([]byte, error) {
	var aead cipher.AEAD

	c := make([]byte, hex.DecodedLen(len(ciphertext)))
	n, err := hex.Decode(c, ciphertext)
	if err != nil {
		panic(err)
	}

	if aead, err = chacha20poly1305.NewX(key); err != nil {
		return nil, err
	}

	nonce := make([]byte, chacha20poly1305.NonceSizeX)
	//if _, err := cryptorand.Read(nonce); err != nil {
	//	panic(err)
	//}

	plaintext, err := aead.Open(nil, nonce, c[:n], nil)
	if err != nil {
		//log.Fatalln("Failed to decrypt or authenticate message:", err)
		panic(err)
	}
	return plaintext, nil
}

func externalEncrypt(ctx context.Context, text []byte) ([]byte, error) {
	cp := ctx.Value(CryptoContextKey).(*CryptoContext)
	externalCryptoProvider := *cp.externalCryptoProvider

	r, err := externalCryptoProvider.Encrypt(ctx, string(text))

	return []byte(r), err
}

func externalDecrypt(ctx context.Context, text []byte) ([]byte, error) {
	cp := ctx.Value(CryptoContextKey).(*CryptoContext)
	externalCryptoProvider := *cp.externalCryptoProvider

	r, err := externalCryptoProvider.Decrypt(ctx, string(text))

	return []byte(r), err
}

//func vaultEncrypt(ctx context.Context, text []byte) ([]byte, error) {
//	cp := ctx.Value(CryptoContextKey).(*CryptoContext)
//	vaultClient := cp.vaultClient
//	vaultKeyName := cp.vaultKeyName
//
//	data := map[string]interface{}{
//		"plaintext": base64.StdEncoding.EncodeToString(text),
//	}
//
//	secret, err := vaultClient.Logical().Write(fmt.Sprintf("/transit/encrypt/%s", vaultKeyName), data)
//	if err != nil {
//		return nil, err
//	}
//
//	return []byte(secret.Data["ciphertext"].(string)), nil
//}
//
//func vaultDecrypt(ctx context.Context, ciphertext []byte) ([]byte, error) {
//	cp := ctx.Value(CryptoContextKey).(*CryptoContext)
//	vaultClient := cp.vaultClient
//	vaultKeyName := cp.vaultKeyName
//
//	if string(ciphertext[0:5]) != "vault" {
//		panic("crypto(vault): Unexpected ciphertext prefix. Cannot decrypt")
//	}
//
//	data := map[string]interface{}{
//		"ciphertext": string(ciphertext),
//	}
//
//	secret, err := vaultClient.Logical().Write(fmt.Sprintf("/transit/decrypt/%s", vaultKeyName), data)
//	if err != nil {
//		return nil, err
//	}
//
//	return base64.StdEncoding.DecodeString(secret.Data["plaintext"].(string))
//}

func sha256StringToHexString(s string) string  {
	h := sha256.New()
	h.Write([]byte(s))
	return hex.EncodeToString(h.Sum(nil))
}