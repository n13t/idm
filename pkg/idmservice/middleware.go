package idmservice

import (
	"context"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
)

// Middleware describes a service (as opposed to endpoint) middleware.
type Middleware func(Service) Service


func AuthMiddleware(config interface{}) Middleware {
	return func(next Service) Service {
		return authMiddleware{config, next}
	}
}

type authMiddleware struct {
	config interface{}
	next   Service
}

func (mw authMiddleware) CreateUser(ctx context.Context, Username, Email, Password string) (string, error) {
	return mw.next.CreateUser(ctx, Username, Email, Password)
}

func (mw authMiddleware) UpdateUser(ctx context.Context, UID, Password, GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber, ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country string, EmailVerified, PhoneNumberVerified bool) (string, error) {
	return mw.next.UpdateUser(ctx, UID, Password,
		GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber,
		ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country,
		EmailVerified, PhoneNumberVerified)
}

func (mw authMiddleware) Search(ctx context.Context, Index, Query string, Limit, Offset int32) ([]User, error) {
	return mw.next.Search(ctx, Index, Query, Limit, Offset)
}

func (mw authMiddleware) ListUsers(ctx context.Context, PageSize int32, PageToken string) ([]User, string, error) {
	fmt.Println("AUTH MW")
	// load casbin config
	//var (
	//	_ = ctx.Value(jwt.JWTTokenContextKey)
	//	jwtClaims = ctx.Value(jwt.JWTClaimsContextKey).(*CustomClaims)
	//)
	//
	//casbinModel := ""
	//casbinPolicy := ""
	//enforcer, err := stdcasbin.NewEnforcer(casbinModel, casbinPolicy)
	//if err != nil {
	//	return nil, "", err
	//}
	//
	//
	//ctx = context.WithValue(ctx, "", enforcer)
	//ok, err := enforcer.Enforce(jwtClaims.Subject, jwtClaims.Authorization.Permissions, "ListUsers")
	//if err != nil {
	//	return nil, "", err
	//}
	//if !ok {
	//	return nil, "", errors.New("core(auth): permission denied")
	//}

	return mw.next.ListUsers(ctx, PageSize, PageToken)
}

func (mw authMiddleware) DeleteUser(ctx context.Context, UID string) error {
	return mw.next.DeleteUser(ctx, UID)
}

func (mw authMiddleware) IsValid(ctx context.Context, UID string, Password string) (bool, error) {
	return mw.next.IsValid(ctx, UID, Password)
}

func (mw authMiddleware) Encrypt(ctx context.Context, plaintext string) (ciphertext string, err error) {
	return mw.next.Encrypt(ctx, plaintext)
}

func (mw authMiddleware) Decrypt(ctx context.Context, ciphertext string) (plaintext string, err error) {
	return mw.next.Decrypt(ctx, ciphertext)
}

func ValidateMiddleware() Middleware {
	return func(next Service) Service {
		return validateMiddleware{next}
	}
}
type validateMiddleware struct {
	next   Service
}

func (mw validateMiddleware) CreateUser(ctx context.Context, Username, Email, Password string) (string, error) {
	if err := usernameValidate(Username); err != nil { return "", err }
	if err := emailValidate(Email); err != nil { return "", err }
	if err := passwordValidate(Password); err != nil { return "", err }

	return mw.next.CreateUser(ctx, Username, Email, Password)
}

func (mw validateMiddleware) UpdateUser(ctx context.Context, UID, Password, GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber, ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country string, EmailVerified, PhoneNumberVerified bool) (string, error) {
	return mw.next.UpdateUser(ctx, UID, Password, GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber, ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country, EmailVerified, PhoneNumberVerified)
}

func (mw validateMiddleware) Search(ctx context.Context, Index, Query string, Limit, Offset int32) ([]User, error) {
	return mw.next.Search(ctx, Index, Query, Limit, Offset)
}

func (mw validateMiddleware) ListUsers(ctx context.Context, PageSize int32, PageToken string) ([]User, string, error) {
	return mw.next.ListUsers(ctx, PageSize, PageToken)
}

func (mw validateMiddleware) DeleteUser(ctx context.Context, UID string) error {
	return mw.next.DeleteUser(ctx, UID)
}

func (mw validateMiddleware) IsValid(ctx context.Context, UID string, Password string) (bool, error) {
	return mw.next.IsValid(ctx, UID, Password)
}

func (mw validateMiddleware) Encrypt(ctx context.Context, plaintext string) (ciphertext string, err error) {
	return mw.next.Encrypt(ctx, plaintext)
}

func (mw validateMiddleware) Decrypt(ctx context.Context, ciphertext string) (plaintext string, err error) {
	return mw.next.Decrypt(ctx, ciphertext)
}

// LoggingMiddleware takes a logger as a dependency
// and returns a service Middleware.
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next Service) Service {
		return loggingMiddleware{logger, next}
	}
}

type loggingMiddleware struct {
	logger log.Logger
	next   Service
}

func (mw loggingMiddleware) Search(ctx context.Context, index, search string, limit, offset int32) (users []User, err error) {
	defer func() {
		mw.logger.Log("method", "Search", "index", index, "search", search, "limit", limit, "offset", offset, "users", users, "err", err)
	}()
	return mw.next.Search(ctx, index, search, limit, offset)
}

func (mw loggingMiddleware) ListUsers(ctx context.Context, PageSize int32, PageToken string) (users []User, pageToken string, err error) {
	defer func() {
		mw.logger.Log("method", "ListUsers", "PageSize", PageSize, "PageToken", PageToken, "err", err)
	}()
	return mw.next.ListUsers(ctx, PageSize, PageToken)
}

//func (mw loggingMiddleware) Sum(ctx context.Context, a, b int) (v int, err error) {
//	defer func() {
//		mw.logger.Log("method", "Sum", "a", a, "b", b, "v", v, "err", err)
//	}()
//	return mw.next.Sum(ctx, a, b)
//}
//
//func (mw loggingMiddleware) Concat(ctx context.Context, a, b string) (v string, err error) {
//	defer func() {
//		mw.logger.Log("method", "Concat", "a", a, "b", b, "v", v, "err", err)
//	}()
//	return mw.next.Concat(ctx, a, b)
//}

func (mw loggingMiddleware) CreateUser(ctx context.Context, Username, Email, Password string) (v string, err error) {
	defer func() {
		mw.logger.Log("method", "CreateUser", "v", v, "err", err)
	}()
	return mw.next.CreateUser(ctx, Username, Email, Password)
}

func (mw loggingMiddleware) UpdateUser(ctx context.Context, UserUUID, Password,
	GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber,
	ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country string,
	EmailVerified, PhoneNumberVerified bool) (v string, err error) {
	defer func() {
		mw.logger.Log("method", "UpdateUser", "v", v, "err", err)
	}()
	return mw.next.UpdateUser(ctx, UserUUID, Password,
		GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber,
		ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country,
		EmailVerified, PhoneNumberVerified)
}

func (mw loggingMiddleware) DeleteUser(ctx context.Context, UID string) (err error) {
	defer func() {
		mw.logger.Log("method", "DeleteUser", "uid", UID, "err", err)
	}()
	return mw.next.DeleteUser(ctx, UID)
}

func (mw loggingMiddleware) IsValid(ctx context.Context, UID, Password string) (v bool, err error) {
	defer func() {
		mw.logger.Log("method", "IsValid", "uid", UID, "v", v, "err", err)
	}()
	return mw.next.IsValid(ctx, UID, Password)
}

func (mw loggingMiddleware) Encrypt(ctx context.Context, Plaintext string) (v string, err error) {
	defer func() {
		mw.logger.Log("method", "Encrypt", "err", err)
	}()
	return mw.next.Encrypt(ctx, Plaintext)
}

func (mw loggingMiddleware) Decrypt(ctx context.Context, Ciphertext string) (v string, err error) {
	defer func() {
		mw.logger.Log("method", "Decrypt", "err", err)
	}()
	return mw.next.Decrypt(ctx, Ciphertext)
}

// InstrumentingMiddleware returns a service middleware that instruments
// the number of integers summed and characters concatenated over the lifetime of
// the service.
func InstrumentingMiddleware(ints, chars metrics.Counter) Middleware {
	return func(next Service) Service {
		return instrumentingMiddleware{
			ints:  ints,
			chars: chars,
			next:  next,
		}
	}
}

type instrumentingMiddleware struct {
	ints  metrics.Counter
	chars metrics.Counter
	next  Service
}

func (mw instrumentingMiddleware) Search(ctx context.Context, index, search string, limit, offset int32) ([]User, error) {
	users, err := mw.next.Search(ctx, index, search, limit, offset)
	mw.ints.Add(float64(len(users)))
	return users, err
}

func (mw instrumentingMiddleware) ListUsers(ctx context.Context, PageSize int32, PageToken string) ([]User, string, error) {
	users, pageToken, err := mw.next.ListUsers(ctx, PageSize, PageToken)
	mw.ints.Add(float64(len(users)))
	return users, pageToken, err
}

//func (mw instrumentingMiddleware) Sum(ctx context.Context, a, b int) (int, error) {
//	v, err := mw.next.Sum(ctx, a, b)
//	mw.ints.Add(float64(v))
//	return v, err
//}
//
//func (mw instrumentingMiddleware) Concat(ctx context.Context, a, b string) (string, error) {
//	v, err := mw.next.Concat(ctx, a, b)
//	mw.chars.Add(float64(len(v)))
//	return v, err
//}

func (mw instrumentingMiddleware) CreateUser(ctx context.Context, Name, Email, Password string) (string, error) {
	v, err := mw.next.CreateUser(ctx, Name, Email, Password)
	mw.chars.Add(float64(len(v)))
	return v, err
}

func (mw instrumentingMiddleware) UpdateUser(ctx context.Context, UID, Password,
	GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber,
	ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country string,
	EmailVerified, PhoneNumberVerified bool) (string, error) {
	v, err := mw.next.UpdateUser(ctx, UID, Password,
		GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber,
		ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country,
		EmailVerified, PhoneNumberVerified)
	mw.chars.Add(float64(len(v)))
	return v, err
}

func (mw instrumentingMiddleware) DeleteUser(ctx context.Context, UID string) error {
	return mw.next.DeleteUser(ctx, UID)
}

func (mw instrumentingMiddleware) IsValid(ctx context.Context, UID, Password string) (bool, error)  {
	v, err := mw.next.IsValid(ctx, UID, Password)
	//mw.chars.Add(float64(len(v)))
	return v, err
}

func (mw instrumentingMiddleware) Encrypt(ctx context.Context, v string) (string, error)  {
	v, err := mw.next.Encrypt(ctx, v)
	mw.chars.Add(float64(len(v)))
	return v, err
}

func (mw instrumentingMiddleware) Decrypt(ctx context.Context, v string) (string, error)  {
	v, err := mw.next.Decrypt(ctx, v)
	mw.chars.Add(float64(len(v)))
	return v, err
}
