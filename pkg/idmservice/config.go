package idmservice

import (
	stdopentracing "github.com/opentracing/opentracing-go"
	stdzipkin "github.com/openzipkin/zipkin-go"
	cryptor2 "gitlab.com/n13t/idm/pkg/idmservice/cryptor"
	"gitlab.com/n13t/idm/pkg/idmservice/index"
	"gitlab.com/n13t/idm/pkg/idmservice/user"
	"gitlab.com/n13t/idm/pkg/uma2"
	"strings"
)

type Config struct {
	SecretKeys				[]string
	Protocol 				string
	IndexRepository			*index.Repository
	UserRepository			*user.Repository
	UserCachedRepository 	*user.Repository
	UMAEnabled				bool
	Uma2RsClient			*uma2.Config
	ExternalCryptoProvider  *cryptor2.Provider
	TransitEnabled			bool
	// tracers
	Tracer stdopentracing.Tracer
	ZipkinTracer *stdzipkin.Tracer
}

const (
	userIndexTable = "IndexOfUsers"
)
var (
	DefaultEncryptProtocol = strings.Join([]string{Internal, Vault}, ",")
)

type secret struct {
	value string
	fingerprint string
}
