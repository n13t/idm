package token

import (
	"encoding/base64"
	"encoding/hex"
	"github.com/golang/protobuf/proto"
)


const (
	PageSize = "pageSize"
	CurrentPage = "currentPage"
	DefaultPageSize = 10
)

func CreateNextPageToken(PageTokenString string, pageSize int32) (string, error) {
	var nextPageToken PageToken

	if PageTokenString != "" {
		result, err := DecodePageToken(PageTokenString)
		if err != nil {
			return "", err
		}

		nextPageToken = PageToken{
			PageSize: result[PageSize],
			CurrentPage: result[CurrentPage] + 1,
		}

	} else {
		nextPageToken = PageToken{
			PageSize: DefaultPageSize,
			CurrentPage: 1,
		}

		if pageSize != 0 && pageSize < 1001 {
			nextPageToken.PageSize = pageSize
		}
	}

	b, err := proto.Marshal(&nextPageToken)
	if err != nil {
		panic(err)
	}

	var RawURLEncoding = base64.URLEncoding.WithPadding(base64.NoPadding)
	payload := RawURLEncoding.EncodeToString([]byte(hex.EncodeToString(b)))

	return payload, nil
}

func DecodePageToken(PageTokenString string) (map[string]int32, error) {
	var token PageToken
	var RawURLEncoding = base64.URLEncoding.WithPadding(base64.NoPadding)
	payload, _ := RawURLEncoding.DecodeString(PageTokenString)

	p2, _ := hex.DecodeString(string(payload))

	err := proto.Unmarshal(p2, &token)
	if err != nil {
		return nil, err
	}

	return map[string]int32{
		PageSize: token.PageSize,
		CurrentPage: token.CurrentPage,
	}, nil
}
