// Code generated by protoc-gen-go. DO NOT EDIT.
// source: token.proto

package token

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type PageToken struct {
	PageSize             int32    `protobuf:"varint,1,opt,name=page_size,json=pageSize,proto3" json:"page_size,omitempty"`
	CurrentPage          int32    `protobuf:"varint,2,opt,name=current_page,json=currentPage,proto3" json:"current_page,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PageToken) Reset()         { *m = PageToken{} }
func (m *PageToken) String() string { return proto.CompactTextString(m) }
func (*PageToken) ProtoMessage()    {}
func (*PageToken) Descriptor() ([]byte, []int) {
	return fileDescriptor_3aff0bcd502840ab, []int{0}
}

func (m *PageToken) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PageToken.Unmarshal(m, b)
}
func (m *PageToken) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PageToken.Marshal(b, m, deterministic)
}
func (m *PageToken) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PageToken.Merge(m, src)
}
func (m *PageToken) XXX_Size() int {
	return xxx_messageInfo_PageToken.Size(m)
}
func (m *PageToken) XXX_DiscardUnknown() {
	xxx_messageInfo_PageToken.DiscardUnknown(m)
}

var xxx_messageInfo_PageToken proto.InternalMessageInfo

func (m *PageToken) GetPageSize() int32 {
	if m != nil {
		return m.PageSize
	}
	return 0
}

func (m *PageToken) GetCurrentPage() int32 {
	if m != nil {
		return m.CurrentPage
	}
	return 0
}

func init() {
	proto.RegisterType((*PageToken)(nil), "PageToken")
}

func init() {
	proto.RegisterFile("token.proto", fileDescriptor_3aff0bcd502840ab)
}

var fileDescriptor_3aff0bcd502840ab = []byte{
	// 109 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x2e, 0xc9, 0xcf, 0x4e,
	0xcd, 0xd3, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x57, 0xf2, 0xe6, 0xe2, 0x0c, 0x48, 0x4c, 0x4f, 0x0d,
	0x01, 0x09, 0x09, 0x49, 0x73, 0x71, 0x16, 0x24, 0xa6, 0xa7, 0xc6, 0x17, 0x67, 0x56, 0xa5, 0x4a,
	0x30, 0x2a, 0x30, 0x6a, 0xb0, 0x06, 0x71, 0x80, 0x04, 0x82, 0x33, 0xab, 0x52, 0x85, 0x14, 0xb9,
	0x78, 0x92, 0x4b, 0x8b, 0x8a, 0x52, 0xf3, 0x4a, 0xe2, 0x41, 0x62, 0x12, 0x4c, 0x60, 0x79, 0x6e,
	0xa8, 0x18, 0xc8, 0x10, 0x27, 0xf6, 0x28, 0x56, 0xb0, 0xd9, 0x49, 0x6c, 0x60, 0xc3, 0x8d, 0x01,
	0x01, 0x00, 0x00, 0xff, 0xff, 0x96, 0x77, 0x25, 0x67, 0x6b, 0x00, 0x00, 0x00,
}
