package token

import "testing"

func TestCreateNextPageToken(t *testing.T) {
	const pageSize = 100

	npt, err := CreateNextPageToken("", pageSize)
	t.Logf("%s", npt)
	t.Logf("%v", err)

	decoded, _ := DecodePageToken(npt)
	if decoded[PageSize] != pageSize {
		t.Errorf("expected page size %d have %d\n", pageSize, decoded[PageSize])
	}

	i := 0
	for i < 1e3{
		npt, err = CreateNextPageToken(npt, pageSize)
		decoded, _ = DecodePageToken(npt)
		t.Logf("%s %d %d", npt, decoded[PageSize], decoded[CurrentPage])
		i++
	}
}