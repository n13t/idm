package idmservice

import (
	"context"
	"database/sql/driver"
	"github.com/stretchr/testify/mock"
	"gitlab.com/n13t/idm/pkg/idmservice/user"
	"gitlab.com/n13t/idm/pkg/idmservice/user/mocks"
	"golang.org/x/crypto/bcrypt"
	"regexp"
	"strings"
	"testing"
)

//// help to avoid some mock args
type Any struct{}
func (a Any) Match(v driver.Value) bool { return true }

// see Plain encrypt method
type PlainData struct{}

// Match satisfies sqlmock.Argument interface
func (a PlainData) Match(v driver.Value) bool {
	re := regexp.MustCompile(`0.?`)
	return re.Match([]byte(v.(string)))
}

// a successful case
func TestShouldCreateUser(t *testing.T) {
	//db, _, err := sqlmock.New()
	mockUserRepo := &mocks.Repository{}

	//mockResultCreateUserFn := func(ctx context.Context, UID, Metadata, Data string) error {
	//	return nil
	//}

	// NB: .Return(...) must return the same signature as the method being mocked.
	//     In this case it's (*s3.ListObjectsOutput, error).

	mockUserRepo.On(
		"Search",
		mock.Anything,
		mock.Anything,
		mock.Anything,
		mock.Anything,
		mock.Anything,
	).Return([]map[string]string{}, nil)
	mockUserRepo.On(
		"Create",
		mock.Anything,
		mock.Anything,
		mock.Anything,
		mock.Anything,
		).Return(nil)


	var userRepo user.Repository
	userRepo = mockUserRepo

	svc := NewService(&Config{
		SecretKeys:             nil,
		Protocol:               strings.Join([]string{Plain}, ","),
		IndexRepository:        nil,
		UserRepository:         &userRepo,
		UserCachedRepository:   nil,
		ExternalCryptoProvider: nil,
		TransitEnabled:         false,
	})
	//if err != nil {
	//	t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	//}
	//defer db.Close()



	// now we execute our method
	type TestUser struct {
		User
		Password string
	}
	for _, u := range []TestUser{
		{User{Username: "myUsername", Email: "myEmail"}, "myPassword"},
	} {
		if hashedPwdBuf, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost); err != nil {
			panic(err)
		} else {
			u.HashedPassword = string(hashedPwdBuf)
		}

		//dbMock.ExpectBegin()
		////mock.ExpectExec("UPDATE products").WillReturnResult(sqlmock.NewResult(1, 1))
		//dbMock.ExpectExec(`INSERT INTO "users"`).WithArgs(Any{},Any{}, PlainData{}).WillReturnResult(sqlmock.NewResult(1, 1))
		//dbMock.ExpectCommit()

		if _, err := svc.CreateUser(context.Background(), u.Username, u.Email, u.Password); err != nil {
			//t.Errorf("error was not expected while create user: %s", err)
		}
	}

	// we make sure that all expectations were met
	//if err := mock.ExpectationsWereMet(); err != nil {
	//	t.Errorf("there were unfulfilled expectations: %s", err)
	//}
}

func TestShouldUpdateUser(t *testing.T) {
	//db, _, err := sqlmock.New()
	mockUserRepo := &mocks.Repository{}

	//mockResultCreateUserFn := func(ctx context.Context, UID, Metadata, Data string) error {
	//	return nil
	//}

	// NB: .Return(...) must return the same signature as the method being mocked.
	//     In this case it's (*s3.ListObjectsOutput, error).

	mockUserRepo.On(
		"GetByID",
		mock.Anything,
		mock.Anything,
	).Return("", "", nil)
	mockUserRepo.On(
		"Update",
		mock.Anything,
		mock.Anything,
		mock.Anything,
		mock.Anything,
	).Return(nil)


	var userRepo user.Repository
	userRepo = mockUserRepo

	svc := NewService(&Config{
		SecretKeys:             nil,
		Protocol:               strings.Join([]string{Plain}, ","),
		IndexRepository:        nil,
		UserRepository:         &userRepo,
		UserCachedRepository:   nil,
		//Vault:                  nil,
		ExternalCryptoProvider: nil,
		TransitEnabled:         false,
	})
	//if err != nil {
	//	t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	//}
	//defer db.Close()



	// now we execute our method
	type TestUser struct {
		User
		Password string
	}
	for _, u := range []TestUser{
		{User{Username: "myUsername", Email: "myEmail"}, "myPassword"},
	} {
		if hashedPwdBuf, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost); err != nil {
			panic(err)
		} else {
			u.HashedPassword = string(hashedPwdBuf)
		}

		//dbMock.ExpectBegin()
		////mock.ExpectExec("UPDATE products").WillReturnResult(sqlmock.NewResult(1, 1))
		//dbMock.ExpectExec(`INSERT INTO "users"`).WithArgs(Any{},Any{}, PlainData{}).WillReturnResult(sqlmock.NewResult(1, 1))
		//dbMock.ExpectCommit()

		if _, err := svc.UpdateUser(
			context.Background(),
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			true,
			true,
			); err != nil {
			//t.Errorf("error was not expected while create user: %s", err)
		}
	}

	// we make sure that all expectations were met
	//if err := mock.ExpectationsWereMet(); err != nil {
	//	t.Errorf("there were unfulfilled expectations: %s", err)
	//}
}


//func _TestGenerateHMAC(t *testing.T) {
//	for _, test := range []struct{
//		key, text string
//	} {
//		{key: "1234567890", text: "a"},
//		{key: "1", text: "a"},
//		{key: "1", text: "b"},
//		{key: "123", text: "abc"},
//		{key: "123", text: "abc"},
//		{key: "12311111111111111111111111111111111111111111111111111111111111111111111111111111", text: "abc22222222222222222222222222222222222222222222222223123123123123cxvcxvzxv"},
//	} {
//		t.Logf("%s\n", calcHmacSha256HexEncode(test.key, test.text))
//	}
//}

// func TestHTTP(t *testing.T) {
// 	zkt, _ := zipkin.NewTracer(nil, zipkin.WithNoopTracer(true))
// 	svc := addservice.New(log.NewNopLogger(), discard.NewCounter(), discard.NewCounter())
// 	eps := addendpoint.New(svc, log.NewNopLogger(), discard.NewHistogram(), opentracing.GlobalTracer(), zkt)
// 	mux := addtransport.NewHTTPHandler(eps, opentracing.GlobalTracer(), zkt, log.NewNopLogger())
// 	srv := httptest.NewServer(mux)
// 	defer srv.Close()

// 	for _, testcase := range []struct {
// 		method, url, body, want string
// 	}{
// 		{"GET", srv.URL + "/concat", `{"a":"1","b":"2"}`, `{"v":"12"}`},
// 		{"GET", srv.URL + "/sum", `{"a":1,"b":2}`, `{"v":3}`},
// 	} {
// 		req, _ := http.NewRequest(testcase.method, testcase.url, strings.NewReader(testcase.body))
// 		resp, _ := http.DefaultClient.Do(req)
// 		body, _ := ioutil.ReadAll(resp.Body)
// 		if want, have := testcase.want, strings.TrimSpace(string(body)); want != have {
// 			t.Errorf("%s %s %s: want %q, have %q", testcase.method, testcase.url, testcase.body, want, have)
// 		}
// 	}
// }

func BenchmarkBasicService_CreateUser(b *testing.B) {

}

func TestCreateUser(t *testing.T) {

}

//func TestServiceEncryption(t *testing.T) {
//	for _, test := range []struct{
//		text string
//	}{
//		{"abc"},
//		{"1123"},
//		{"absdf"},
//		{"adgadsfbc"},
//		{"`1234567890-=~!@#$%^&*()_+"},
//		{"abcdefghijklmnopqrstuvwxyz"},
//		{",./;'[]\\<>?:\"{}|"},
//	} {
//		ct := s.encrypt(test.text)
//		text := s.decrypt(ct)
//		if text != test.text {
//			t.Errorf("want %q, have %q", test.text, text)
//		}
//	}
//}
