package idmservice

import (
	"errors"
	"regexp"
	"strings"
)

var (
	// From [Name guidelines for users and groups](https://support.google.com/a/answer/9193374?hl=en)
	// Usernames can contain letters (a-z), numbers (0-9), dashes (-), underscores (_), apostrophes ('), and periods (.).
	// Usernames can't contain more than one period (.) in a row, accents, accented letters, ampersands (&), equal signs (=), brackets (<,>), plus signs (+), or commas (,).
	// Usernames can begin or end with non-alphanumeric characters except periods (.), with a maximum of 64 characters.
	usernamePattern = regexp.MustCompile(`^[-_'a-z0-9][-._'a-z0-9]{6,64}$`)
	passwordPattern = regexp.MustCompile(`^.*$`)
	emailPattern = regexp.MustCompile(`^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$`)
)

func usernameValidate(Username string) error {
	if len(Username) < 6 || len(Username) > 64 {
		return errors.New("value length must be at least 6 and 64 at most")
	}

	if strings.Contains(Username, "..") {
		return errors.New("usernames can't contain more than one period (.) in a row")
	}

	if !usernamePattern.MatchString(Username) {
		return errors.New("username does not match regex pattern")
	}

	return nil
}

func emailValidate(Email string) error {
	if len(Email) > 254 {
		return errors.New("value length must be at most 254 bytes")
	}

	if !emailPattern.MatchString(Email) {
		return errors.New("email does not match regex pattern")
	}

	return nil
}

func passwordValidate(Password string) error {
	if len(Password) < 8 || len(Password) > 100 {
		return errors.New("value length must be at least 8 and 100 at most")
	}

	if !passwordPattern.MatchString(Password) {
		return errors.New("password does not match regex pattern")
	}

	return nil
}
