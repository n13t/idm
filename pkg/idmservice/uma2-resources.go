package idmservice

import (
	"fmt"
	"gitlab.com/n13t/idm/pkg/uma2"
)

/**
 * UMA2 Protected Resources
 */

const (
	typeFormat = "urn:n13t:idm:resources:%s"
	uma2ReadOnlyScope = "readonly"
	uma2ManageScope   = "manage"
	uma2TransitScope  = "transit"
)

var (
	defaultType = fmt.Sprintf(typeFormat, "default")
	DefaultUserResource = uma2.Resource{
			Type:           defaultType,
			Name:           "User",
			Description:    "",
			ResourceScopes: []string{uma2ReadOnlyScope, uma2ManageScope},
			IconUri:        "",
	}
	TransitEngineResource = uma2.Resource{
		Type:           defaultType,
		Name:           "TransitEngine",
		Description:    "",
		ResourceScopes: []string{uma2TransitScope},
		IconUri:        "",
	}
)

func migrateDefaultResources(client *uma2.Config) error {
	defaultResources := []uma2.Resource{
		DefaultUserResource,
		TransitEngineResource,
	}

	for _, rs := range defaultResources {
		rsIds, err := client.ListResources("?type=" + rs.Type + "&name=" + rs.Name)
		if err != nil {
			return err
		}

		if len(rsIds) == 0 {
			// not found
			if resp, err := client.CreateResource(&rs); err != nil {
				return err
			} else {
				rs.Id = resp.Id
			}
		} else {
			DefaultUserResource.Id = rsIds[0]
		}

		if DefaultUserResource.Id == "" {
			panic("core(uma2): default resource id CANNOT be empty\n\nmigration failed")
		}
	}

	return nil
}
