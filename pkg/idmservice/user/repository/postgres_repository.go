package repository

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/n13t/idm/pkg/idmservice/user"
)

type postgresUserRepo struct {
	DB *sql.DB
}

const CreateUserQuery = `INSERT INTO "Users"(uid, metadata, data) VALUES ($1, $2, $3)`

func (r *postgresUserRepo) Search(ctx context.Context, Key, Value string, Limit, Offset int) ([]map[string]string, error){
	rows, err := r.DB.QueryContext(ctx, fmt.Sprintf(`SELECT uid, metadata, data FROM "Users" WHERE metadata @> '{"%s": "%s"}' LIMIT %d OFFSET %d`, Key, Value, Limit, Offset))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]map[string]string, 0)
	for rows.Next() {
		var uid, md, dt string
		if err := rows.Scan(&uid, &md, &dt); err != nil {
			return result, err
		}

		result = append(result, map[string]string{"uid": uid, "metadata": md, "data": dt})
	}

	if rerr := rows.Close(); rerr != nil {
		panic(rerr)
	}

	if err := rows.Err(); err != nil {
		panic(err)
	}

	return result, nil
}

func (r *postgresUserRepo) List(ctx context.Context, Limit, Offset int32) ([]map[string]string, error){
	rows, err := r.DB.QueryContext(ctx, `SELECT uid, metadata, data FROM "Users" LIMIT $1 OFFSET $2`, Limit, Offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]map[string]string, 0)
	for rows.Next() {
		var uid, md, dt string
		if err := rows.Scan(&uid, &md, &dt); err != nil {
			return result, err
		}

		result = append(result, map[string]string{"uid": uid, "metadata": md, "data": dt})
	}

	if rerr := rows.Close(); rerr != nil {
		panic(rerr)
	}

	if err := rows.Err(); err != nil {
		panic(err)
	}

	return result, nil
}

func (r *postgresUserRepo) GetByID(ctx context.Context, UID string) (string, string, error) {
	metadata := ""
	data := ""

	rows, err := r.DB.QueryContext(ctx, `SELECT metadata, data FROM "Users" WHERE uid = $1`, UID)
	if err != nil {
		return "", "", err
	}
	defer rows.Close()

	if rows.Next() {
		if err := rows.Scan(&metadata, &data); err != nil {
			return "", "", err
		}
	} else {
		return "", "", nil
	}

	rerr := rows.Close()
	if rerr != nil {
		return "", "", err
	}

	if err := rows.Err(); err != nil {
		return "", "", err
	}

	return metadata, data, nil
}

func (r *postgresUserRepo) Create(ctx context.Context, UID, Metadata, Data string) error {
	result, err := r.DB.ExecContext(ctx, CreateUserQuery, UID, Metadata, Data)
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows != 1 {
		fmt.Printf("user(postgres): create return unexpected result\n\nexpected to affect 1 row, affected %d\n", rows)
	}

	return nil
}

func (r *postgresUserRepo) Update(ctx context.Context, UID string, Metadata, Data string) error {
	result, err := r.DB.ExecContext(ctx, `UPDATE "Users" SET metadata = $2, data = $3 WHERE uid = $1`, UID, Metadata, Data)
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows != 1 {
		fmt.Printf("user(postgres): update return unexpected result\n\nexpected to affect 1 row, affected %d\n", rows)
	}

	return nil
}

func (r *postgresUserRepo) Delete(ctx context.Context, UID string) error {
	result, err := r.DB.ExecContext(ctx, `DELETE FROM "Users" WHERE uid = $1`, UID)
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows != 1 {
		fmt.Printf("user(postgres): delete return unexpected result\n\nexpected to affect 1 row, affected %d\n", rows)
	}

	return nil
}

func (r *postgresUserRepo) migrate() {
	//var id int
	var currentMigration sql.NullString 		// https://golang.org/pkg/database/sql/#NullString
	skip := true

	// CREATE TABLE IF NOT EXISTS require Postgres 9.1+
	if _, err := r.DB.Exec(`CREATE TABLE IF NOT EXISTS "UsersMigrations" (
													id serial PRIMARY KEY,
													name VARCHAR (50) UNIQUE NOT NULL,
													migrated_at TIMESTAMP NOT NULL
					);`); err != nil {
		panic(err)
	}

	if migrations, err := r.DB.Query(`SELECT name FROM "UsersMigrations" ORDER BY id DESC;`); err != nil {
		panic(err)
	} else {
		if migrations.Next() {
			err := migrations.Scan(&currentMigration)
			if err != nil {
				panic(err)
			}
			fmt.Printf("%v \n", currentMigration)
		}

		// Execute migrations in a single transaction
		{
			txn, err := r.DB.Begin()
			if err != nil {
				panic(err)
			}

			for _, migrate := range []struct {
				name  string
				query string
			}{
				{
					name: "20200313124737_initial_migration",
					query: `
						CREATE TABLE "Users" (
							id SERIAL PRIMARY KEY,
							uid VARCHAR(64) UNIQUE,
							metadata JSONB,
							data TEXT
						);
					`,
				},
				{
					name: "20200313150303_add_fields",
					query: `
				-- New --
				CREATE TABLE "20200313150303_add_fields" (
					id SERIAL PRIMARY KEY,
					version INT,
					uid VARCHAR(64) UNIQUE,
					metadata JSONB,
					data TEXT
				);

				-- Copy --
				INSERT INTO "20200313150303_add_fields"(id, uid, metadata, data) 
				SELECT * FROM "Users";

				-- Swap --
				ALTER TABLE "Users" RENAME TO "_20200313124737_initial_migration";
				ALTER TABLE "20200313150303_add_fields" RENAME TO "Users";
			`,
				},
			} {
				if !currentMigration.Valid || !skip {
					_, err = txn.Exec(migrate.query)
					if err != nil {
						panic(err)
					}

					// Update migration log
					_, _ = txn.Exec(`INSERT INTO "UsersMigrations"(name, migrated_at) VALUES($1, NOW());`, migrate.name)
				} else if currentMigration.String == migrate.name {
					skip = false
				}
			}

			err = txn.Commit()
			if err != nil {
				panic(err)
			}
		}
	}
}

func NewPostgresUserRepository(db *sql.DB) user.Repository {
	pgUserRepo := postgresUserRepo{
		DB: db,
	}

	pgUserRepo.migrate()

	return &pgUserRepo
}
