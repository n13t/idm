package repository

/**
 * https://github.com/go-redis/redis
 */

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v7"
	"gitlab.com/n13t/idm/pkg/idmservice"
)

type redisUserRepository struct {

}

func NewRedisUserRepository(addr string) *redisUserRepository {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	pong, err := client.Ping().Result()
	fmt.Println(pong, err)

	return nil
}

/**
 * Methods
 */
func (redis *redisUserRepository) Create(ctx context.Context, UUID, Name, Email, Password string) (*idmservice.User, error) {
	panic("Unimplemented")

	return nil, nil
}

