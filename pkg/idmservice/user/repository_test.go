package user

//type Suite struct {
//	suite.Suite
//	DB   *sql.DB
//	mock sqlmock.Sqlmock
//
//	repository Repository
//	user     *idmservice.User
//}
//
//func (s *Suite) SetupSuite() {
//	var (
//		db  *sql.DB
//		err error
//	)
//
//	db, s.mock, err = sqlmock.New()
//	require.NoError(s.T(), err)
//	//
//	//s.DB, err = sql.Open("postgres", db)
//	//require.NoError(s.T(), err)
//
//	//s.DB.LogMode(true)
//
//	s.repository = userRepository.NewPostgresUserRepository(db)
//}
//
//func (s *Suite) AfterTest(_, _ string) {
//	require.NoError(s.T(), s.mock.ExpectationsWereMet())
//}
//
//func TestInit(t *testing.T) {
//	suite.Run(t, new(Suite))
//}
//
//// help to avoid some mock args
//type Any struct{}
//func (a Any) Match(v driver.Value) bool { return true }
//
////func (s *Suite) Test_repository_Get() {
////	var (
////		id   = uuid.NewV4()
////		name = "test-name"
////	)
////
////	s.mock.ExpectQuery(regexp.QuoteMeta(
////		`SELECT * FROM "person" WHERE (id = $1)`)).
////		WithArgs(id.String()).
////		WillReturnRows(sqlmock.NewRows([]string{"id", "name"}).
////			AddRow(id.String(), name))
////
////	res, err := s.repository.Get(id)
////
////	require.NoError(s.T(), err)
////	require.Nil(s.T(), deep.Equal(&model.Person{ID: id, Name: name}, res))
////}
//
//func (s *Suite) Test_repository_Create() {
//	var (
//		id   = uuid.NewV4()
//		name = "test-name"
//		email = "test-email"
//		password = "test-password"
//	)
//
//	s.mock.ExpectBegin()
//	s.mock.ExpectQuery(regexp.QuoteMeta(
//		`INSERT INTO "users"
//				("uuid","username","email","password","name","email_verified","phone_number_verified")
//				VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING "users"."id"`)).
//		WithArgs(id, Any{}, Any{}, Any{}, name, Any{}, Any{}).
//		WillReturnRows(
//			sqlmock.NewRows([]string{"uuid"}).AddRow(id.String()))
//	//s.mock.ExpectCommit()
//	//s.mock.ExpectClose()
//
//	err := s.repository.Create(nil, id.String(), name, email, password)
//
//	require.NoError(s.T(), err)
//}
