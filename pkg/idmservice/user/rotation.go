package user

// key rotation is a heavy computing task
// but need to be done to enforce security

// Assumptions:
// SERVICE_KEY rotation will be perform after several months. Ex: 6 months
// VAULT_KEY rotation may be perform automatically quiet often by vault provider. Ex: once every month.
//

// Bellow functions will be performs when FORCE_KEY_ROTATION flag is enabled.
// Otherwise, key rotation will be perform automatically went data is access.

// run on leader
func scheduleJobs()  {
	//targetKey, targetKeyFingerprint := "", ""

	// counting how many tasks need to be done
	// total := SELECT count(*) FROM "Users" WHERE @> metadata.key_hmac != $targetKeyFingerprint
	// if total = 0 -> prompt: "All data is at the latest version, no additional task needed."

	// counting how many workers is available
	// SELECT count(*), workerId FROM SomeTable

	// assign tasks to workers
	// INSERT INTO AssignedTasks(taskIds) VALUES($2) WHERE workerID = $1
	//


	// If total tasks unchanged after X seconds (15s) -> reassign tasks
}

// run on workers
// run every 15s
func fetchJobs() {
	//
}

func rotatingUserData() {
	// getUserById
	// UpdateUser
}
