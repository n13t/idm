//go:generate mockery -name=Repository

package user

import (
	"context"
)

type Repository interface {
	Create(ctx context.Context, UID, Metadata, Data string) error
	GetByID(ctx context.Context, UID string) (Metadata, Data string, err error)
	Update(ctx context.Context, UID string, Metadata, Data string) error
	Delete(ctx context.Context, UID string) error
	Search(ctx context.Context, Key, Value string, Limit, Offset int) ([]map[string]string, error)
	List(ctx context.Context, Limit, Offset int32) ([]map[string]string, error)
}
