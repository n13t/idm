package idmendpoint

import (
	"context"
	"encoding/json"
	"errors"
	stdjwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/n13t/idm/pkg/uma2"
	"net/http"
	"os"

	"golang.org/x/time/rate"

	stdopentracing "github.com/opentracing/opentracing-go"
	stdzipkin "github.com/openzipkin/zipkin-go"
	"github.com/sony/gobreaker"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
	"github.com/go-kit/kit/ratelimit"
	"github.com/go-kit/kit/tracing/opentracing"
	"github.com/go-kit/kit/tracing/zipkin"

	idmService "gitlab.com/n13t/idm/pkg/idmservice"
)

// Set collects all of the endpoints that compose an add service. It's meant to
// be used as a helper struct, to collect all of the endpoints into a single
// parameter.
type Set struct {
	SumEndpoint    		endpoint.Endpoint
	ConcatEndpoint 		endpoint.Endpoint
	CreateUserEndpoint  endpoint.Endpoint
	SearchEndpoint 		endpoint.Endpoint
	ListUsersEndpoint 	endpoint.Endpoint
	UpdateUserEndpoint 	endpoint.Endpoint
	DeleteUserEndpoint 	endpoint.Endpoint
	IsValidEndpoint 	endpoint.Endpoint
	EncryptEndpoint 	endpoint.Endpoint
	DecryptEndpoint 	endpoint.Endpoint
}

type Jwks struct {
	Keys []JSONWebKeys `json:"keys"`
}

type JSONWebKeys struct {
	Kty string `json:"kty"`
	Kid string `json:"kid"`
	Use string `json:"use"`
	N string `json:"n"`
	E string `json:"e"`
	X5c []string `json:"x5c"`
}

func getPemCert(token *stdjwt.Token) (string, error) {
	cert := ""
	resp, err := http.Get(os.Getenv("JWKS_URI"))

	if err != nil {
		return cert, err
	}
	defer resp.Body.Close()

	var jwks = Jwks{}
	err = json.NewDecoder(resp.Body).Decode(&jwks)

	if err != nil {
		return cert, err
	}

	for k, _ := range jwks.Keys {
		if token.Header["kid"] == jwks.Keys[k].Kid {
			cert = "-----BEGIN CERTIFICATE-----\n" + jwks.Keys[k].X5c[0] + "\n-----END CERTIFICATE-----"
		}
	}

	if cert == "" {
		err := errors.New("Unable to find appropriate key.")
		return cert, err
	}

	return cert, nil
}

func kf (token *stdjwt.Token) (interface{}, error) {
	// HS256
	//return []byte("your-256-bit-secret"), nil

	// RS256
	cert, err := getPemCert(token)
	if err != nil {
		panic(err.Error())
	}
	result, _ := stdjwt.ParseRSAPublicKeyFromPEM([]byte(cert))
	return result, nil
}

// New returns a Set that wraps the provided server, and wires in all of the
// expected endpoint middlewares via the various parameters.
func New(svc idmService.Service, logger log.Logger, duration metrics.Histogram, otTracer stdopentracing.Tracer, zipkinTracer *stdzipkin.Tracer) Set {
	//var sumEndpoint endpoint.Endpoint
	//{
	//	sumEndpoint = MakeSumEndpoint(svc)
	//	// Sum is limited to 1 request per second with burst of 1 request.
	//	// Note, rate is defined as a time interval between requests.
	//	sumEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Every(time.Second), 1))(sumEndpoint)
	//	sumEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(sumEndpoint)
	//	sumEndpoint = opentracing.TraceServer(otTracer, "Sum")(sumEndpoint)
	//	if zipkinTracer != nil {
	//		sumEndpoint = zipkin.TraceEndpoint(zipkinTracer, "Sum")(sumEndpoint)
	//	}
	//	sumEndpoint = LoggingMiddleware(log.With(logger, "method", "Sum"))(sumEndpoint)
	//	sumEndpoint = InstrumentingMiddleware(duration.With("method", "Sum"))(sumEndpoint)
	//}
	//var concatEndpoint endpoint.Endpoint
	//{
	//	concatEndpoint = MakeConcatEndpoint(svc)
	//	// Concat is limited to 1 request per second with burst of 100 requests.
	//	// Note, rate is defined as a number of requests per second.
	//	concatEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Limit(1), 100))(concatEndpoint)
	//	concatEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(concatEndpoint)
	//	concatEndpoint = opentracing.TraceServer(otTracer, "Concat")(concatEndpoint)
	//	if zipkinTracer != nil {
	//		concatEndpoint = zipkin.TraceEndpoint(zipkinTracer, "Concat")(concatEndpoint)
	//	}
	//	concatEndpoint = LoggingMiddleware(log.With(logger, "method", "Concat"))(concatEndpoint)
	//	concatEndpoint = InstrumentingMiddleware(duration.With("method", "Concat"))(concatEndpoint)
	//}
	var createUserEndpoint endpoint.Endpoint
	{
		createUserEndpoint = MakeCreateUserEndpoint(svc)
		//createUserEndpoint = jwt.NewParser(kf, stdjwt.SigningMethodRS256, func() stdjwt.Claims {return &idmService.CustomClaims{}})(createUserEndpoint)
		// limited to 1 request per second with burst of 100 requests.
		// Note, rate is defined as a number of requests per second.
		createUserEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Limit(1), 100))(createUserEndpoint)
		createUserEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(createUserEndpoint)
		createUserEndpoint = opentracing.TraceServer(otTracer, "CreateUser")(createUserEndpoint)
		if zipkinTracer != nil {
			createUserEndpoint = zipkin.TraceEndpoint(zipkinTracer, "CreateUser")(createUserEndpoint)
		}
		createUserEndpoint = LoggingMiddleware(log.With(logger, "method", "CreateUser"))(createUserEndpoint)
		createUserEndpoint = InstrumentingMiddleware(duration.With("method", "CreateUser"))(createUserEndpoint)
	}
	var updateUserEndpoint endpoint.Endpoint
	{
		updateUserEndpoint = MakeUpdateUserEndpoint(svc)
		// limited to 1 request per second with burst of 100 requests.
		// Note, rate is defined as a number of requests per second.
		updateUserEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Limit(1), 100))(updateUserEndpoint)
		updateUserEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(updateUserEndpoint)
		updateUserEndpoint = opentracing.TraceServer(otTracer, "UpdateUser")(updateUserEndpoint)
		if zipkinTracer != nil {
			updateUserEndpoint = zipkin.TraceEndpoint(zipkinTracer, "UpdateUser")(updateUserEndpoint)
		}
		updateUserEndpoint = LoggingMiddleware(log.With(logger, "method", "UpdateUser"))(updateUserEndpoint)
		updateUserEndpoint = InstrumentingMiddleware(duration.With("method", "UpdateUser"))(updateUserEndpoint)
	}
	var searchEndpoint endpoint.Endpoint
	{
		searchEndpoint = MakeSearchEndpoint(svc)
		//searchEndpoint = jwt.NewParser(kf, stdjwt.SigningMethodRS256, func() stdjwt.Claims {return &idmService.CustomClaims{}})(searchEndpoint)
		searchEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Limit(1), 100))(searchEndpoint)
		searchEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(searchEndpoint)
		searchEndpoint = opentracing.TraceServer(otTracer, "Search")(searchEndpoint)
		if zipkinTracer != nil {
			searchEndpoint = zipkin.TraceEndpoint(zipkinTracer, "Search")(searchEndpoint)
		}
		searchEndpoint = LoggingMiddleware(log.With(logger, "method", "Search"))(searchEndpoint)
		searchEndpoint = InstrumentingMiddleware(duration.With("method", "Search"))(searchEndpoint)
	}
	var listUsersEndpoint endpoint.Endpoint
	{
		listUsersEndpoint = MakeListUsersEndpoint(svc)
		//listUsersEndpoint = jwt.NewParser(kf, stdjwt.SigningMethodRS256, func() stdjwt.Claims { return &idmService.CustomClaims{} })(listUsersEndpoint)
		//{
		//	f := func(file string) endpoint.Middleware {
		//		return func(next endpoint.Endpoint) endpoint.Endpoint {
		//			return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		//				return casbin.NewEnforcer("", "", ""), nil
		//			}
		//		}
		//	}
		//	listUsersEndpoint = f("")(listUsersEndpoint)
		//}
		//listUsersEndpoint = casbin.NewEnforcer("", "", "")(listUsersEndpoint)
		listUsersEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Limit(1), 100))(listUsersEndpoint)
		listUsersEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(listUsersEndpoint)
		listUsersEndpoint = opentracing.TraceServer(otTracer, "ListUsers")(listUsersEndpoint)
		if zipkinTracer != nil {
			listUsersEndpoint = zipkin.TraceEndpoint(zipkinTracer, "ListUsers")(listUsersEndpoint)
		}
		listUsersEndpoint = LoggingMiddleware(log.With(logger, "method", "ListUsers"))(listUsersEndpoint)
		listUsersEndpoint = InstrumentingMiddleware(duration.With("method", "ListUsers"))(listUsersEndpoint)
		//listUsersEndpoint = AuthMiddleware("")(listUsersEndpoint)
	}
	var deleteUserEndpoint endpoint.Endpoint
	{
		deleteUserEndpoint = MakeDeleteUserEndpoint(svc)
		// limited to 1 request per second with burst of 1 requests.
		// Note, rate is defined as a number of requests per second.
		deleteUserEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Limit(1), 1))(deleteUserEndpoint)
		deleteUserEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(deleteUserEndpoint)
		deleteUserEndpoint = opentracing.TraceServer(otTracer, "DeleteUser")(deleteUserEndpoint)
		if zipkinTracer != nil {
			deleteUserEndpoint = zipkin.TraceEndpoint(zipkinTracer, "DeleteUser")(deleteUserEndpoint)
		}
		deleteUserEndpoint = LoggingMiddleware(log.With(logger, "method", "DeleteUser"))(deleteUserEndpoint)
		deleteUserEndpoint = InstrumentingMiddleware(duration.With("method", "DeleteUser"))(deleteUserEndpoint)
	}
	var isValidEndpoint endpoint.Endpoint
	{
		isValidEndpoint = MakeIsValidEndpoint(svc)
		// Concat is limited to 1 request per second with burst of 100 requests.
		// Note, rate is defined as a number of requests per second.
		isValidEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Limit(1), 100))(isValidEndpoint)
		isValidEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(isValidEndpoint)
		isValidEndpoint = opentracing.TraceServer(otTracer, "IsValid")(isValidEndpoint)
		if zipkinTracer != nil {
			isValidEndpoint = zipkin.TraceEndpoint(zipkinTracer, "IsValid")(isValidEndpoint)
		}
		isValidEndpoint = LoggingMiddleware(log.With(logger, "method", "IsValid"))(isValidEndpoint)
		isValidEndpoint = InstrumentingMiddleware(duration.With("method", "IsValid"))(isValidEndpoint)
	}
	var encryptEndpoint endpoint.Endpoint
	{
		encryptEndpoint = MakeEncryptEndpoint(svc)
		// limited to 1 request per second with burst of 100 requests.
		// Note, rate is defined as a number of requests per second.
		encryptEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Limit(1e2), 1e5))(encryptEndpoint)
		encryptEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(encryptEndpoint)
		encryptEndpoint = opentracing.TraceServer(otTracer, "Encrypt")(encryptEndpoint)
		if zipkinTracer != nil {
			encryptEndpoint = zipkin.TraceEndpoint(zipkinTracer, "Encrypt")(encryptEndpoint)
		}
		encryptEndpoint = LoggingMiddleware(log.With(logger, "method", "Encrypt"))(encryptEndpoint)
		encryptEndpoint = InstrumentingMiddleware(duration.With("method", "Encrypt"))(encryptEndpoint)
	}
	var decryptEndpoint endpoint.Endpoint
	{
		decryptEndpoint = MakeDecryptEndpoint(svc)
		// limited to 1 request per second with burst of 100 requests.
		// Note, rate is defined as a number of requests per second.
		decryptEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Limit(1e2), 1e5))(decryptEndpoint)
		decryptEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(decryptEndpoint)
		decryptEndpoint = opentracing.TraceServer(otTracer, "Decrypt")(decryptEndpoint)
		if zipkinTracer != nil {
			decryptEndpoint = zipkin.TraceEndpoint(zipkinTracer, "Decrypt")(decryptEndpoint)
		}
		decryptEndpoint = LoggingMiddleware(log.With(logger, "method", "Decrypt"))(decryptEndpoint)
		decryptEndpoint = InstrumentingMiddleware(duration.With("method", "Decrypt"))(decryptEndpoint)
	}

	return Set{
		//SumEndpoint:    sumEndpoint,
		//ConcatEndpoint: concatEndpoint,
		CreateUserEndpoint: createUserEndpoint,
		UpdateUserEndpoint: updateUserEndpoint,
		SearchEndpoint: 	searchEndpoint,
		ListUsersEndpoint: 	listUsersEndpoint,
		DeleteUserEndpoint: deleteUserEndpoint,
		IsValidEndpoint: 	isValidEndpoint,
		EncryptEndpoint: 	encryptEndpoint,
		DecryptEndpoint: 	decryptEndpoint,
	}
}

// Sum implements the service interface, so Set may be used as a service.
// This is primarily useful in the context of a client library.
func (s Set) Sum(ctx context.Context, a, b int) (int, error) {
	resp, err := s.SumEndpoint(ctx, SumRequest{A: a, B: b})
	if err != nil {
		return 0, err
	}
	response := resp.(SumResponse)
	return response.V, response.Err
}

// Concat implements the service interface, so Set may be used as a
// service. This is primarily useful in the context of a client library.
func (s Set) Concat(ctx context.Context, a, b string) (string, error) {
	resp, err := s.ConcatEndpoint(ctx, ConcatRequest{A: a, B: b})
	if err != nil {
		return "", err
	}
	response := resp.(ConcatResponse)
	return response.V, response.Err
}

func (s Set) Search(ctx context.Context, Index, Query string, Limit, Offset int32) ([]idmService.User, error) {
	resp, err := s.SearchEndpoint(ctx, SearchRequest{Index, Query, Limit, Offset})
	if err != nil {
		return nil, err
	}

	response := resp.(SearchResponse)
	return response.Users, response.Err
}

func (s Set) ListUsers(ctx context.Context, PageSize int32, PageToken string) ([]idmService.User, string, error) {
	resp, err := s.ListUsersEndpoint(ctx, ListUsersRequest{PageSize: PageSize, PageToken: PageToken})
	if err != nil {
		return nil, "", err
	}
	response := resp.(ListUsersResponse)
	return response.Users, response.NextPageToken, response.Err
}

func (s Set) CreateUser(ctx context.Context, Username, Email, Password string) (string, error) {
	resp, err := s.CreateUserEndpoint(ctx, CreateUserRequest{Username: Username, Email: Email, Password: Password})
	if err != nil {
		return "", err
	}
	response := resp.(CreateUserResponse)
	return "", response.Err
}

func (s Set) UpdateUser(ctx context.Context, UID, Password,
	GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber,
	ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country string,
	EmailVerified, PhoneNumberVerified bool) (string, error) {
	resp, err := s.UpdateUserEndpoint(ctx, UpdateUserRequest{
		UID:         UID,
		Password: Password,
		GivenName:   GivenName,
		FamilyName:  FamilyName,
		MiddleName:  MiddleName,
		Nickname:    Nickname,
		Gender:      Gender,
		Birthday:    Birthday,
		ZoneInfo:    ZoneInfo,
		Locale:      Locale,
		PhoneNumber: PhoneNumber,
		ProfileURL:  ProfileURL,
		PictureURL: PictureURL,
		WebsiteURL: WebsiteURL,
		Email: Email,
		StreetAddress: StreetAddress,
		Locality: Locality,
		Region: Region,
		PostalCode: PostalCode,
		Country: Country,
		EmailVerified: EmailVerified,
		PhoneNumberVerified: PhoneNumberVerified,
	})
	if err != nil {
		return "", err
	}
	response := resp.(UpdateUserResponse)
	return "", response.Err
}

func (s Set) DeleteUser(ctx context.Context, UID string) error {
	resp, err := s.DeleteUserEndpoint(ctx, DeleteUserRequest{UID})
	if err != nil {
		return err
	}
	response := resp.(DeleteUserResponse)
	return response.Err
}

func (s Set) IsValid(ctx context.Context, id, password string) (bool, error) {
	resp, err := s.IsValidEndpoint(ctx, IsValidRequest{Id: id, Password: password})
	if err != nil {
		return false, err
	}
	response := resp.(IsValidResponse)
	return response.V, response.Err
}

func (s Set) Encrypt(ctx context.Context, Plaintext string) (string, error) {
	resp, err := s.EncryptEndpoint(ctx, EncryptRequest{Plaintext: Plaintext})
	if err != nil {
		return "", err
	}
	response := resp.(EncryptResponse)
	return response.Ciphertext, response.Err
}

func (s Set) Decrypt(ctx context.Context, Ciphertext string) (string, error) {
	resp, err := s.DecryptEndpoint(ctx, DecryptRequest{Ciphertext})
	if err != nil {
		return "", err
	}
	response := resp.(DecryptResponse)
	return response.Plaintext, response.Err
}

//// MakeSumEndpoint constructs a Sum endpoint wrapping the service.
//func MakeSumEndpoint(s idmService.Service) endpoint.Endpoint {
//	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
//		req := request.(SumRequest)
//		v, err := s.Sum(ctx, req.A, req.B)
//		return SumResponse{V: v, Err: err}, nil
//	}
//}

//// MakeConcatEndpoint constructs a Concat endpoint wrapping the service.
//func MakeConcatEndpoint(s idmService.Service) endpoint.Endpoint {
//	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
//		req := request.(ConcatRequest)
//		v, err := s.Concat(ctx, req.A, req.B)
//		return ConcatResponse{V: v, Err: err}, nil
//	}
//}

func MakeCreateUserEndpoint(s idmService.Service) endpoint.Endpoint  {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(CreateUserRequest)
		v, err := s.CreateUser(ctx, req.Username, req.Email, req.Password)
		return CreateUserResponse{V: v, Err: err}, nil
	}
}

func MakeSearchEndpoint(s idmService.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(SearchRequest)

		resp, err := s.Search(ctx, req.Index, req.Query, req.Limit, req.Offset)
		return SearchResponse{Users: resp, Err: err}, nil
	}
}

func MakeListUsersEndpoint(s idmService.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(ListUsersRequest)
		users, nextPageToken, err := s.ListUsers(ctx, req.PageSize, req.PageToken)
		return ListUsersResponse{Users: users, NextPageToken: nextPageToken, Err: err}, nil
	}
}

func MakeUpdateUserEndpoint(s idmService.Service) endpoint.Endpoint  {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(UpdateUserRequest)
		v, err := s.UpdateUser(ctx,
			req.UID, req.Password,
			req.GivenName, req.FamilyName, req.MiddleName, req.Nickname, req.Gender, req.Birthday, req.ZoneInfo, req.Locale, req.PhoneNumber,
			req.ProfileURL, req.PictureURL, req.WebsiteURL, req.Email, req.StreetAddress, req.Locality, req.Region, req.PostalCode, req.Country,
			req.EmailVerified, req.PhoneNumberVerified,
		)
		return UpdateUserResponse{V: v, Err: err}, nil
	}
}

func MakeDeleteUserEndpoint(s idmService.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteUserRequest)
		err := s.DeleteUser(ctx, req.UID)
		return DeleteUserResponse{Err: err}, nil
	}
}

func MakeIsValidEndpoint(s idmService.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(IsValidRequest)
		v, err := s.IsValid(ctx, req.Id, req.Password)
		return IsValidResponse{V: v, Err: err}, nil
	}
}

func MakeEncryptEndpoint(s idmService.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(EncryptRequest)
		v, err := s.Encrypt(ctx, req.Plaintext)
		return EncryptResponse{Ciphertext: v, Err: err}, nil
	}
}

func MakeDecryptEndpoint(s idmService.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(DecryptRequest)
		v, err := s.Decrypt(ctx, req.Ciphertext)
		return DecryptResponse{Plaintext: v, Err: err}, nil
	}
}

// compile time assertions for our response types implementing endpoint.Failer.
var (
	_ endpoint.Failer = SumResponse{}
	_ endpoint.Failer = ConcatResponse{}
	_ endpoint.Failer = SearchResponse{}
	_ endpoint.Failer = CreateUserResponse{}
	_ endpoint.Failer = UpdateUserResponse{}
	_ endpoint.Failer = IsValidResponse{}
	_ endpoint.Failer = ListUsersResponse{}
)

type UmaResponse struct {
	Ticket uma2.Ticket
}

// SumRequest collects the request parameters for the Sum method.
type SumRequest struct {
	A, B int
}

// SumResponse collects the response values for the Sum method.
type SumResponse struct {
	V   int   `json:"v"`
	Err error `json:"-"` // should be intercepted by Failed/errorEncoder
}

// Failed implements endpoint.Failer.
func (r SumResponse) Failed() error { return r.Err }

// ConcatRequest collects the request parameters for the Concat method.
type ConcatRequest struct {
	A, B string
}

// ConcatResponse collects the response values for the Concat method.
type ConcatResponse struct {
	V   string `json:"v"`
	Err error  `json:"-"`
}

// Failed implements endpoint.Failer.
func (r ConcatResponse) Failed() error { return r.Err }


type SearchRequest struct {
	Index, Query string
	Limit, Offset int32
}

type SearchResponse struct {
	Users   []idmService.User `json:"users"`
	Err 	error `json:"-"`
}

func (r SearchResponse) Failed() error { return r.Err }

type ListUsersRequest struct {
	PageSize 	int32
	PageToken 	string
}

type ListUsersResponse struct {
	Users   		[]idmService.User 	`json:"users"`
	NextPageToken 	string 				`json:"next_page_token"`
	Err 			error 				`json:"-"`
}

func (r ListUsersResponse) Failed() error { return r.Err }

type CreateUserRequest struct {
	Username 	string
	Email 		string
	Password 	string
}

type CreateUserResponse struct {
	V   string `json:"v"`
	Err error `json:"-"`
}

func (r CreateUserResponse) Failed() error { return r.Err }

// UpdateUser
type UpdateUserRequest struct {
	UID, Password,
	GivenName, FamilyName, MiddleName, Nickname, Gender, Birthday, ZoneInfo, Locale, PhoneNumber,
	ProfileURL, PictureURL, WebsiteURL, Email, StreetAddress, Locality, Region, PostalCode, Country string
	EmailVerified, PhoneNumberVerified bool
}

type UpdateUserResponse struct {
	V   string `json:"v"`
	Err error  `json:"-"`
}

func (r UpdateUserResponse) Failed() error { return r.Err }

// Delete User
type DeleteUserRequest struct {
	UID string
}

type DeleteUserResponse struct {
	Err error  `json:"-"`
}
func (r DeleteUserResponse) Failed() error { return r.Err }

// IsValid
type IsValidRequest struct {
	Id, Password string
}
type IsValidResponse struct {
	V   bool  `json:"v"`
	Err error `json:"-"`
}

func (r IsValidResponse) Failed() error { return r.Err }

// Encrypt
type EncryptRequest struct {
	Plaintext string
}

type EncryptResponse struct {
	Ciphertext	string  `json:"ciphertext"`
	Err 		error 	`json:"-"`
}

func (r EncryptResponse) Failed() error { return r.Err }

// Decrypt
type DecryptRequest struct {
	Ciphertext string
}

type DecryptResponse struct {
	Plaintext	string  `json:"plaintext"`
	Err 		error 	`json:"-"`
}

func (r DecryptResponse) Failed() error { return r.Err }