# Build image
FROM golang:1.13-buster as build

WORKDIR /go/src/app

COPY go.* ./
RUN go mod download         ## Go 1.11+
COPY . .
RUN go build -o /go/bin/idm ./cmd/idm

# Release image
FROM debian:10
COPY --from=build /go/bin/idm /
COPY ./scripts/wait-for-it.sh /
CMD ["/app", "--grpc-addr=:8082"]