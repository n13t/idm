# multi-stage build
# build
FROM golang:1.13

WORKDIR /src
COPY ./cmd/idm/go.mod ./cmd/idm/go.sum ./
RUN go mod download
COPY ./cmd/idm .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

#FROM golang:1.13
#WORKDIR /src
#COPY ./cmd/grpc-gateway/go.mod ./cmd/grpc-gateway/go.sum ./
#RUN go mod download
#COPY ./cmd/grpc-gateway .
#RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
#RUN ls -la /go/

# release
FROM envoyproxy/envoy-alpine:v1.12.2
ENV GRPC_GO_LOG_VERBOSITY_LEVEL=99 GRPC_GO_LOG_SEVERITY_LEVEL=info
RUN apk --no-cache add supervisor ca-certificates
COPY ./init/supervisord.conf /etc/supervisord.conf
COPY ./config/envoy.yaml /etc/envoy.yaml
COPY --from=0 /go/pkg/mod/gitlab.com/nghinhut/protos*/generated/idm/v0/descriptor_set.pb ./
COPY --from=0 /go/pkg/mod/gitlab.com/nghinhut/protos*/generated/idm/v0/doc ./
COPY --from=0 /go/pkg/mod/gitlab.com/nghinhut/protos*/generated/idm/v0/*.swagger.json ./
COPY --from=0 /src/main ./idm


EXPOSE 50051 5000 9901
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
