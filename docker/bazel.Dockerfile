##
# Build stage
##
# bazel 2.0.0
FROM l.gcr.io/google/bazel@sha256:e8b0adc7d9b8cf6ef28ae8d8c575169a9affeaca89fec2fced0e38c8d08e5059
WORKDIR /src
RUN bazel version
COPY WORKSPACE BUILD ./
COPY ./cmd/ ./cmd/
# --compilation_mode (fastbuild|dbg|opt) (-c)
RUN bazel build \
    --platforms=@io_bazel_rules_go//go/toolchain:linux_amd64 \
    --spawn_strategy=standalone \
    --genrule_strategy=standalone \
    --compilation_mode opt \
    //cmd/idm:idm
RUN ls -lah && pwd
RUN ls -lah ./bazel-src/external/com_gitlab_n13t_protos/pkg/n13t/idm/v0/
RUN ls -la ./bazel-bin/cmd/idm/
RUN ls -la ./bazel-bin/cmd/idm/linux_amd64_pure_stripped/idm


##
# Release Image
##
FROM envoyproxy/envoy-alpine:v1.12.2

RUN apk --no-cache add supervisor ca-certificates

COPY ./init/supervisord.conf /etc/supervisord.conf
COPY ./config/envoy.yaml /etc/envoy.yaml
COPY --from=0 /src/bazel-src/external/com_gitlab_n13t_protos/pkg/n13t/idm/v0/descriptor_set.pb ./
COPY --from=0 /src/bazel-src/external/com_gitlab_n13t_protos/pkg/n13t/idm/v0/doc ./
COPY --from=0 /src/bazel-src/external/com_gitlab_n13t_protos/pkg/n13t/idm/v0/idm.swagger.json ./
COPY --from=0 /src/bazel-src/external/com_gitlab_n13t_protos/pkg/n13t/idm/v0/user.swagger.json ./

COPY --from=0 /src/bazel-bin/cmd/idm/linux_amd64_pure_stripped/idm ./idm

EXPOSE 50051 5000 9901

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
